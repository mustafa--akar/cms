<div class="row">
	<div class="col-md-12">
		<h4 class="m-b-lg">
			Yeni Ürün Ekle			
		</h4>
	</div><!-- END column -->
	<div class="col-md-12">
		<div class="widget">			
			<div class="widget-body">
			
			<?php if(empty($categories)){ ?>
			<div class="alert alert-info">				
				<h4 class="alert-title">Ürün kategorisi bulunamadı</h4>
				<p>Lütfen önce ürünlere ait kategori ekleyiniz. Eklemek için lütfen <a href="<?php echo base_url("product_categories/new_form"); ?>">tıklayınız</a> </p>
			</div>	
			<?php }else { ?>
			
				<form action="<?php echo base_url("product/save"); ?>" method="post">
					
				<div class="form-group">
						<label>Kategori</label>
						<select class="form-control" name="category_id">
							<?php foreach($categories as $category){ ?>
								<?php $category_id = (isset($form_error)) ? set_value("category_id") : ""; ?>
								<option 
									<?php echo ($category->id == $category_id) ? "selected" : ""; ?>
									value="<?php echo $category->id; ?>"><?php echo $category->title; ?></option>
							<?php } ?>
						</select>
					</div>					
						
					<div class="form-group">
						<label>Başlık</label>
						<input class="form-control" placeholder="Başlık" name="title">
						<?php if(isset($form_error)){ ?>
							<small class="input-form-error"><?php echo form_error("title"); ?></small>
						<?php } ?>
					</div>
										
					<div class="form-group">
						<label>Açıklama</label>
						<textarea name="description" class="m-0" data-plugin="summernote" data-options="{height: 250}"></textarea>
					</div>
					<button type="submit" class="btn btn-primary btn-md btn-outline">Kaydet</button>
					<a href="<?php echo base_url("product"); ?>" class="btn btn-md btn-danger btn-outline">İptal</a>
				</form>
				
			<?php } ?>
			</div><!-- .widget-body -->
		</div><!-- .widget -->
	</div><!-- END column -->
</div>