<div class="row">
	<div class="col-md-12">
		<h4 class="m-b-lg">
			Galeri Listesi
			<?php if(isAllowedWriteModule()){ ?>
				<a href="<?php echo base_url("galleries/new_form"); ?>" class="btn btn-outline btn-primary btn-xs pull-right"><i class="fa fa-plus"></i>  Yeni Ekle</a>
			<?php } ?>	
		</h4>
	</div><!-- END column -->
	<div class="col-md-12">
		<div class="widget p-lg">
			<?php if(empty($items)){ ?>
			<div class="alert alert-info">				
				<h4 class="alert-title">Kayıt Bulunamadı</h4>
				<p>Burada herhangi bir kayıt bulunmamaktadır. Eklemek için lütfen <a href="<?php echo base_url("galleries/new_form"); ?>">tıklayınız</a> </p>
			</div>	
			<?php }else { ?>	
			<table class="table table-striped table-hover content_container table-bordered">
				<thead>
					<th class="order"><i class="fa fa-reorder"></i></th>
					<th class="w50 text-center">#id</th>
					<th>Galeri Adı</th>	
					<th>Galeri Türü</th>
					<th>Klasör Adı</th>
					<!--<th>Url</th> -->					
					<th>Durum</th>
					<th>İşlem</th>
				</thead>
				<tbody class="sortable" data-url = "<?php echo base_url("galleries/rankSetter"); ?>">
					<?php foreach($items as $item){ 				
						
						if($item->gallery_type == "image")
						{
							$button_icon = "fa-image";
						}else if($item->gallery_type == "file"){
							$button_icon = "fa-folder";
						}else if($item->gallery_type == "video"){
							$button_icon = "fa-play-circle-o";
						}	
						
					?>					
					<tr id="ord-<?php echo $item->id; ?>">
						<td class="order"><i class="fa fa-reorder"></i></td>
						<td class="w50 text-center"><?php echo '#'.$item->id; ?></td>
						<td class="w200"><?php echo $item->title; ?></td>
						<td class="text-center"> <i class="fa fa-2x <?php echo $button_icon; ?>"></i>  </td>					
						<td class=""><?php echo $item->folder_name; ?></td>
						<!--  <td class=""><?php echo $item->url; ?></td>  -->
						<td class="text-center">							
							<input 
								class="isActive"
								type="checkbox"
								data-url = "<?php echo base_url("galleries/isActiveSetter/$item->id"); ?>"
								data-switchery
								data-color="#10c469"
								<?php echo ($item->isActive) ? "checked" : "" ; ?> />
						</td>
						<td class="text-center">
							<?php if(isAllowedDeleteModule()){ ?>
								<button 
									data-url="<?php echo base_url("galleries/delete/$item->id"); ?>"
									class="btn btn-sm btn-danger btn-outline remove-btn">
									<i class="fa fa-trash"></i> Sil
								</button>
						    <?php } ?>

							<?php 
								
								if($item->gallery_type == "image"){
									
									$button_icon = "fa-image";
									$button_url = "galleries/upload_form/$item->id";
									
								}else if($item->gallery_type == "file"){
									
									$button_icon = "fa-folder";
									$button_url = "galleries/upload_form/$item->id";
									
								}else if($item->gallery_type == "video"){
									
									$button_icon = "fa-play-circle-o";
									$button_url = "galleries/gallery_video_list/$item->id";
									
								}	
								
							?>
							<?php if(isAllowedUpdateModule()){ ?>
								<a href="<?php echo base_url("galleries/update_form/$item->id"); ?>" class="btn btn-sm btn-warning btn-outline"><i class="fa fa-pencil-square-o"></i> Düzenle</a>
								<a href="<?php echo base_url($button_url); ?>" class="btn btn-sm btn-dark btn-outline">							
									<i class="fa <?php echo $button_icon; ?>"></i> 
									<?php 
										
										if($item->gallery_type == "image"){
											echo "Resimler";
										}else if($item->gallery_type == "video"){
											echo "Videolar";
										}else if($item->gallery_type == "file"){
											echo "Dosyalar";
										}
										
									?>
								</a>
							<?php } ?>	
						</td>						
					</tr>
					<?php } ?>

				</tbody>
			</table>
			
			<?php } ?>
		</div><!-- .widget -->
	</div><!-- END column -->
</div>