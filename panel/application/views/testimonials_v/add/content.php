<div class="row">
	<div class="col-md-12">
		<h4 class="m-b-lg">
			Yeni Ziyaretçi Notu Ekle			
		</h4>
	</div><!-- END column -->
	<div class="col-md-12">
		<div class="widget">			
			<div class="widget-body">

				<form action="<?php echo base_url("testimonials/save"); ?>" method="post" enctype="multipart/form-data">
					<div class="form-group">
						<label><?php echo text("Dili Seçiniz") ?></label>
						<select class="form-control" name="lang">
							<option <?php echo ($this->session->userdata("lang") == "tr") ? "selected" : "" ; ?>
								value="tr"><?php echo text("Türkçe"); ?>
							<option <?php echo ($this->session->userdata("lang") == "de") ? "selected" : "" ; ?>
								 value="de"><?php echo text("Almanca"); ?>
							<option <?php echo ($this->session->userdata("lang") == "en") ? "selected" : "" ; ?>
								value="en"><?php echo text("İngilizce"); ?>
						</select>
					</div>					
					<div class="form-group">
						<label>Ad Soyad</label>
						<input class="form-control" placeholder="İsim Soyisim" name="full_name">
						<?php if(isset($form_error)){ ?>
							<small class="input-form-error"><?php echo form_error("full_name"); ?></small>
						<?php } ?>
					</div>	
					<div class="form-group">
						<label>Şirket Adı</label>
						<input class="form-control" placeholder="Şirket Adı" name="company">
						<?php if(isset($form_error)){ ?>
							<small class="input-form-error"><?php echo form_error("company"); ?></small>
						<?php } ?>
					</div>										
					<div class="form-group">
						<label>Başlık</label>
						<input class="form-control" placeholder="Başlık" name="title">
						<?php if(isset($form_error)){ ?>
							<small class="input-form-error"><?php echo form_error("title"); ?></small>
						<?php } ?>
					</div>
					<div class="form-group">
						<label>Ziyaretçi Notu</label>
						<textarea class="form-control" name="description" placeholder="Mesajı yazınız" cols="30" rows="10"></textarea>
						<?php if(isset($form_error)){ ?>
							<small class="input-form-error"><?php echo form_error("title"); ?></small>
						<?php } ?>
					</div>										
				
					<div class="form-group image_upload_container">
						<label>Görsel Seçiniz</label>
						<input type="file" name="img_url" class="form-control">
					</div>					
					
					<button type="submit" class="btn btn-primary btn-md btn-outline">Kaydet</button>
					<a href="<?php echo base_url("testimonials"); ?>" class="btn btn-md btn-danger btn-outline">İptal</a>
				</form>
			</div><!-- .widget-body -->
		</div><!-- .widget -->
	</div><!-- END column -->
</div>