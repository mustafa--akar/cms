<div class="row">
	<div class="col-md-8">
		<h4 class="m-b-lg">
			E-Posta Gönder	
		</h4>
	</div><!-- END column -->	
	<div class="col-md-8">
	<div class="panel panel-default new-message">						
			<form action="<?php echo base_url("send_email/send"); ?>" class="cms-email" method="post">
				<div class="panel-body">
					<div class="form-group">
						<input type="text" class="form-control" placeholder="<?php echo isset($form_error)  ? set_value("to") : "Kime" ;?>" name="to">
					</div><!-- .form-group -->

					<div class="form-group m-b-0">
						<div class="row">
							<div class="col-sm-6">
								<input type="text" class="form-control m-b-lg" placeholder="<?php echo isset($form_error)  ? set_value("cc") : "Cc" ;?>" name="cc">
							<?php if(isset($form_error)){ ?>
								<small class="input-form-error"><?php echo form_error("cc"); ?></small>
							<?php } ?>								
							</div>
							<div class="col-sm-6">
								<input type="text" class="form-control m-b-lg" placeholder="<?php echo isset($form_error)  ? set_value("bcc") : "Bcc" ;?>" name="bcc">
							<?php if(isset($form_error)){ ?>
								<small class="input-form-error"><?php echo form_error("bcc"); ?></small>
							<?php } ?>								
							</div>
						</div>
					</div><!-- .form-group -->

					<div class="form-group">
						<input type="text" class="form-control" placeholder="<?php echo isset($form_error)  ? set_value("subject") : "Konu" ;?>" name="subject">
							<?php if(isset($form_error)){ ?>
								<small class="input-form-error"><?php echo form_error("subject"); ?></small>
							<?php } ?>						
					</div><!-- .form-group -->

					<textarea name="message" class="form-control full-wysiwyg"><?php echo isset($form_error)  ? set_value("message") : "" ;?></textarea>
					<?php if(isset($form_error)){ ?>
						<small class="input-form-error"><?php echo form_error("message"); ?></small>
					<?php } ?>					
				</div><!-- .panel-body -->

				<div class="panel-footer clearfix">
					<div class="pull-right">
						<button type="button" class="btn btn-danger clear-all-input"><i class="fa fa-trash"></i></button>						
						<button type="submit" class="btn btn-primary">Gönder <i class="fa fa-send"></i></button>
					</div>
				</div><!-- .panel-footer -->
			</form>
		</div>		

	</div>

</div>
	