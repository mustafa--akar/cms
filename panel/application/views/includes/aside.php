<?php $user = get_active_user(); ?>

<!-- APP ASIDE ==========-->
<aside id="menubar" class="menubar light">
  <div class="app-user">
    <div class="media">
      <div class="media-left">
        <div class="avatar avatar-md avatar-circle">
          <a href="javascript:void(0)"><img class="img-responsive" src="<?php echo base_url("assets"); ?>/assets/images/221.jpg" alt="avatar"/></a>
        </div><!-- .avatar -->
      </div>
      <div class="media-body">
        <div class="foldable">
          <h5><a href="javascript:void(0)" class="username"><?php echo $user->full_name; ?></a></h5>
          <ul>
            <li class="dropdown">
              <a href="javascript:void(0)" class="dropdown-toggle usertitle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <small><?php echo text("İşlemler"); ?></small>
                <span class="caret"></span>
              </a>
              <ul class="dropdown-menu animated flipInY">
                <li>
                  <a class="text-color" href="<?php echo base_url(); ?>">
                    <span class="m-r-xs"><i class="fa fa-home"></i></span>
                    <span><?php echo text("Ana Sayfa"); ?></span>
                  </a>
                </li>
                <li>
                  <a class="text-color" href="<?php echo base_url("users/update_form/$user->id"); ?>">
                    <span class="m-r-xs"><i class="fa fa-user"></i></span>
                    <span><?php echo text("Profilim"); ?></span>
                  </a>
                </li>

                <li role="separator" class="divider"></li>
                <li>
                  <a class="text-color" href="<?php echo base_url("logout"); ?>">
                    <span class="m-r-xs"><i class="fa fa-power-off"></i></span>
                    <span><?php echo text("Çıkış"); ?></span>
                  </a>
                </li>
              </ul>
            </li>
          </ul>
        </div>
      </div><!-- .media-body -->
    </div><!-- .media -->
  </div><!-- .app-user -->

  <div class="menubar-scroll">
    <div class="menubar-scroll-inner">
      <ul class="app-menu">
	      <?php if(isAllowedViewModule("dashboard")){ ?>
          <li class="<?php echo ($viewFolder == "dashboard_v") ? "active" : ""; ?>">
            <a href="<?php echo base_url("dashboard"); ?>">
              <i class="menu-icon zmdi zmdi-view-dashboard zmdi-hc-lg"></i>
              <span class="menu-text"><?php echo text("Ana Sayfa"); ?></span>            
            </a>
          </li>
         <?php } ?> 
        <?php if(isAllowedViewModule("settings")){ ?>        
        <li class="<?php echo ($viewFolder == "settings_v") ? "active" : ""; ?>">
          <a href="<?php echo base_url("settings"); ?>">
            <i class="menu-icon zmdi zmdi-settings zmdi-hc-lg"></i>
            <span class="menu-text"><?php echo text("Site Ayarları"); ?></span>
          </a>
        </li>
        <?php } ?> 
        <?php if(isAllowedViewModule("emailsettings")){ ?>

        <li class="has-submenu <?php if($viewFolder == "email_settings_v" || $viewFolder == "send_email_v" || $viewFolder == "send_many_email_v"){echo "active"; }?>">        
            <a href="javascript:void(0)" class="submenu-toggle">
              <i class="menu-icon zmdi zmdi-email zmdi-hc-lg"></i>
              <span class="menu-text"><?php echo text("E-Posta İşlemleri"); ?></span>
              <i class="menu-caret zmdi zmdi-hc-sm zmdi-chevron-right"></i>
            </a>

            <ul class="submenu"<?php echo ($viewFolder == "send_email_v" || $viewFolder == "email_settings_v" || $viewFolder == "send_many_email_v") ? "style = 'display: block;'" : ""; ?>>
              <li class="<?php echo ($viewFolder == "send_email_v") ? "active" : ""; ?>">
               <a href="<?php echo base_url("send_email"); ?>">       
                <span class="menu-text"><?php echo text("Mail Gönder"); ?></span>
               </a>     
              </li>
              <li class="<?php echo ($viewFolder == "send_many_email_v") ? "active" : ""; ?>">
                <a href="<?php echo base_url("send_many_email"); ?>">       
                <span class="menu-text"><?php echo text("Toplu Mail Gönder"); ?></span>
                </a>      
              </li>
              <li class="<?php echo ($viewFolder == "email_settings_v") ? "active" : ""; ?>">
                <a href="<?php echo base_url("emailsettings"); ?>">       
                <span class="menu-text"><?php echo text("E-Posta Ayarları"); ?></span>
                </a>      
              </li>              
            </ul>
        </li>

        <!--  
        <li class="<?php echo ($viewFolder == "email_settings_v") ? "active" : ""; ?>">
          <a href="<?php echo base_url("emailsettings"); ?>">
            <i class="menu-icon zmdi zmdi-email zmdi-hc-lg"></i>
            <span class="menu-text">E Posta Ayarları</span>
          </a>
        </li>
        -->
        <?php } ?>
        <?php if(isAllowedViewModule("galleries")){ ?>
        <li class="<?php echo ($viewFolder == "galleries_v") ? "active" : ""; ?>">
          <a href="<?php echo base_url("galleries"); ?>">
            <i class="menu-icon zmdi zmdi-apps zmdi-hc-lg"></i>
            <span class="menu-text"><?php echo text("Galeri İşlemleri"); ?></span>
          </a>
        </li>	
        <?php } ?> 
        <?php if(isAllowedViewModule("slides")){ ?>

        <li class="<?php echo ($viewFolder == "slides_v") ? "active" : ""; ?>">
          <a href="<?php echo base_url("slides"); ?>">
            <i class="menu-icon zmdi zmdi-layers zmdi-hc-lg"></i>
            <span class="menu-text"><?php echo text("Slider"); ?></span>
          </a>
        </li>
        <?php } ?> 
        <?php if(isAllowedViewModule("product")){ ?>

        <li class="has-submenu  <?php if($viewFolder == "product_v" || $viewFolder == "product_categories_v"){echo "active"; }?>">
          <a href="javascript:void(0)" class="submenu-toggle">
            <i class="menu-icon fa fa-asterisk"></i>
            <span class="menu-text"><?php echo text("Ürün İşlemleri"); ?></span>
            <i class="menu-caret zmdi zmdi-hc-sm zmdi-chevron-right"></i>
          </a>
          <ul class="submenu"<?php echo ($viewFolder == "product_categories_v" || $viewFolder == "product_v") ? "style = 'display: block;'" : ""; ?>>
            <li class="<?php echo ($viewFolder == "product_categories_v") ? "active" : ""; ?>">
      			  <a href="<?php echo base_url("product_categories"); ?>">				
      				<span class="menu-text"><?php echo text("Ürün Kategorileri"); ?></span>
      			  </a>			
      			</li>

            <li class="<?php echo ($viewFolder == "product_v") ? "active" : ""; ?>">
      			  <a href="<?php echo base_url("product"); ?>">				
      				<span class="menu-text"><?php echo text("Ürünler"); ?></span>
      			  </a>			
      			</li>
          </ul>
        </li>		
        <?php } ?> 
        <?php if(isAllowedViewModule("services")){ ?>

        <li class="<?php echo ($viewFolder == "services_v") ? "active" : ""; ?>">
          <a href="<?php echo base_url("services"); ?>">
            <i class="menu-icon fa fa-cutlery"></i>
            <span class="menu-text"><?php echo text("Hizmetlerimiz"); ?></span>
          </a>
        </li>
        <?php } ?> 
        <?php if(isAllowedViewModule("portfolio")){ ?>

  	   	<li class="has-submenu <?php if($viewFolder == "portfolio_v" || $viewFolder == "portfolio_categories_v"){echo "active"; }?>">        
            <a href="javascript:void(0)" class="submenu-toggle">
              <i class="menu-icon fa fa-asterisk"></i>
              <span class="menu-text"><?php echo text("Portfolyo İşlemleri"); ?></span>
              <i class="menu-caret zmdi zmdi-hc-sm zmdi-chevron-right"></i>
            </a>

            <ul class="submenu"<?php echo ($viewFolder == "portfolio_categories_v" || $viewFolder == "portfolio_v") ? "style = 'display: block;'" : ""; ?>>
              <li class="<?php echo ($viewFolder == "portfolio_categories_v") ? "active" : ""; ?>">
  			       <a href="<?php echo base_url("portfolio_categories"); ?>">				
  			      	<span class="menu-text"><?php echo text("Portfolyo Kategorileri"); ?></span>
  			       </a>			
  		        </li>
              <li class="<?php echo ($viewFolder == "portfolio_v") ? "active" : ""; ?>">
        			  <a href="<?php echo base_url("portfolio"); ?>">				
        				<span class="menu-text"><?php echo text("Portfolyolar"); ?></span>
        			  </a>			
        			</li>
            </ul>
        </li>	
        <?php } ?> 
        <?php if(isAllowedViewModule("news")){ ?>

        <li class="<?php echo ($viewFolder == "news_v") ? "active" : ""; ?>">
          <a href="<?php echo base_url("news"); ?>">
            <i class="menu-icon fa fa-newspaper-o"></i>
            <span class="menu-text"><?php echo text("Haberler"); ?></span>
          </a>
        </li>	
        <?php } ?> 

        <?php if(isAllowedViewModule("courses")){ ?>

        <li class="<?php echo ($viewFolder == "courses_v") ? "active" : ""; ?>">
          <a href="<?php echo base_url("courses"); ?>">
            <i class="menu-icon fa fa-calendar"></i>
            <span class="menu-text"><?php echo text("Eğitimler"); ?></span>
          </a>
        </li>	
        <?php } ?> 

        <?php if(isAllowedViewModule("references")){ ?>

        <li class="<?php echo ($viewFolder == "references_v") ? "active" : ""; ?>">
          <a href="<?php echo base_url("references"); ?>">
            <i class="menu-icon zmdi zmdi-check zmdi-hc-lg"></i>
            <span class="menu-text"><?php echo text("Referanslar"); ?></span>
          </a>
        </li>
        <?php } ?> 

        <?php if(isAllowedViewModule("brands")){ ?>

        <li class="<?php echo ($viewFolder == "brands_v") ? "active" : ""; ?>">
          <a href="<?php echo base_url("brands"); ?>">
            <i class="menu-icon zmdi zmdi-puzzle-piece zmdi-hc-lg"></i>
            <span class="menu-text"><?php echo text("Markalar"); ?></span>
          </a>
        </li>	
        <?php } ?> 

        <?php if(isAllowedViewModule("users")){ ?>

        <li class="<?php echo ($viewFolder == "users_v") ? "active" : ""; ?>">
          <a href="<?php echo base_url("users"); ?>">
            <i class="menu-icon fa fa-user-secret"></i>
            <span class="menu-text"><?php echo text("Kullanıcılar"); ?></span>
          </a>
        </li>	
        <?php } ?> 
        <?php if(isAllowedViewModule("user_roles")){ ?>

        <li class="<?php echo ($viewFolder == "user_roles_v") ? "active" : ""; ?>">
          <a href="<?php echo base_url("user_roles"); ?>">
            <i class="menu-icon fa fa-eye"></i>
            <span class="menu-text"><?php echo text("Kullanıcı Rolü"); ?></span>
          </a>
        </li>
        <?php } ?> 
        <?php if(isAllowedViewModule("members")){ ?>

        <li class="<?php echo ($viewFolder == "members_v") ? "active" : ""; ?>">
          <a href="<?php echo base_url("members"); ?>">
            <i class="menu-icon zmdi zmdi-accounts-alt zmdi-hc-fw"></i>
            <span class="menu-text"><?php echo text("Aboneler"); ?></span>
          </a>
        </li>
        <?php } ?> 
        <?php if(isAllowedViewModule("testimonials")){ ?>

        <li class="<?php echo ($viewFolder == "testimonials_v") ? "active" : ""; ?>">
          <a href="<?php echo base_url("testimonials"); ?>">
            <i class="menu-icon fa fa-comments"></i>
            <span class="menu-text"><?php echo text("Ziyaretçi Notları"); ?></span>
          </a>
        </li> 
        <?php } ?> 
        <?php if(isAllowedViewModule("popups")){ ?>

        <li class="<?php echo ($viewFolder == "popups_v") ? "active" : ""; ?>">
          <a href="<?php echo base_url("popups"); ?>">
            <i class="menu-icon zmdi zmdi-lamp zmdi-hc-lg"></i>
            <span class="menu-text"><?php echo text("Popup"); ?></span>
          </a>
        </li>
		    <?php } ?> 
     </ul><!-- .app-menu -->
    </div><!-- .menubar-scroll-inner -->
  </div><!-- .menubar-scroll -->
</aside>
<!--========== END app aside -->