			
				<?php if(empty($item_images)){ ?>
				<div class="alert alert-info">						
					<p>Burada herhangi bir resim bulunmamaktadır. </p>
				</div>	
				<?php }else { ?>				
			
				<table class="table table-bordered table-striped table-hover pictures-list">
					<thead>
						<th class="order"><i class="fa fa-reorder"></i></th>
						<th>#id</th>
						<th>Göresel</th>
						<th>Resim Adı</th>
						<th>Durumu</th>
						<th>Kapak Fotoğrafı</th>
						<th>İşlem</th>
					</thead>
					<tbody class="sortable" data-url = "<?php echo base_url("portfolio/imageRankSetter"); ?>">
						<?php foreach($item_images as $image){ ?>
						<tr id="ord-<?php echo $image->id; ?>">
							<td class="order"><i class="fa fa-reorder"></i></td>
							<td class="w100 text-center">#<?php echo $image->id; ?></td>
							<td class="w100 text-center">
								<img src="<?php echo get_picture($viewFolder, $image->img_url, "255x157"); ?>"
									width="50" 
									class="img-responsive portfolio_img"
									alt="<?php echo $image->img_url; ?>">
								</img>							
							</td>
							<td class="text-center"><?php echo $image->img_url; ?></td>
							<td class="w100 text-center">
								<input 
									class="isActive"
									type="checkbox"
									data-url = "<?php echo base_url("portfolio/imageIsActiveSetter/$image->id"); ?>"
									data-switchery
									data-color="#10c469"
									<?php echo ($image->isActive) ? "checked" : ""; ?>
								 />						
							</td>
							<td class="w100 text-center">
								<input 
									class="isCover"
									type="checkbox"
									data-url = "<?php echo base_url("portfolio/isCoverSetter/$image->id/$image->portfolio_id"); ?>"
									data-switchery
									data-color="#ff5b5b"
									<?php echo ($image->isCover) ? "checked" : ""; ?>
								 />						
							</td>							
							<td class="w100 text-center">
								<button 
									data-url="<?php echo base_url("portfolio/imageDelete/$image->id/$image->portfolio_id"); ?>"
									class="btn btn-sm btn-danger btn-outline remove-btn btn-block">
									<i class="fa fa-trash"></i> Sil
								</button>						
							</td>
						</tr>
						<?php } ?>
						
					</tbody>
				</table>
				
				<?php } ?>