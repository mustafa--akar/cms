<div class="row">
	<div class="col-md-12">
		<h4 class="m-b-lg">
			Portfolyo Listesi
			<?php if(isAllowedWriteModule()){ ?>
				<a href="<?php echo base_url("portfolio/new_form"); ?>" class="btn btn-outline btn-primary btn-xs pull-right"><i class="fa fa-plus"></i>  Yeni Ekle</a>
			<?php } ?>	
		</h4>
	</div><!-- END column -->
	<div class="col-md-12">
		<div class="widget p-lg">
			<?php if(empty($items)){ ?>
			<div class="alert alert-info">				
				<h4 class="alert-title">Kayıt Bulunamadı</h4>
				<p>Burada herhangi bir kayıt bulunmamaktadır. Eklemek için lütfen <a href="<?php echo base_url("portfolio/new_form"); ?>">tıklayınız</a> </p>
			</div>	
			<?php }else { ?>	
			<table class="table table-striped table-hover content_container table-bordered">
				<thead>
					<th class="order"><i class="fa fa-reorder"></i></th>
					<th class="w50 text-center">#id</th>
					<th>Başlık</th>											
					<th>Kategori</th>  
					<th>Müşteri</th> 
					<th>Bitiş Tarihi</th> 
					<th>Durum</th>
					<th>İşlem</th>
				</thead>
				<tbody class="sortable" data-url = "<?php echo base_url("portfolio/rankSetter"); ?>">
					<?php foreach($items as $item){ ?>
					<tr id="ord-<?php echo $item->id; ?>">
						<td class="order"><i class="fa fa-reorder"></i></td>
						<td class="w50 text-center"><?php echo '#'.$item->id; ?></td>
						<td class="text-center"><?php echo $item->title; ?></td>											
						<td class="text-center"><?php echo get_category_title($item->category_id, "portfolio"); ?></td> 
						<td class="text-center"><?php echo $item->client; ?></td> 
						<td class="text-center"><?php echo get_readable_date($item->finishedAt); ?></td> 
						<td class="text-center">							
							<input 
								class="isActive"
								type="checkbox"
								data-url = "<?php echo base_url("portfolio/isActiveSetter/$item->id"); ?>"
								data-switchery
								data-color="#10c469"
								<?php echo ($item->isActive) ? "checked" : "" ; ?> />
						</td>
						<td class="text-center">
							<?php if(isAllowedDeleteModule()){ ?>
								<button 
									data-url="<?php echo base_url("portfolio/delete/$item->id"); ?>"
									class="btn btn-sm btn-danger btn-outline remove-btn">
									<i class="fa fa-trash"></i> Sil
								</button>
							<?php } ?>
							<?php if(isAllowedUpdateModule()){ ?>	
								<a href="<?php echo base_url("portfolio/update_form/$item->id"); ?>" class="btn btn-sm btn-warning btn-outline"><i class="fa fa-pencil-square-o"></i> Düzenle</a>
								<a href="<?php echo base_url("portfolio/image_form/$item->id"); ?>" class="btn btn-sm btn-dark btn-outline"><i class="fa fa-image"></i> Resimler</a>
							<?php } ?>	
						</td>						
					</tr>
					<?php } ?>
				</tbody>
			</table>
			<?php } ?>
		</div><!-- .widget -->
	</div><!-- END column -->
</div>