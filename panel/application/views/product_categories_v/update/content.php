<div class="row">
	<div class="col-md-12">
		<h4 class="m-b-lg">
			<?php echo "<b>$item->title</b> kaydını düzenliyorsunuz";  ?>	
		</h4>
	</div><!-- END column -->
	<div class="col-md-12">
		<div class="widget">			
			<div class="widget-body">

				<form action="<?php echo base_url("product_categories/update/$item->id"); ?>" method="post">
					<div class="form-group">
					<div class="form-group">
						<label><?php echo text("Dili Seçiniz") ?></label>
						<select class="form-control" name="lang">
							<option <?php echo ($item->lang == "tr") ? "selected" : "";  ?>
							 value="tr"><?php echo text("Türkçe"); ?>
							<option <?php echo ($item->lang == "de") ? "selected" : "";  ?>
							 value="de"><?php echo text("Almanca"); ?>
							<option<?php echo ($item->lang == "en") ? "selected" : "";  ?>
						     value="en"><?php echo text("İngilizce"); ?>
						</select>
					</div>						
						<label>Başlık</label>
						<input class="form-control" placeholder="Başlık" name="title" value="<?php echo $item->title; ?>">
						<?php if(isset($form_error)){ ?>
							<small class="input-form-error"><?php echo form_error("title"); ?></small>
						<?php } ?>
					</div>

					<?php if(count($categories)> 0){  ?>	
						<div class="form-group">
							<label>Varsa üst kategori seçiniz</label>
							<select class="form-control" name="parent_id">
								<option value="0">Ana Kategori</option>
								<?php foreach($categories as $category){ 
									 $parent_id = (isset($form_error)) ? set_value("parent_id") : ""; 
									 if($category->id != $item->id && $category->parent_id != $item->id){ ?>
										<option 
										<?php echo ($category->id == $item->parent_id) ? "selected" : ""; ?>
										value="<?php echo $category->id; ?>"><?php echo $category->title; ?></option>
									 <?php }
									} ?>
							</select>
						</div>						
					<?php } ?>						
				
					<button type="submit" class="btn btn-primary btn-md btn-outline">Güncelle</button>
					<a href="<?php echo base_url("product_categories"); ?>" class="btn btn-md btn-danger btn-outline">İptal</a>
				</form>
			</div><!-- .widget-body -->
		</div><!-- .widget -->
	</div><!-- END column -->
</div>