<div class="row">
	<div class="col-md-12">
		<h4 class="m-b-lg">
			Ürünler Kategori Listesi
			<?php if(isAllowedWriteModule()){ ?>
				<a href="<?php echo base_url("product_categories/new_form"); ?>" class="btn btn-outline btn-primary btn-xs pull-right"><i class="fa fa-plus"></i>  Yeni Ekle</a>
			<?php } ?>	
		</h4>
	</div><!-- END column -->
	<div class="col-md-12">
		<div class="widget p-lg">
			<?php if(empty($items)){ ?>
			<div class="alert alert-info">				
				<h4 class="alert-title">Kayıt Bulunamadı</h4>
				<p>Burada herhangi bir kayıt bulunmamaktadır. Eklemek için lütfen <a href="<?php echo base_url("product_categories/new_form"); ?>">tıklayınız</a> </p>
			</div>	
			<?php }else { ?>
			
		
	<!--		
  
  <button type="button" id="expander">expand</button>
  <button type="button" id="collapser">collapse</button>
  <button type="button" id="open1">open 1</button>
  <button type="button" id="close1">close 1</button>
  
  -->
  <table id="basic" class="table table-striped table-hover content_container table-bordered" border="1">
  
		<thead>				
			<th>Başlık</th>	
			<th class="w50 text-center">#id</th>	
			<th>Durum</th>
			<th>İşlem</th>
		</thead> 
		<tbody>
		
			<?php echo drawElements($tree); ?>	
			
		</tbody>	

  </table>				
			<?php } ?>
			
		</div><!-- .widget -->
	</div><!-- END column -->
</div>

