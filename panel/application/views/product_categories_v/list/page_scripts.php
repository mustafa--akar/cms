
<script src="<?php echo base_url("assets"); ?>/assets/js/jquery-simple-tree-table.js"></script>

  <script>
  $('#basic').simpleTreeTable({
    expander: $('#expander'),
    collapser: $('#collapser'),
    store: 'session',
    storeKey: 'simple-tree-table-basic'
  });
  $('#open1').on('click', function() {
    $('#basic').data('simple-tree-table').openByID("1");
  });
  $('#close1').on('click', function() {
    $('#basic').data('simple-tree-table').closeByID("1");
  });
  </script>