<div class="row">
	<div class="col-md-12">
		<h4 class="m-b-lg">
			<?php echo "<b>$item->title</b> kaydını düzenliyorsunuz";  ?>	
		</h4>
	</div><!-- END column -->
	<div class="col-md-12">
		<div class="widget">			
			<div class="widget-body">

				<form action="<?php echo base_url("courses/update/$item->id"); ?>" method="post" enctype="multipart/form-data">
					<div class="form-group">
						<label><?php echo text("Dili Seçiniz") ?></label>
						<select class="form-control" name="lang">
							<option <?php echo ($item->lang == "tr") ? "selected" : "";  ?>
							 value="tr"><?php echo text("Türkçe"); ?>
							<option <?php echo ($item->lang == "de") ? "selected" : "";  ?>
							 value="de"><?php echo text("Almanca"); ?>
							<option<?php echo ($item->lang == "en") ? "selected" : "";  ?>
						     value="en"><?php echo text("İngilizce"); ?>
						</select>
					</div>					
					<div class="form-group">
						<label>Başlık</label>
						<input class="form-control" placeholder="Başlık" name="title" value="<?php echo $item->title; ?>">
						<?php if(isset($form_error)){ ?>
							<small class="input-form-error"><?php echo form_error("title"); ?></small>
						<?php } ?>
					</div>
					<div class="form-group">
						<label>Açıklama</label>
						<textarea name="description" class="m-0" data-plugin="summernote" data-options="{height: 250}">
							<?php echo $item->description; ?>
						</textarea>
					</div>			
					<div class="row">
                        <div class="col-md-4">
                            <label for="datetimepicker1">Eğitim Tarihi</label>
                            <input type="hidden" value="<?php echo $item->event_date; ?>" name="event_date" id="datetimepicker1" data-plugin="datetimepicker" data-options="{ inline: true, viewMode: 'days', format: 'YYYY-MM-DD HH:mm:ss' }"></input>
                        </div><!-- END column -->
						<div class="col-md-1 image_upload_container">
							<img src="<?php echo get_picture($viewFolder, $item->img_url, "255x157"); ?>" class="img-responsive" width="75px" alt="">
						</div>
						<div class="col-md-7 form-group image_upload_container">
							<label>Görsel Seçiniz</label>
							<input type="file" name="img_url" class="form-control">
						</div>							
					</div>				
				
					<button type="submit" class="btn btn-primary btn-md btn-outline">Güncelle</button>
					<a href="<?php echo base_url("courses"); ?>" class="btn btn-md btn-danger btn-outline">İptal</a>
				</form>
			</div><!-- .widget-body -->
		</div><!-- .widget -->
	</div><!-- END column -->
</div>