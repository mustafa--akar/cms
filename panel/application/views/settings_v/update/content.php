<div class="row">
	<div class="col-md-12">
		<h4 class="m-b-lg">
			<?php echo "<b>$item->company_name</b> kaydını düzenliyorsunuz";  ?>	
		</h4>
	</div><!-- END column -->
	<div class="col-md-12">
		<form action="<?php echo base_url("settings/update/$item->id"); ?>" method="post" enctype="multipart/form-data">
			<div class="widget">
				<div class="m-b-lg nav-tabs-horizontal">
					<!-- tabs list -->
					<ul class="nav nav-tabs" role="tablist">
						<li role="presentation" class="active"><a href="#tab-1" aria-controls="tab-1" role="tab" data-toggle="tab">Site Bilgileri</a></li>
						<li role="presentation"><a href="#tab-2" aria-controls="tab-2" role="tab" data-toggle="tab">Adres Bilgileri</a></li>
						<li role="presentation"><a href="#tab-6" aria-controls="tab-6" role="tab" data-toggle="tab">Hakkımızda</a></li>		
				    	<li role="presentation"><a href="#tab-8" aria-controls="tab-8" role="tab" data-toggle="tab">Ana Sayfa</a></li>											
						<li role="presentation"><a href="#tab-3"  aria-controls="tab-3" role="tab" data-toggle="tab">Misyon</a></li>
						<li role="presentation"><a href="#tab-4"  aria-controls="tab-4" role="tab" data-toggle="tab">Vizyon</a></li>
						<li role="presentation"><a href="#tab-9"  aria-controls="tab-9" role="tab" data-toggle="tab">Slogan</a></li>
						<li role="presentation"><a href="#tab-5"  aria-controls="tab-5" role="tab" data-toggle="tab">Sosyal Medya</a></li>
						<li role="presentation"><a href="#tab-7"  aria-controls="tab-7" role="tab" data-toggle="tab">Logo</a></li>						
					</ul><!-- .nav-tabs -->					
					
					<!-- Tab panes -->
					<div class="tab-content p-md">
						<div role="tabpanel" class="tab-pane in active fade" id="tab-1">
						
							<div class="row">
								<div class="form-group col-md-8">
									<label>Şirket Adı</label>
									<input class="form-control" placeholder="Şirketin yada sitenizin adı" name="company_name" value="<?php echo isset($form_error)  ? set_value("company_name") : $item->company_name ;?>">
									<?php if(isset($form_error)){ ?>
										<small class="input-form-error"><?php echo form_error("company_name"); ?></small>
									<?php } ?>
								</div>							
							</div>

							<div class="row">
								<div class="form-group col-md-4">
									<label>Telefon 1</label>
									<input class="form-control" placeholder="Telefon numaranız" name="phone_1" value="<?php echo isset($form_error)  ? set_value("phone_1") : $item->phone_1 ;?>">
									<?php if(isset($form_error)){ ?>
										<small class="input-form-error"><?php echo form_error("phone_1"); ?></small>
									<?php } ?>
								</div>	
								<div class="form-group col-md-4">
									<label>Telefon 2</label>
									<input class="form-control" placeholder="Diğer telefon numaranız" name="phone_2" value="<?php echo isset($form_error)  ? set_value("phone_2") : $item->phone_2 ;?>">		
								</div>								
							</div>	

							<div class="row">
								<div class="form-group col-md-4">
									<label>Fax 1</label>
									<input class="form-control" placeholder="Fax numaranız" name="fax_1" value="<?php echo isset($form_error)  ? set_value("fax_1") : $item->fax_1 ;?>">					
								</div>	
								<div class="form-group col-md-4">
									<label>Fax 2</label>
									<input class="form-control" placeholder="Diğer Fax numaranız" name="fax_2" value="<?php echo isset($form_error)  ? set_value("fax_2") : $item->fax_2 ;?>">				
								</div>								
							</div>								

						</div><!-- .tab-pane  -->

						<div role="tabpanel" class="tab-pane fade p-xs" id="tab-2">
							<div class="row">
								<div class="form-group">
									<label>Adres Bilgisi</label>
									<textarea name="address" class="m-0" data-plugin="summernote" data-options="{height: 250}"><?php echo isset($form_error)  ? set_value("address") : $item->address ;?></textarea>
								</div>						
							</div>
							<div class="row">
								<div class="form-group col-md-4">
									<label>Adres Enlem Bilgisi</label>
									<input class="form-control" placeholder="Lat verisini giriniz" name="lat" value="<?php echo isset($form_error)  ? set_value("lat") : $item->lat ;?>">					
								</div>	
								<div class="form-group col-md-4">
									<label>Adresin Boylan Bilgisi</label>
									<input class="form-control" placeholder="Long verisini giriniz" name="long" value="<?php echo isset($form_error)  ? set_value("long") : $item->long ;?>">				
								</div>								
							</div>							
						</div><!-- .tab-pane  -->

						<div role="tabpanel" class="tab-pane fade p-xs" id="tab-6">
							<div class="row">
								<div class="form-group">
									<label>Hakkımızda</label>
									<textarea name="about_us" class="m-0" data-plugin="summernote" data-options="{height: 250}"><?php echo isset($form_error)  ? set_value("about_us") : $item->about_us ;?></textarea>
								</div>						
							</div>
						</div><!-- .tab-pane  -->
						<div role="tabpanel" class="tab-pane fade p-xs" id="tab-8">
							<div class="row">
								<div class="form-group">
									<?php if($galleries){ ?>									
										<label>Ana Sayfada Text Yanında Görüntülenmesini istediğiniz Resim Galerisini Seçiniz</label>									
										<select class="form-control" name="homepage_gallery_id">
											<?php foreach($galleries as $gallery){ ?>
												<?php $homepage_gallery_id = (isset($form_error)) ? set_value("homepage_gallery_id") : $item->homepage_gallery_id; ?>
												<option 
													<?php echo ($gallery->id == $homepage_gallery_id) ? "selected" : ""; ?>
													value="<?php echo $gallery->id; ?>"><?php echo $gallery->title; ?></option>
											<?php } ?>
										</select>
									<?php }else{ ?>
										<div class="alert alert-warning" role="alert">
												<strong>Uyarı! </strong>
												<span>Önce galeriler modülünde bir resim galerisi ekleyiniz .</span>
												<a href="<?php echo base_url("galleries/new_form"); ?>" class="alert-link">Yeni Galeri Ekle</a>
										</div>	
									<?php } ?>										
								</div>	
																
								<div class="form-group">
									<label>Ana Sayfada Sleider Altındaki Alanda Görünecek Yazı</label>
									<textarea name="homepage_text" class="m-0" data-plugin="summernote" data-options="{height: 250}"><?php echo isset($form_error)  ? set_value("homepage_text") : $item->homepage_text ;?></textarea>
								</div>	
								<div class="form-group">
									<label>Ana Sayfada Referanslar Kısmında Görünecek Yazı</label>
									<input class="form-control" placeholder="Referanslar Açıklama Alanı" name="homepage_references_description" value="<?php echo isset($form_error)  ? set_value("homepage_references_description") : $item->homepage_references_description ;?>">		
								</div>

				

							</div>
						</div><!-- .tab-pane  -->							
						<div role="tabpanel" class="tab-pane fade p-xs" id="tab-3">
							<div class="row">
								<div class="form-group">
									<label>Misyonunuz</label>
									<textarea name="mission" class="m-0" data-plugin="summernote" data-options="{height: 250}"><?php echo isset($form_error)  ? set_value("mission") : $item->mission ;?></textarea>
								</div>						
							</div>
						</div><!-- .tab-pane  -->						
						<div role="tabpanel" class="tab-pane fade p-xs" id="tab-4">
							<div class="row">
								<div class="form-group">
									<label>Vizyonumuz</label>
									<textarea name="vision" class="m-0" data-plugin="summernote" data-options="{height: 250}"><?php echo isset($form_error)  ? set_value("vision") : $item->vision ;?></textarea>
								</div>						
							</div>
						</div><!-- .tab-pane  -->
						<div role="tabpanel" class="tab-pane fade p-xs" id="tab-9">
							<div class="row">
								<div class="form-group">
									<label>Sloganımız</label>
									<textarea name="motto" class="m-0" data-plugin="summernote" data-options="{height: 250}"><?php echo isset($form_error)  ? set_value("motto") : $item->motto ;?></textarea></textarea>
								</div>						
							</div>
							<div class="row">
								<div class="form-group col-md-12">
									<label>Kısa Sloganı Yazınız</label>
									<input class="form-control" placeholder="Şirketinize ait kısa bir slogan yazınız" name="short_motto" value="<?php echo isset($form_error)  ? set_value("short_motto") : $item->short_motto ;?>">
									<?php if(isset($form_error)){ ?>
										<small class="input-form-error"><?php echo form_error("short_motto"); ?></small>
									<?php } ?>
								</div>							
							</div>							
						</div><!-- .tab-pane  -->							
						<div role="tabpanel" class="tab-pane fade" id="tab-5">
							<div class="row">
								<div class="form-group col-md-8">
									<label>E-posta adresiniz</label>
									<input class="form-control" placeholder="Şirketinize ait E-posta adresiniz" name="email" value="<?php echo isset($form_error)  ? set_value("email") : $item->email ;?>">
									<?php if(isset($form_error)){ ?>
										<small class="input-form-error"><?php echo form_error("email"); ?></small>
									<?php } ?>
								</div>							
							</div>
							<div class="row">
								<div class="form-group col-md-4">
									<label>Facebook</label>
									<input class="form-control" placeholder="Facebook Adresiniz" name="facebook" value="<?php echo isset($form_error)  ? set_value("facebook") : $item->facebook ;?>">		
								</div>	
								<div class="form-group col-md-4">
									<label>Twitter</label>
									<input class="form-control" placeholder="Twitter Adresiniz" name="twitter" value="<?php echo isset($form_error)  ? set_value("twitter") : $item->twitter ;?>">			
								</div>									
							</div>
							<div class="row">
								<div class="form-group col-md-4">
									<label>Instagram</label>
									<input class="form-control" placeholder="Instagram Adresiniz" name="instagram" value="<?php echo isset($form_error)  ? set_value("instagram") : $item->instagram ;?>">		
								</div>	
								<div class="form-group col-md-4">
									<label>Linkedin Adresiniz</label>
									<input class="form-control" placeholder="Linkedin Adresiniz" name="linkedin" value="<?php echo isset($form_error)  ? set_value("linkedin") : $item->linkedin ;?>">		
								</div>										
							</div>							
						</div><!-- .tab-pane  -->
						<div role="tabpanel" class="tab-pane fade p-xs" id="tab-7">
							<div class="row">
								<div class="col-md-3">
									<img src="<?php echo get_picture($viewFolder, $item->logo, "150x35"); ?>" alt="<?php echo $item->company_name; ?>" class="img-responsive">
								</div>
								<div class="form-group image_upload_container col-md-6">
									<label>Görsel Seçiniz</label>
									<input type="file" name="logo" class="form-control">
								</div>									
							</div>
						</div><!-- .tab-pane  -->						
					</div><!-- .tab-content  -->
				</div><!-- .nav-tabs-horizontal -->
			</div><!-- .widget -->
			<div class="widget">
				<div class="widget-body">					
					<button type="submit" class="btn btn-primary btn-md">Güncelle</button>
					<a href="<?php echo base_url("settings"); ?>" class="btn btn-md btn-danger">İptal</a>				
				</div>
			</div>
		</form>				
	</div><!-- END column -->
</div>