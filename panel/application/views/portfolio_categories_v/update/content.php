<div class="row">
	<div class="col-md-12">
		<h4 class="m-b-lg">
			<?php echo "<b>$item->title</b> kaydını düzenliyorsunuz";  ?>	
		</h4>
	</div><!-- END column -->
	<div class="col-md-12">
		<div class="widget">			
			<div class="widget-body">
				<form action="<?php echo base_url("portfolio_categories/update/$item->id"); ?>" method="post">
					<div class="form-group">
						<label><?php echo text("Dili Seçiniz") ?></label>
						<select class="form-control" name="lang">
							<option <?php echo ($item->lang == "tr") ? "selected" : "";  ?>
							 value="tr"><?php echo text("Türkçe"); ?>
							<option <?php echo ($item->lang == "de") ? "selected" : "";  ?>
							 value="de"><?php echo text("Almanca"); ?>
							<option<?php echo ($item->lang == "en") ? "selected" : "";  ?>
						     value="en"><?php echo text("İngilizce"); ?>
						</select>
					</div>					
					<div class="form-group">
						<label>Başlık</label>
						<input class="form-control" placeholder="Başlık" name="title" value="<?php echo $item->title; ?>">
						<?php if(isset($form_error)){ ?>
							<small class="input-form-error"><?php echo form_error("title"); ?></small>
						<?php } ?>
					</div>
						
				
					<button type="submit" class="btn btn-primary btn-md btn-outline">Güncelle</button>
					<a href="<?php echo base_url("portfolio_categories"); ?>" class="btn btn-md btn-danger btn-outline">İptal</a>
				</form>
			</div><!-- .widget-body -->
		</div><!-- .widget -->
	</div><!-- END column -->
</div>