<div class="row">
	<div class="col-md-12">
		<h4 class="m-b-lg">
			E-Posta Gönder	
		</h4>
	</div><!-- END column -->	
<div class="col-md-12">
	<div class="widget">			
		<div class="widget-body">
			<div class="panel panel-default new-message pt-20">						
					<form action="<?php echo base_url("send_many_email/send"); ?>" class="cms-email" method="post">
						<div class="alert alert-warning" role="alert">
								<strong>Hatırlatma! </strong>
								<span>Bu E-Posta otomatik olarak tüm abonelere gönderilecektir... .</span>
								<a href="<?php echo base_url("members"); ?>" class="alert-link">Abone Listesini Kontrol Et</a>
						</div>
						<div class="form-group">

							<input type="text" class="form-control" placeholder="<?php echo isset($form_error)  ? set_value("subject") : "Konu" ;?>" name="subject">
								<?php if(isset($form_error)){ ?>
									<small class="input-form-error"><?php echo form_error("subject"); ?></small>
								<?php } ?>						
						</div><!-- .form-group -->
						<div class="form-group">					
							<textarea name="message" class="m-0" data-plugin="summernote" data-options="{height: 250}">
								<?php echo isset($form_error)  ? set_value("message") : "" ;?>			
							</textarea>
							<?php if(isset($form_error)){ ?>
								<small class="input-form-error"><?php echo form_error("message"); ?></small>
							<?php } ?>						
						</div>	

						<div class="panel-footer clearfix">
							<div class="pull-right">												
								<button type="submit" class="btn btn-primary">Gönder <i class="fa fa-send"></i></button>
							</div>
						</div><!-- .panel-footer -->
					</form>
				</div>	
			</div>	
		</div>
	</div>
</div>
	