<div class="row">
	<div class="col-md-12">
		<h4 class="m-b-lg">
			<?php echo "<b>$item->email</b> kaydını düzenliyorsunuz";  ?>	
		</h4>
	</div><!-- END column -->
	<div class="col-md-12">
		<div class="widget">			
			<div class="widget-body">

				<form action="<?php echo base_url("members/update/$item->id"); ?>" method="post">
					<div class="form-group">
						<label>Başlık</label>
						<input class="form-control" placeholder="E-Posta Adresi" name="email" value="<?php echo $item->email; ?>">
						<?php if(isset($form_error)){ ?>
							<small class="input-form-error"><?php echo form_error("email"); ?></small>
						<?php } ?>
					</div>
						
				
					<button type="submit" class="btn btn-primary btn-md btn-outline">Güncelle</button>
					<a href="<?php echo base_url("members"); ?>" class="btn btn-md btn-danger btn-outline">İptal</a>
				</form>
			</div><!-- .widget-body -->
		</div><!-- .widget -->
	</div><!-- END column -->
</div>