<div class="row">
	<div class="col-md-12">
		<h4 class="m-b-lg">
			Yeni E-Posta Adresi Ekle			
		</h4>
	</div><!-- END column -->
	<div class="col-md-12">
		<div class="widget">			
			<div class="widget-body">

				<form action="<?php echo base_url("members/save"); ?>" method="post">
					<div class="form-group">
						<label>E-Posta Adresi</label>
						<input class="form-control" placeholder="E-Posta Adresi" name="email">
						<?php if(isset($form_error)){ ?>
							<small class="input-form-error"><?php echo form_error("email"); ?></small>
						<?php } ?>
					</div>					
				
					<button type="submit" class="btn btn-primary btn-md btn-outline">Kaydet</button>
					<a href="<?php echo base_url("members"); ?>" class="btn btn-md btn-danger btn-outline">İptal</a>
				</form>
			</div><!-- .widget-body -->
		</div><!-- .widget -->
	</div><!-- END column -->
</div>