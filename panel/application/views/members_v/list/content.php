<div class="row">
	<div class="col-md-12">
		<h4 class="m-b-lg">
			E-Posta Listesi
			<?php if(isAllowedWriteModule()){ ?>
				<a href="<?php echo base_url("members/new_form"); ?>" class="btn btn-outline btn-primary btn-xs pull-right"><i class="fa fa-plus"></i>  Yeni Ekle</a>
			<?php } ?>	
		</h4>
	</div><!-- END column -->
	<div class="col-md-12">
		<div class="widget p-lg">
			<?php if(empty($items)){ ?>
			<div class="alert alert-info">				
				<h4 class="alert-title">Kayıt Bulunamadı</h4>
				<p>Burada herhangi bir kayıt bulunmamaktadır. Eklemek için lütfen <a href="<?php echo base_url("members/new_form"); ?>">tıklayınız</a> </p>
			</div>	
			<?php }else { ?>	
			<table class="table table-striped table-hover content_container table-bordered">
				<thead>
					<th class=""><i class="fa fa-reorder"></i></th>
					<th class="w50 text-center">#id</th>
					<th>E-Posta Adresi</th>				
					<th>Durum</th>
					<th>İşlem</th>
				</thead>
				<tbody>
					<?php foreach($items as $item){ ?>
					<tr>
						<td class="text-center"><i class="fa fa-reorder"></i></td>
						<td class="w50 text-center"><?php echo '#'.$item->id; ?></td>
						<td class="text-center"><?php echo $item->email; ?></td>			
						<td class="text-center w100">							
							<input 
								class="isActive"
								type="checkbox"
								data-url = "<?php echo base_url("members/isActiveSetter/$item->id"); ?>"
								data-switchery
								data-color="#10c469"
								<?php echo ($item->isActive) ? "checked" : "" ; ?> />
						</td>
						<td class="text-center w200">
							<?php if(isAllowedDeleteModule()){ ?>
								<button 
									data-url="<?php echo base_url("members/delete/$item->id"); ?>"
									class="btn btn-sm btn-danger btn-outline remove-btn">
									<i class="fa fa-trash"></i> Sil
								</button>
						    <?php } ?>
							<?php if(isAllowedUpdateModule()){ ?>								
								<a href="<?php echo base_url("members/update_form/$item->id"); ?>" class="btn btn-sm btn-warning btn-outline"><i class="fa fa-pencil-square-o"></i> Düzenle</a>
							<?php } ?>								
						</td>						
					</tr>
					<?php } ?>
				</tbody>
			</table>
			<div class="text-center"><?php echo $links; ?></div>			
			<?php } ?>
		</div><!-- .widget -->
	</div><!-- END column -->
</div>