<?php

function convertToSeo($text)
{
		$turkce = array("ç", "Ç", "ğ", "Ğ", "ü", "Ü", "ö", "Ö", "ı", "İ", "ş", "Ş", ".", ",", "!", "'", "\"", " ", "?", "*", "_", "|", "=", "(", ")", "[", "]", "{", "}", "+", "-");
		$convert = array("c", "c", "g", "g", "u", "u", "o", "o", "i", "i", "s", "s", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-",);
		return strtolower(str_replace($turkce, $convert, $text));	
}

function get_readable_date($date)
{

    return strftime('%d %B %Y', strtotime($date));
}

function get_active_user()
{
	
	$t	= &get_instance();
	
	$user =  $t->session->userdata("user");
	
	if($user){
		return $user;
	}else{
		return false;
	}
}


function setUserRoles()
{

	$t	= &get_instance();

	$t->load->model("user_role_model");

	$user_roles = $t->user_role_model->get_all(
		array(
			"isActive"	=> 1
		)
	);

	$roles = [];

	foreach($user_roles as $role){
		$roles[$role->id] = $role->permissions;
	}

	$t->session->set_userdata("user_roles", $roles);
}


function get_user_roles()
{

	$t	= &get_instance();	

   // Eğer anında değişikliği görmek istiyorsan bunu aç yoksa giriş çıkış yaparak session un yenilenmesini sağlamalısın
    setUserRoles();     
   

	return $t->session->userdata("user_roles");
	
}

function getControllerList()
{
	$t = &get_instance();

	$controllers = array();
	$t->load->helper("file");

	$files = get_dir_file_info(APPPATH. "controllers", FALSE);

	foreach(array_keys($files) as $file){

		if($file !== "index.html"){

			$controllers[] = strtolower(str_replace(".php", "", $file));
		}
	}

	return $controllers;
}

function send_email($toEmail = "", $subject = "", $message = "", $ccEmail = "", $bccEmail = "")
{
	$t = &get_instance();
	
	$t->load->model("emailsettings_model");
	
	 
	$email_settings = $t->emailsettings_model->get(
		array(
			"isActive"	=> 1
		)
	);
	
	//print_r($email_settings);
	
	$config = array(
		"protocol"	=> $email_settings->protocol,
		"smtp_host"	=> $email_settings->host,
		"smtp_port"	=> $email_settings->port,
		"smtp_user"	=> $email_settings->user,
		"smtp_pass"	=> $email_settings->password,
		"starttls"	=> true,
		"charset"	=> "utf-8",
		"mailtype"	=> "html",
		"newline"	=> "\r\n",
		"wordwrap"	=> true
	);
	$t->load->library("email", $config);
	
	$t->email->from($email_settings->from, $email_settings->user_name);			
	$t->email->to($toEmail);			
	$t->email->cc($ccEmail);			
	$t->email->bcc($bccEmail);			
	$t->email->subject($subject);			
	$t->email->message($message);
	
	return $t->email->send();	
	
}

function get_settings()
{
	
	$t = &get_instance();
	
	$t->load->model("settings_model");
	
	if($t->session->userdata("settings")){
		$settings = $t->session->userdata("settings");
	}else{
		$settings = $t->settings_model->get();
		
		if(!$settings)
		{
			$settings = new stdClass;
			
			$settings->company_name = "Panel";
			$settings->logo         = "default"; 
		
		}	
		$t->session->set_userdata("settings", $settings);
	}	
	
	return $settings;
	
}

function get_category_title($category_id = 0, $category_type = "product")
{
	$t = &get_instance();
	
	if($category_type == "product"){
			
		$category = $t->load->product_category_model->get(
			array(
				"id"	=> $category_id
			)
		);	
	
	}else if($category_type == "portfolio"){
		
		$category = $t->load->portfolio_category_model->get(
			array(
				"id"	=> $category_id
			)
		);	
		
	}

	
	if($category){
		
		return $category->title;
		
	}else{
		
		return "<b style='color:red'> Tanımlı değil</b>";
		
	}
	
}


function drawElements($items)
{
	$t = &get_instance();
		
	foreach($items as $item)
	{
		
		$checked = ($item->isActive) ? "checked" : "";
		$url = base_url("product_categories/isActiveSetter/$item->id");
		$deleteUrl = base_url("product_categories/delete/$item->id");
		$updateUrl = base_url("product_categories/update_form/$item->id");
		
		if($item->parent_id){
			
			echo "    <tr data-node-id='$item->id' data-node-pid='$item->parent_id'>
						  <td>$item->title</td>
						  <td class='w50 text-center'>$item->id</td>
						  <td class='text-center w50'>
					       <input   class='isActive product-categories-isActive'
									type='checkbox'
									data-url = $url
									data-switchery
									data-color='#10c469'
									$checked  />						  
						  </td>
						  <td class='text-center w200'>
						  
								<button 
									data-url='$deleteUrl'
									class='btn btn-sm btn-danger btn-outline remove-btn product-cat-btn'>
									<i class='fa fa-trash'></i> Sil
								</button>
								<a href='$updateUrl' class='btn btn-sm btn-warning btn-outline product-cat-btn'><i class='fa fa-pencil-square-o'></i> Düzenle</a>						  
						  
						  </td>
						</tr>";		
						
		}else{
			
			echo "    <tr data-node-id='$item->id'>
						  <td>$item->title</td>	
						  <td class='w50 text-center'>$item->id</td>
						  <td class='text-center w50'>
					       <input   class='isActive product-categories-isActive'
									type='checkbox'
									data-url = $url
									data-switchery
									data-color='#10c469'
									$checked  />						  
						  </td>
						  <td class='text-center w200'>
						  
								<button 
									data-url='$deleteUrl'
									class='btn btn-sm btn-danger btn-outline remove-btn product-cat-btn'>
									<i class='fa fa-trash'></i> Sil
								</button>
								<a href='$updateUrl' class='btn btn-sm btn-warning btn-outline product-cat-btn'><i class='fa fa-pencil-square-o'></i> Düzenle</a>						  
						  
						  </td>
						</tr>";				
			
		}

		if(sizeof($item->children) > 0)
		{			
			drawElements($item->children);

		}
		
	}
	
}

// $_FILES["img_url"]["tmp_name"]

function upload_picture($file, $uploadPath, $width, $height, $name)
{
	$t = &get_instance();

	$t->load->library("simpleimagelib");
	ini_set('memory_limit', '-1');

	if(!is_dir("{$uploadPath}/{$width}x{$height}"))
	{
		mkdir("{$uploadPath}/{$width}x{$height}");
	}	

	$upload_error = false;
	try {
	  
	  $simpleImage = $t->simpleimagelib->get_simple_image_instance();

	  
	  $simpleImage
	    ->fromFile($file)
	    ->thumbnail($width,$height,'center')    
	    ->toFile("{$uploadPath}/{$width}x{$height}/$name",null, 75);      

	  
	} catch(Exception $err) {
	  
	  $error =  $err->getMessage();
	  $upload_error = true;
	}
	if($upload_error){
		echo $error;
	}else{
		return true;
	}	
}

function get_picture($path = "", $picture = "", $resolation = "50x50")
{

	if($picture != ""){
		//echo FCPATH."uploads/$path/$resolation/$picture"; die();	
		if(file_exists(FCPATH."uploads/$path/$resolation/$picture")){

			$picture = base_url("uploads/$path/$resolation/$picture");
		}else{

			$picture = base_url("assets/assets/images/no_image.png");
		}

	}else{
		
		$picture = base_url("assets/assets/images/no_image.png");
	}
	return $picture;

}

function get_page_list($page)
{
	$page_list = array(
		"home_v"           =>  "Ana Sayfa",
		"about_v"          =>  "Hakkımızda Sayfası",
		"news_list_v"      =>  "Haberler Sayfası",
		"galleries"        =>  "Galeri Sayfası",
		"portfolio_list_v" =>  "Portfolyo Sayfası",
		"reference_list_v" =>  "Referenslar Sayfası",
		"service_list_v"   =>  "Hizmetlerimiz Sayfası",
		"course_list_v"    =>  "Eğitimler Sayfası",
		"brand_list_v"     =>  "Markalar Sayfası",
		"contact_v"        =>  "İletişim Sayfası"
	);

	return (empty($page)) ? $page_list : $page_list[$page];
}

function text($text)
{
	$t = &get_instance();

	return ($t->lang->line($text)) ? $t->lang->line($text) : $text;
}


?>