<?php

	$lang["İşlemler"]    			 = "Einstellungen";
	$lang["Ana Sayfa"]   			 = "Homepage";
	$lang["Profilim"]     			 = "Mein Profil";
	$lang["Çıkış"]       			 = "Ausloggen";
	$lang["Site Ayarları"]     	     = "Site-Einstellungen";
	$lang["E-Posta İşlemleri"] 	     = "E-Mail-Transaktionen";
	$lang["Mail Gönder"]	   		 = "E-Mail senden";
	$lang["Toplu Mail Gönder"]		 = "Massen-E-Mail senden";
	$lang["E-Posta Ayarları"] 		 = "E-Mail-Einstellungen";
	$lang["Galeri İşlemleri"] 		 = "Galerie";
	$lang["Slider"]	  		    	 = "Slider";
	$lang["Ürün İşlemleri"]	    	 = "Produkte";
	$lang["Ürün Kategorileri"]		 = "Produktkategorien";
	$lang["Ürünler"]	  			 = "Produktliste";
	$lang["Hizmetlerimiz"]	  		 = "Unsere Leistungen";
	$lang["Portfolyo İşlemleri"]	 = "Portfolios";
	$lang["Portfolyo Kategorileri"]	 = "Portfolio-Kategorien";
	$lang["Portfolyolar"]	  		 = "Portfolioliste";
	$lang["Haberler"]	  		     = "Nachrichten";
	$lang["Eğitimler"]	  		     = "Kurse";
	$lang["Referanslar"]	  		 = "Referenzen";
	$lang["Markalar"]	  		     = "Marken";
	$lang["Kullanıcılar"]	  		 = "Nutzer";
	$lang["Kullanıcı Rolü"]	  		 = "Nutzerrolle";
	$lang["Aboneler"]	  		     = "Abonnenten";
	$lang["Ziyaretçi Notları"]	  	 = "Besucherrückmeldung";
	$lang["Popup"]	  				 = "Popup";
	$lang["Türkçe"]	  				 = "Türkisch";
	$lang["Almanca"]	  			 = "Deutsch";
	$lang["İngilizce"]	  			 = "Englisch";	
	$lang["İşlem Başarılı"]	  		 = "Erfolgreich";	
	$lang["Dili Seçiniz"]	  		 = "Wählen Sie Sprache";		
		
	$lang["Sitenin dili başarılı bir şekilde değiştirildi"]	  = "Die Sprache der Site wurde erfolgreich geändert";		

?>