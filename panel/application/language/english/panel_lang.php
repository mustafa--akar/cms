<?php

	$lang["İşlemler"]    			 = "Settings";
	$lang["Ana Sayfa"]   			 = "Home Page";
	$lang["Profilim"]     			 = "Profile";
	$lang["Çıkış"]       			 = "Log Out";
	$lang["Site Ayarları"]     	     = "Site settings";
	$lang["E-Posta İşlemleri"] 	     = "Email Transactions";
	$lang["Mail Gönder"]	   		 = "Send E-mail";
	$lang["Toplu Mail Gönder"]		 = "Send Bulk E-mail";
	$lang["E-Posta Ayarları"] 		 = "Email settings";
	$lang["Galeri İşlemleri"] 		 = "Galleries";
	$lang["Slider"]	  		    	 = "Slider";
	$lang["Ürün İşlemleri"]	    	 = "Products";
	$lang["Ürün Kategorileri"]		 = "Product categories";
	$lang["Ürünler"]	  			 = "Product list";
	$lang["Hizmetlerimiz"]	  		 = "Services";
	$lang["Portfolyo İşlemleri"]	 = "Portfolios";
	$lang["Portfolyo Kategorileri"]	 = "Portfolio categories";
	$lang["Portfolyolar"]	  		 = "Portfolio list";
	$lang["Haberler"]	  		     = "News";
	$lang["Eğitimler"]	  		     = "Courses";
	$lang["Referanslar"]	  		 = "References";
	$lang["Markalar"]	  		     = "Brands";
	$lang["Kullanıcılar"]	  		 = "User";
	$lang["Kullanıcı Rolü"]	  		 = "User role";
	$lang["Aboneler"]	  		     = "Subscribers";
	$lang["Ziyaretçi Notları"]	  	 = "Testimonials";
	$lang["Popup"]	  				 = "Popup";
	$lang["Türkçe"]	  				 = "Türkish";
	$lang["Almanca"]	  			 = "German";
	$lang["İngilizce"]	  			 = "English";		
	$lang["İşlem Başarılı"]	  		 = "Success";
	$lang["Dili Seçiniz"]	  		 = "Choose Language";		

	$lang["Sitenin dili başarılı bir şekilde değiştirildi"]	  = "The language of the site has been successfully changed";		

?>