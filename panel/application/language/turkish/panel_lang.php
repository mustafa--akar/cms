<?php

	$lang["İşlemler"]    			 = "İşlemler";
	$lang["Ana Sayfa"]   			 = "Ana Sayfa";
	$lang["Profilim"]     			 = "Profilim";
	$lang["Çıkış"]       			 = "Çıkış";
	$lang["Site Ayarları"]     	     = "Site Ayarları";
	$lang["E-Posta İşlemleri"] 	     = "E-Posta İşlemleri";
	$lang["Mail Gönder"]	   		 = "Mail Gönder";
	$lang["Toplu Mail Gönder"]		 = "Toplu Mail Gönder";
	$lang["E-Posta Ayarları"] 		 = "E-Posta Ayarları";
	$lang["Galeri İşlemleri"] 		 = "Galeri İşlemleri";
	$lang["Slider"]	  		    	 = "Slider";
	$lang["Ürün İşlemleri"]	    	 = "Ürün İşlemleri";
	$lang["Ürün Kategorileri"]		 = "Ürün Kategorileri";
	$lang["Ürünler"]	  			 = "Ürünler";
	$lang["Hizmetlerimiz"]	  		 = "Hizmetlerimiz";
	$lang["Portfolyo İşlemleri"]	 = "Portfolyo İşlemleri";
	$lang["Portfolyo Kategorileri"]	 = "Portfolyo Kategorileri";
	$lang["Portfolyolar"]	  		 = "Portfolyolar";
	$lang["Haberler"]	  		     = "Haberler";
	$lang["Eğitimler"]	  		     = "Eğitimler";
	$lang["Referanslar"]	  		 = "Referanslar";
	$lang["Markalar"]	  		     = "Markalar";
	$lang["Kullanıcılar"]	  		 = "Kullanıcılar";
	$lang["Kullanıcı Rolü"]	  		 = "Kullanıcı Rolü";
	$lang["Aboneler"]	  		     = "Aboneler";
	$lang["Ziyaretçi Notları"]	  	 = "Ziyaretçi Notları";
	$lang["Popup"]	  				 = "Popup";
	$lang["Türkçe"]	  				 = "Türkçe";
	$lang["Almanca"]	  			 = "Almanca";
	$lang["İngilizce"]	  			 = "İngilizce";
	$lang["İşlem Başarılı"]	  		 = "İşlem Başarılı";		
	$lang["Dili Seçiniz"]	  		 = "Dili Seçiniz";		
	$lang["Sitenin dili başarılı bir şekilde değiştirildi"]	  = "Sitenin dili başarılı bir şekilde değiştirildi";		

?>