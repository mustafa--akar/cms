<?php

class Services extends MY_CONTROLLER
{
		public $viewFolder = "";
		
		public function __construct()
		{
			parent::__construct();
			$this->viewFolder = "services_v";
			$this->load->model("service_model");

			if(!get_active_user()){
				redirect(base_url("login"));
			}			
			
			
		}
		public function index()
		{
			if(!(isAllowedViewModule())){
				redirect(base_url());
			}

			$viewData = new stdClass();
			
			/* Tablodan verilerin getirilmesi */
			$items = $this->service_model->get_all(
				array(
					"lang"	=> $this->session->userdata("lang")
				), "rank ASC"
			);
			
			/* View'e gönderilecek değişkenlerin set edilmesi */
			$viewData->items = $items;
			$viewData->viewFolder = $this->viewFolder;
			$viewData->subViewFolder = "list";			
			
			$this->load->view("{$viewData->viewFolder}/{$viewData->subViewFolder}/index", $viewData);
				
		}
		public function new_form()
		{		
			if(!(isAllowedWriteModule())){
				redirect(base_url($this->router->fetch_class()));
			}

			$viewData = new stdClass();
			
			/* View'e gönderilecek değişkenlerin set edilmesi */			
			$viewData->viewFolder = $this->viewFolder;
			$viewData->subViewFolder = "add";
			
			$this->load->view("{$viewData->viewFolder}/{$viewData->subViewFolder}/index", $viewData);	
		}
		public function save()
		{
			if(!(isAllowedWriteModule())){
				redirect(base_url($this->router->fetch_class()));
			}

			$this->load->library("form_validation");
			//kurallar yazılır		
			
			if($_FILES["img_url"]["name"] == "")
			{
				$alert = array(
					"title"		=> "İşlem Başarısız",
					"text"		=> "Lütfen Bir Görsel Seçiniz",
					"type"		=> "error"	
				);
				// İşlem sonucunu session'a yazma işlemi...
				$this->session->set_flashdata("alert", $alert);
				
				redirect(base_url("services/new_form"));	
				die();	
			}				
			
			$this->form_validation->set_rules("title", "Başlık", "required|trim");
			$this->form_validation->set_message(
				array(
					"required" => "{field} alanı doldurulmalıdır"
				)
			);
			//Form Validation Çalıştırılır
			$validate = $this->form_validation->run();
			//kontrol edilir
			if($validate)
			{
				
				// Upload Süreci
				// Dosya adı düzenlemesi;
				$ext = pathinfo($_FILES["img_url"]["name"], PATHINFO_EXTENSION);
				$file_name_stamm = convertToSeo(pathinfo($_FILES["img_url"]["name"], PATHINFO_FILENAME));
				$file_name = $file_name_stamm. "." .$ext;
				
				$file = $_FILES["img_url"]["tmp_name"];
				$uploadPath = "uploads/$this->viewFolder/";	

				
				$image_555x343 = upload_picture($file, $uploadPath, 555, 343, $file_name);
				$image_358x217 = upload_picture($file, $uploadPath, 358, 217, $file_name);
				
				if($image_555x343 && $image_358x217)
				{
						
					$insert = $this->service_model->add(
						array(
								"title"         => $this->input->post("title"),
								"description"   => $this->input->post("description"),
								"lang"		    => $this->input->post("lang"),
								"url"           => convertToSeo($this->input->post("title")),							
								"img_url"       => $file_name,								
								"rank"          => 0,
								"isActive"      => 1,
								"createdAt"     => date("Y-m-d H:i:s")
							)					
					);				
					if($insert)
					{
						$alert = array(
							"title"  => "İşlem Başarılı",
							"text"   => "Kayıt başarılı bir şekilde eklendi",
							"type"	 => "success"
						);					
					}else{
						$alert = array(
							"title"   => "İşlem Başarısızdır",
							"text"    => "Kayıt Eklenemedi", 
							"type"	 => "error"
						);										
					}

				}else{
					$alert = array(
						"title"   => "İşlem Başarısızdır",
						"text"    => "Görsel yüklenirken bir problem oluştu", 
						"type"	  => "error"
					);	

					//İşlemin Sonucunu Sessiona yazma işlemi
					$this->session->set_flashdata("alert", $alert);	
					redirect(base_url("services/new_form"));
					die();	
				}				

				//İşlemin Sonucunu Sessiona yazma işlemi
				$this->session->set_flashdata("alert", $alert);	
				redirect(base_url("services"));
			}
			else
			{
				$viewData = new stdClass();
				
				/* View'e gönderilecek değişkenlerin set edilmesi */				
				$viewData->viewFolder = $this->viewFolder;
				$viewData->subViewFolder = "add";
				$viewData->form_error = true;			
				$this->load->view("{$viewData->viewFolder}/{$viewData->subViewFolder}/index", $viewData);				
			}			
		}

		public function update($id)
		{
			if(!(isAllowedUpdateModule())){
				redirect(base_url($this->router->fetch_class()));
			}

			$this->load->library("form_validation");
			//kurallar yazılır
			
			$this->form_validation->set_rules("title", "Başlık", "required|trim");
			$this->form_validation->set_message(
				array(
					"required" => "{field} alanı doldurulmalıdır"
				)
			);
			//Form Validation Çalıştırılır
			$validate = $this->form_validation->run();
			//kontrol edilir
			if($validate)
			{
				// Eğer resim seçildiyse resmi güncellemem ve yeni resmi eklemem gerek	
				if($_FILES["img_url"]["name"] !== "")
				{

					// Upload Süreci
					// Dosya adı düzenlemesi;
					$ext = pathinfo($_FILES["img_url"]["name"], PATHINFO_EXTENSION);
					$file_name_stamm = convertToSeo(pathinfo($_FILES["img_url"]["name"], PATHINFO_FILENAME));
					$file_name = $file_name_stamm. "." .$ext;
					$file = $_FILES["img_url"]["tmp_name"];
					$uploadPath = "uploads/$this->viewFolder/";	

					
					$image_555x343 = upload_picture($file, $uploadPath, 555, 343, $file_name);
					$image_358x217 = upload_picture($file, $uploadPath, 358, 217, $file_name);
					
					if($image_555x343 && $image_358x217)
					{
											
						$data = array(
									"title"         => $this->input->post("title"),
									"description"   => $this->input->post("description"),
									"lang"          => $this->input->post("lang"),
									"url"           => convertToSeo($this->input->post("title")),						
									"img_url"       => $file_name
									);

					}else{
						$alert = array(
							"title"   => "İşlem Başarısızdır",
							"text"    => "Görsel yüklenirken bir problem oluştu", 
							"type"	  => "error"
						);	

						//İşlemin Sonucunu Sessiona yazma işlemi
						$this->session->set_flashdata("alert", $alert);	
						redirect(base_url("services/update_form/$id"));
							
					}				
				
				// Eğer resim seçilmediyse sadece title ve description değerlerini güncelle resmi muhafaza et.
				}else{
					$data = array(
								"title"         => $this->input->post("title"),
								"description"   => $this->input->post("description"),
								"lang"          => $this->input->post("lang"),
								"url"           => convertToSeo($this->input->post("title"))
								);						
				}	

		
		
				$update = $this->service_model->update(array("id" => $id),$data);
				
				if($update)
				{
					$alert = array(
						"title"  => "İşlem Başarılı",
						"text"   => "Kayıt başarılı bir şekilde güncellendi",
						"type"	 => "success"
					);					
				}else{
					$alert = array(
						"title"   => "İşlem Başarısızdır",
						"text"    => "Kayıt Güncellenemedi", 
						"type"	 => "error"
					);										
				}
				//İşlemin Sonucunu Sessiona yazma işlemi
				$this->session->set_flashdata("alert", $alert);	
				redirect(base_url("services"));
			}
			else
			{
				$viewData = new stdClass();
				
				/* View'e gönderilecek değişkenlerin set edilmesi */				
				$viewData->viewFolder = $this->viewFolder;
				$viewData->subViewFolder = "update";
				$viewData->form_error = true;
				
				
				/* Tablodan verilerin getirilmesi */			
				$viewData->item = $this->service_model->get(
					array(
						"id" => $id
					)
				);				
				$this->load->view("{$viewData->viewFolder}/{$viewData->subViewFolder}/index", $viewData);				
			}			
		}		
		
		public function update_form($id)
		{
			if(!(isAllowedUpdateModule())){
				redirect(base_url($this->router->fetch_class()));
			}

			$viewData = new stdClass();
			
			/* Tablodan verilerin getirilmesi */			
			$item = $this->service_model->get(
				array(
					"id" => $id
				)
			);			
			
			/* View'e gönderilecek değişkenlerin set edilmesi */			
			$viewData->viewFolder = $this->viewFolder;
			$viewData->subViewFolder = "update";
			$viewData->item = $item;		
			
			$this->load->view("{$viewData->viewFolder}/{$viewData->subViewFolder}/index", $viewData);			
		}

		public function delete($id)
		{
			if(!(isAllowedDeleteModule())){
				redirect(base_url($this->router->fetch_class()));
			}

			$service = $this->service_model->get(
				array(
					"id"	=> $id
				)
			);

			$delete = $this->service_model->delete(
				array(
					"id" => $id
				)
			);
			
			if($delete)
			{
				// DB den silme işlemi başarılıysa resim dosyasını da sileriz.
				unlink("uploads/{$this->viewFolder}/358x217/$service->img_url");	
				unlink("uploads/{$this->viewFolder}/555x343/$service->img_url");	

				$alert = array(
					"title"  => "İşlem Başarılı",
					"text"   => "Kayıt başarılı bir şekilde silindi",
					"type"	 => "success"
				);					
			}else{
				$alert = array(
					"title"  => "İşlem Başarısız",
					"text"   => "Silme İşlemi Gerçekleştirilemedi",
					"type"	 => "error"
				);					
			}
			$this->session->set_flashdata("alert", $alert);
			redirect(base_url("services"));	
		}
				

		public function isActiveSetter($id)
		{
			if(!(isAllowedUpdateModule())){
				die();
			}

			if($id)
			{
				$isActive = ($this->input->post("data") === "true") ? 1 : 0;
				
				$this->service_model->update(
					array(
						"id" => $id
					),
					array(
						"isActive" => $isActive
					)
				);
			}	
		}
		

		public function rankSetter()
		{
			if(!(isAllowedUpdateModule())){
				die();
			}
						
			$data = $this->input->post("data");
			parse_str($data, $order);
			$items = $order["ord"];
			
			foreach($items as $rank => $id)
			{
				$this->service_model->update(
					array(
						"id" => $id,
						"rank !=" => $rank
					),
					array(
						"rank" => $rank
					)
				);
			}
		}

}

  ?>