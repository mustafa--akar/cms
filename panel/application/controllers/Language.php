<?php

class Language extends MY_Controller{
	
	public function __construct(){

		parent::__construct();

		if(!get_active_user()){
			redirect(base_url("login"));
		}			
	}

	public function change($lang)
	{
		if($lang){		
			
			if($lang == "en" || $lang == "tr" || $lang == "de"){

				$this->session->set_userdata("lang", $lang);

				if($lang == "de"){					
					$this->config->set_item("language", "german");

				}elseif($lang == "en"){					
					$this->config->set_item("language", "english");
								
				}elseif($lang == "tr"){					
					$this->config->set_item("language", "turkish");
									
				}

			}
			redirect(base_url());
			
		}
	}
	
}

?>