<?php

class Courses extends MY_CONTROLLER
{
		public $viewFolder = "";
		
		public function __construct()
		{
			parent::__construct();
			$this->viewFolder = "courses_v";
			$this->load->model("course_model");
			
			if(!get_active_user()){
				redirect(base_url("login"));
			}			
			
		}
		public function index()
		{

			if(!(isAllowedViewModule())){
				redirect(base_url());
			}

			$viewData = new stdClass();
			
			/* Tablodan verilerin getirilmesi */
			$items = $this->course_model->get_all(
				array(
					"lang"	=> $this->session->userdata("lang")
				), "rank ASC"
			);
			
			/* View'e gönderilecek değişkenlerin set edilmesi */
			$viewData->items = $items;
			$viewData->viewFolder = $this->viewFolder;
			$viewData->subViewFolder = "list";			
			
			$this->load->view("{$viewData->viewFolder}/{$viewData->subViewFolder}/index", $viewData);
				
		}
		public function new_form()
		{		

			if(!(isAllowedWriteModule())){
				redirect(base_url($this->router->fetch_class()));
			}

			$viewData = new stdClass();
			
			/* View'e gönderilecek değişkenlerin set edilmesi */			
			$viewData->viewFolder = $this->viewFolder;
			$viewData->subViewFolder = "add";
			
			$this->load->view("{$viewData->viewFolder}/{$viewData->subViewFolder}/index", $viewData);	
		}
		public function save()
		{

			if(!(isAllowedWriteModule())){
				redirect(base_url($this->router->fetch_class()));
			}

			$this->load->library("form_validation");
			//kurallar yazılır		

			if($_FILES["img_url"]["name"] == "")
			{
				$alert = array(
					"title"		=> "İşlem Başarısız",
					"text"		=> "Lütfen Bir Görsel Seçiniz",
					"type"		=> "error"	
				);
				// İşlem sonucunu session'a yazma işlemi...
				$this->session->set_flashdata("alert", $alert);
				
				redirect(base_url("courses/new_form"));
				die();	
			}				
			
			$this->form_validation->set_rules("title", "Başlık", "required|trim");
            $this->form_validation->set_rules("event_date", "Eğitim Tarihi", "required|trim");
			$this->form_validation->set_message(
				array(
					"required" => "{field} alanı doldurulmalıdır"
				)
			);
			//Form Validation Çalıştırılır
			$validate = $this->form_validation->run();
			//kontrol edilir
			if($validate)
			{
				
				// Upload Süreci
				// Dosya adı düzenlemesi;
				$ext = pathinfo($_FILES["img_url"]["name"], PATHINFO_EXTENSION);
				$file_name_stamm = convertToSeo(pathinfo($_FILES["img_url"]["name"], PATHINFO_FILENAME));
				$file_name = $file_name_stamm. "." .$ext;
				$file = $_FILES["img_url"]["tmp_name"];
				$uploadPath = "uploads/$this->viewFolder/";	

				
				$image_255x157 = upload_picture($file, $uploadPath, 255, 157, $file_name);
				$image_352x157 = upload_picture($file, $uploadPath, 352, 157, $file_name);
				$image_1140x411 = upload_picture($file, $uploadPath, 1140, 411, $file_name);
				
				if($image_255x157 && $image_1140x411 && $image_352x157)
				{
						
					$insert = $this->course_model->add(
						array(
								"title"         => $this->input->post("title"),
								"description"   => $this->input->post("description"),
								"lang"          => $this->input->post("lang"),
								"url"           => convertToSeo($this->input->post("title")),							
								"img_url"       => $file_name,
								"event_date"    => $this->input->post("event_date"),
								"rank"          => 0,
								"isActive"      => 1,
								"createdAt"     => date("Y-m-d H:i:s")
							)					
					);				
					if($insert)
					{
						$alert = array(
							"title"  => "İşlem Başarılı",
							"text"   => "Kayıt başarılı bir şekilde eklendi",
							"type"	 => "success"
						);					
					}else{
						$alert = array(
							"title"   => "İşlem Başarısızdır",
							"text"    => "Kayıt Eklenemedi", 
							"type"	 => "error"
						);										
					}

				}else{
					$alert = array(
						"title"   => "İşlem Başarısızdır",
						"text"    => "Görsel yüklenirken bir problem oluştu", 
						"type"	  => "error"
					);	

					//İşlemin Sonucunu Sessiona yazma işlemi
					$this->session->set_flashdata("alert", $alert);	
					redirect(base_url("courses/new_form"));
					die();	
				}				

				//İşlemin Sonucunu Sessiona yazma işlemi
				$this->session->set_flashdata("alert", $alert);	
				redirect(base_url("courses"));
			}
			else
			{
				$viewData = new stdClass();
				
				/* View'e gönderilecek değişkenlerin set edilmesi */				
				$viewData->viewFolder = $this->viewFolder;
				$viewData->subViewFolder = "add";
				$viewData->form_error = true;			
				$this->load->view("{$viewData->viewFolder}/{$viewData->subViewFolder}/index", $viewData);				
			}			
		}

		public function update($id)
		{

			if(!(isAllowedUpdateModule())){
				redirect(base_url($this->router->fetch_class()));
			}

			$this->load->library("form_validation");
			//kurallar yazılır

			$this->form_validation->set_rules("title", "Başlık", "required|trim");
            $this->form_validation->set_rules("event_date", "Eğitim Tarihi", "required|trim");
			$this->form_validation->set_message(
				array(
					"required" => "{field} alanı doldurulmalıdır"
				)
			);
			//Form Validation Çalıştırılır
			$validate = $this->form_validation->run();
			//kontrol edilir
			if($validate)
			{
				// Eğer resim seçildiyse resmi güncellemem ve yeni resmi eklemem gerek	
				if($_FILES["img_url"]["name"] !== "")
				{

					// Upload Süreci
					// Dosya adı düzenlemesi;
					$ext = pathinfo($_FILES["img_url"]["name"], PATHINFO_EXTENSION);
					$file_name_stamm = convertToSeo(pathinfo($_FILES["img_url"]["name"], PATHINFO_FILENAME));
					$file_name = $file_name_stamm. "." .$ext;
					$file = $_FILES["img_url"]["tmp_name"];
					$uploadPath = "uploads/$this->viewFolder/";	

					
					$image_255x157 = upload_picture($file, $uploadPath, 255, 157, $file_name);
					$image_352x157 = upload_picture($file, $uploadPath, 352, 157, $file_name);
					$image_1140x411 = upload_picture($file, $uploadPath, 1140, 411, $file_name);
					
					if($image_255x157 && $image_1140x411 && $image_352x157)
					{
												
						$data = array(
									"title"         => $this->input->post("title"),
									"description"   => $this->input->post("description"),
									"lang"          => $this->input->post("lang"),
                                    "event_date"    => $this->input->post("event_date"),
									"url"           => convertToSeo($this->input->post("title")),						
									"img_url"       => $file_name
									);

					}else{
						$alert = array(
							"title"   => "İşlem Başarısızdır",
							"text"    => "Görsel yüklenirken bir problem oluştu", 
							"type"	  => "error"
						);	

						//İşlemin Sonucunu Sessiona yazma işlemi
						$this->session->set_flashdata("alert", $alert);	
						redirect(base_url("courses/update_form/$id"));
							
					}				
				
				// Eğer resim seçilmediyse sadece title ve description değerlerini güncelle resmi muhafaza et.
				}else{
					$data = array(
								"title"         => $this->input->post("title"),
								"description"   => $this->input->post("description"),
								"lang"          => $this->input->post("lang"),
                                "event_date"    => $this->input->post("event_date"),
								"url"           => convertToSeo($this->input->post("title"))
								);						
				}	

		
		
				$update = $this->course_model->update(array("id" => $id),$data);
				
				if($update)
				{
					$alert = array(
						"title"  => "İşlem Başarılı",
						"text"   => "Kayıt başarılı bir şekilde güncellendi",
						"type"	 => "success"
					);					
				}else{
					$alert = array(
						"title"   => "İşlem Başarısızdır",
						"text"    => "Kayıt Güncellenemedi", 
						"type"	 => "error"
					);										
				}
				//İşlemin Sonucunu Sessiona yazma işlemi
				$this->session->set_flashdata("alert", $alert);	
				redirect(base_url("courses"));
			}
			else
			{
				$viewData = new stdClass();
				
				/* View'e gönderilecek değişkenlerin set edilmesi */				
				$viewData->viewFolder = $this->viewFolder;
				$viewData->subViewFolder = "update";
				$viewData->form_error = true;
				
				
				/* Tablodan verilerin getirilmesi */			
				$viewData->item = $this->course_model->get(
					array(
						"id" => $id
					)
				);				
				$this->load->view("{$viewData->viewFolder}/{$viewData->subViewFolder}/index", $viewData);				
			}			
		}		
		
		public function update_form($id)
		{
			if(!(isAllowedUpdateModule())){
				redirect(base_url($this->router->fetch_class()));
			}			
			$viewData = new stdClass();
			
			/* Tablodan verilerin getirilmesi */			
			$item = $this->course_model->get(
				array(
					"id" => $id
				)
			);			
			
			/* View'e gönderilecek değişkenlerin set edilmesi */			
			$viewData->viewFolder = $this->viewFolder;
			$viewData->subViewFolder = "update";
			$viewData->item = $item;		
			
			$this->load->view("{$viewData->viewFolder}/{$viewData->subViewFolder}/index", $viewData);			
		}

		public function delete($id)
		{

			if(!(isAllowedDeleteModule())){
				redirect(base_url($this->router->fetch_class()));
			}	
			$course = $this->course_model->get(
				array(
					"id" => $id
				)
			);					
			$delete = $this->course_model->delete(
				array(
					"id" => $id
				)
			);
			
			if($delete)
			{
				unlink("uploads/{$this->viewFolder}/255x157/$course->img_url");
				unlink("uploads/{$this->viewFolder}/352x157/$course->img_url");				
				unlink("uploads/{$this->viewFolder}/1140x411/$course->img_url");				
				$alert = array(
					"title"  => "İşlem Başarılı",
					"text"   => "Kayıt başarılı bir şekilde silindi",
					"type"	 => "success"
				);					
			}else{
				$alert = array(
					"title"  => "İşlem Başarısız",
					"text"   => "Silme İşlemi Gerçekleştirilemedi",
					"type"	 => "error"
				);					
			}
			$this->session->set_flashdata("alert", $alert);
			redirect(base_url("courses"));	
		}
				

		public function isActiveSetter($id)
		{
			if(!(isAllowedUpdateModule())){
				die();
			}

			if($id)
			{
				$isActive = ($this->input->post("data") === "true") ? 1 : 0;
				
				$this->course_model->update(
					array(
						"id" => $id
					),
					array(
						"isActive" => $isActive
					)
				);
			}	
		}
		

		public function rankSetter()
		{
			if(!(isAllowedUpdateModule())){
				die();
			}
			
			$data = $this->input->post("data");
			parse_str($data, $order);
			$items = $order["ord"];
			
			foreach($items as $rank => $id)
			{
				$this->course_model->update(
					array(
						"id" => $id,
						"rank !=" => $rank
					),
					array(
						"rank" => $rank
					)
				);
			}
		}

}

  ?>