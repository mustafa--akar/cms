<?php

class Send_email extends MY_CONTROLLER
{
		public $viewFolder = "";
		
		public function __construct()
		{
			parent::__construct();
			$this->viewFolder = "send_email_v";
			
			
			if(!get_active_user()){
				redirect(base_url("login"));
			}			
			
		}
		public function index()
		{
			if(!(isAllowedViewModule())){
				redirect(base_url());
			}

			$viewData = new stdClass();
			
			/* View'e gönderilecek değişkenlerin set edilmesi */

			$viewData->viewFolder = $this->viewFolder;
					
			
			$this->load->view("{$viewData->viewFolder}/index", $viewData);
				
		}

		public function send()
		{

			$this->load->library("form_validation");
			//kurallar yazılır		
							
			$this->form_validation->set_rules("to", "Kime", "required|trim");
			$this->form_validation->set_rules("subject", "Konu", "required|trim");
			$this->form_validation->set_rules("message", "Mesaj", "required|trim");
			
			$this->form_validation->set_message(
				array(
					"required" => "{field} alanı doldurulmalıdır"
				)
			);
			//Form Validation Çalıştırılır
			$validate = $this->form_validation->run();
			//kontrol edilir
			if($validate)
			{
				$send_email = send_email($this->input->post("to"), $this->input->post("subject"), $this->input->post("message"), $this->input->post("cc"), $this->input->post("bcc"));

				if($send_email){
					$alert = array(
						"title"  => "İşlem Başarılı",
						"text"   => "E-Posta başarılı bir şekilde gönderildi",
						"type"	 => "success"
					);					
				}else{
					$alert = array(
						"title"   => "İşlem Başarısızdır",
						"text"    => "E-Posta Gönderme işlemi esnasında bir hata oluştu", 
						"type"	  => "error"
					);
				}

				//İşlemin Sonucunu Sessiona yazma işlemi
				$this->session->set_flashdata("alert", $alert);	
				redirect(base_url("send_email"));
			}else{
				$viewData = new stdClass();			
							
				$viewData->viewFolder = $this->viewFolder;				
				$viewData->form_error = true;			
				$this->load->view("{$viewData->viewFolder}/index", $viewData);				
			}			

		}
}	

?>