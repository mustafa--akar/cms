<?php

class Galleries extends MY_CONTROLLER
{
		public $viewFolder = "";
		
		public function __construct()
		{
			parent::__construct();
			$this->viewFolder = "galleries_v";
			$this->load->model("gallery_model");
			$this->load->model("image_model");
			$this->load->model("video_model");
			$this->load->model("file_model");
			
			if(!get_active_user()){
				redirect(base_url("login"));
			}			
		}
		public function index()
		{
			if(!(isAllowedViewModule())){
				redirect(base_url());
			}

			$viewData = new stdClass();			
			
			/* Tablodan verilerin getirilmesi */
			$items = $this->gallery_model->get_all(
				array(
					"lang"	=> $this->session->userdata("lang")
				), "rank ASC"
			);		
			
			/* View'e gönderilecek değişkenlerin set edilmesi */
			$viewData->items = $items;
			$viewData->viewFolder = $this->viewFolder;
			$viewData->subViewFolder = "list";			
			
			$this->load->view("{$viewData->viewFolder}/{$viewData->subViewFolder}/index", $viewData);
				
		}
		public function new_form()
		{		

			if(!(isAllowedWriteModule())){
				redirect(base_url($this->router->fetch_class()));
			}

			$viewData = new stdClass();
			
			/* View'e gönderilecek değişkenlerin set edilmesi */			
			$viewData->viewFolder = $this->viewFolder;
			$viewData->subViewFolder = "add";
			
			$this->load->view("{$viewData->viewFolder}/{$viewData->subViewFolder}/index", $viewData);	
		}
		public function save()
		{

			if(!(isAllowedWriteModule())){
				redirect(base_url($this->router->fetch_class()));
			}

			$this->load->library("form_validation");
			//kurallar yazılır
			$this->form_validation->set_rules("title", "Galeri Adı", "required|trim");
			$this->form_validation->set_message(
				array(
					"required" => "{field} alanı doldurulmalıdır"
				)
			);
			//Form Validation Çalıştırılır
			$validate = $this->form_validation->run();
			//kontrol edilir
			if($validate)
			{
				$gallery_type = $this->input->post("gallery_type");
				$path = "uploads/$this->viewFolder/";
				$foler_name = "";
				$folder_name = convertToSeo($this->input->post("title"));
				
				if($gallery_type == "image")
				{					
					$path = "$path/images/$folder_name";
				}else if($gallery_type == "file")
				{					
					$path = "$path/files/$folder_name";					
				}	
				
				//gallery_type video değilse dosya oluşturulur					
				
				if($gallery_type != "video")
				{
					if(!mkdir($path, 0755))
					{
						$alert = array(
							"title"   => "İşlem Başarısızdır",
							"text"    => "Galeri Üretilirken Problem Oluştu (Yetki Hatası)", 
							"type"	 => "error"
						);	
						//İşlemin Sonucunu Sessiona yazma işlemi
						$this->session->set_flashdata("alert", $alert);	
						redirect(base_url("galleries"));
						die();	
					}	
				}	
	
				$insert = $this->gallery_model->add(
					array(
						"title"         => $this->input->post("title"),
						"gallery_type"  => $gallery_type,
						"lang"          => $this->input->post("lang"),
						"url"           => convertToSeo($this->input->post("title")),
						"folder_name"   => $folder_name,
						"rank"          => 0,
						"isActive"      => 1,
						"createdAt"     => date("Y-m-d H:i:s")
					)
				);
				
				if($insert)
				{
					$alert = array(
						"title"  => "İşlem Başarılı",
						"text"   => "Kayıt başarılı bir şekilde eklendi",
						"type"	 => "success"
					);					
				}else{
					$alert = array(
						"title"   => "İşlem Başarısızdır",
						"text"    => "Kayıt Eklenemedi", 
						"type"	 => "error"
					);										
				}
				//İşlemin Sonucunu Sessiona yazma işlemi
				$this->session->set_flashdata("alert", $alert);	
				redirect(base_url("galleries"));
			}
			else
			{
				$viewData = new stdClass();
				
				/* View'e gönderilecek değişkenlerin set edilmesi */				
				$viewData->viewFolder = $this->viewFolder;
				$viewData->subViewFolder = "add";
				$viewData->form_error = true;
				$this->load->view("{$viewData->viewFolder}/{$viewData->subViewFolder}/index", $viewData);				
			}			
		}
		public function update_form($id)
		{

			if(!(isAllowedUpdateModule())){
				redirect(base_url($this->router->fetch_class()));
			}

			$viewData = new stdClass();
			
			/* Tablodan verilerin getirilmesi */			
			$item = $this->gallery_model->get(
				array(
					"id" => $id
				)
			);			
			
			/* View'e gönderilecek değişkenlerin set edilmesi */			
			$viewData->viewFolder = $this->viewFolder;
			$viewData->subViewFolder = "update";
			$viewData->item = $item;		
			
			$this->load->view("{$viewData->viewFolder}/{$viewData->subViewFolder}/index", $viewData);			
		}
		public function update($id, $gallery_type = "image", $oldFolderName = "")
		{

			if(!(isAllowedUpdateModule())){
				redirect(base_url($this->router->fetch_class()));
			}

			$this->load->library("form_validation");
			//kurallar yazılır
			$this->form_validation->set_rules("title", "Galeri Adı", "required|trim");
			$this->form_validation->set_message(
				array(
					"required" => "{field} alanı doldurulmalıdır"
				)
			);
			//Form Validation Çalıştırılır
			$validate = $this->form_validation->run();
			//kontrol edilir
			if($validate)
			{			
				$path = "uploads/$this->viewFolder/";
				$foler_name = "";
				$folder_name = convertToSeo($this->input->post("title"));
				
				if($gallery_type == "image")
				{					
					$path = "$path/images";
				}else if($gallery_type == "file")
				{					
					$path = "$path/files";					
				}	
				
				//gallery_type video değilse dosya oluşturulur					
				
				if($gallery_type != "video")
				{
					if(!rename("$path/$oldFolderName", "$path/$folder_name"))
					{
						$alert = array(
							"title"   => "İşlem Başarısızdır",
							"text"    => "Galeri Üretilirken Problem Oluştu (Yetki Hatası)", 
							"type"	 => "error"
						);	
						//İşlemin Sonucunu Sessiona yazma işlemi
						$this->session->set_flashdata("alert", $alert);	
						redirect(base_url("galleries"));
						die();	
					}	
				}					
				
				
				$update = $this->gallery_model->update(
					array(
						"id" => $id
					),
					array(
						"title"         => $this->input->post("title"),
						"lang"          => $this->input->post("lang"),
						"folder_name"   => $folder_name,
						"url"           => convertToSeo($this->input->post("title"))
					)
				);
				
				if($update)
				{
					$alert = array(
						"title"  => "İşlem Başarılı",
						"text"   => "Güncelleme işlemi başarılı bir şekilde gerçekleştirildi",
						"type"	 => "success"
					);					
				}else{
					$alert = array(
						"title"  => "İşlem Başarısız",
						"text"   => "Güncelleme İşlemi Gerçekleştirilemedi",
						"type"	 => "error"
					);					
				}
				$this->session->set_flashdata("alert", $alert);
				redirect(base_url("galleries"));				
			}
			else
			{
				$viewData = new stdClass();
				
				/* Tablodan verilerin getirilmesi */			
				$item = $this->gallery_model->get(
					array(
						"id" => $id
					)
				);					
				/* View'e gönderilecek değişkenlerin set edilmesi */				
				$viewData->viewFolder = $this->viewFolder;
				$viewData->subViewFolder = "update";
				$viewData->form_error = true;
				$viewData->item = $item;
				$this->load->view("{$viewData->viewFolder}/{$viewData->subViewFolder}/index", $viewData);				
			}			
		}
		
		public function delete($id)
		{

			if(!(isAllowedDeleteModule())){
				redirect(base_url($this->router->fetch_class()));
			}

			$gallery = $this->gallery_model->get(
				array(
					"id"  =>  $id
				)
			);
			
			if($gallery)
			{
				if($gallery->gallery_type != "video")
				{
					if($gallery->gallery_type == "image")
					{
						$path = "uploads/$this->viewFolder/images/$gallery->folder_name";
					}else if($gallery->gallery_type == "file"){
						$path = "uploads/$this->viewFolder/files/$gallery->folder_name";
					}	
					
					$delete_folder = rmdir($path);
					
					if(!$delete_folder)
					{
						$alert = array(
							"title"  => "İşlem Başarısız",
							"text"   => "Dosya Silme İşlemi Gerçekleştirilemedi",
							"type"	 => "error"
						);	
						$this->session->set_flashdata("alert", $alert);
						redirect(base_url("galleries"));
						die();						
					}	
					
				}	
				$delete = $this->gallery_model->delete(
					array(
						"id" => $id
					)
				);
				
				if($delete)
				{
					$alert = array(
						"title"  => "İşlem Başarılı",
						"text"   => "Kayıt başarılı bir şekilde silindi",
						"type"	 => "success"
					);					
				}else{
					$alert = array(
						"title"  => "İşlem Başarısız",
						"text"   => "Silme İşlemi Gerçekleştirilemedi",
						"type"	 => "error"
					);					
				}
				$this->session->set_flashdata("alert", $alert);
				redirect(base_url("galleries"));				
			}	
		}
		
		public function fileDelete($id, $parent_id, $gallery_type)
		{

			if(!(isAllowedDeleteModule())){
				redirect(base_url($this->router->fetch_class()));
			}

			$modelName = ($gallery_type == "image") ?  "image_model" : "file_model";
			
			$fileName = $this->$modelName->get(
				array(
					"id"  =>  $id
				)
			);			
			
			$gallery = $this->gallery_model->get(
				array(
					"id"	=> $parent_id
				)
			);

			$delete = $this->$modelName->delete(
				array(
					"id" => $id
				)
			);

			if($delete)
			{
				// DB den silme işlemi başarılıysa resim dosyasını da sileriz.
				if($gallery_type == "image"){
					unlink("uploads/{$this->viewFolder}/images/$gallery->url/252x156/$fileName->url");
					unlink("uploads/{$this->viewFolder}/images/$gallery->url/350x216/$fileName->url");
					unlink("uploads/{$this->viewFolder}/images/$gallery->url/851x606/$fileName->url");
				}elseif($gallery_type == "file"){
					unlink("uploads/{$this->viewFolder}/files/$gallery->url/$fileName->url");
				}				
				$alert = array(
					"title"  => "İşlem Başarılı",
					"text"   => "Kayıt başarılı bir şekilde silindi",
					"type"	 => "success"
				);	

			}else{
				$alert = array(
					"title"   => "İşlem Başarısızdır",
					"text"    => "Kayıt Silinemedi", 
					"type"	 => "error"
				);										
			}
			//İşlemin Sonucunu Sessiona yazma işlemi
			$this->session->set_flashdata("alert", $alert);
			redirect(base_url("galleries/upload_form/$parent_id"));	
	
		}		

		public function isActiveSetter($id)
		{

			if(!(isAllowedUpdateModule())){
				die();
			}

			if($id)
			{
				$isActive = ($this->input->post("data") === "true") ? 1 : 0;
				
				$this->gallery_model->update(
					array(
						"id" => $id
					),
					array(
						"isActive" => $isActive
					)
				);
			}	
		}
		
		public function fileIsActiveSetter($id, $gallery_type)
		{

			if(!(isAllowedUpdateModule())){
				die();
			}

			if($id && $gallery_type)
			{
				$isActive = ($this->input->post("data") === "true") ? 1 : 0;
				
				$modelName = ($gallery_type == "image") ?  "image_model" : "file_model";
				
				$this->$modelName->update(
					array(
						"id" => $id
					),
					array(
						"isActive" => $isActive
					)
				);
			}	
		}		
		
		public function rankSetter()
		{

			if(!(isAllowedUpdateModule())){
				die();
			}

			$data = $this->input->post("data");
			parse_str($data, $order);
			$items = $order["ord"];
			
			foreach($items as $rank => $id)
			{
				$this->gallery_model->update(
					array(
						"id" => $id,
						"rank !=" => $rank
					),
					array(
						"rank" => $rank
					)
				);
			}
		}

		public function fileRankSetter($gallery_type)
		{

			if(!(isAllowedUpdateModule())){
				die();
			}

			$data = $this->input->post("data");
			parse_str($data, $order);
			$items = $order["ord"];
			
			$modelName = ($gallery_type == "image") ?  "image_model" : "file_model";
			
			foreach($items as $rank => $id)
			{
				$this->$modelName->update(
					array(
						"id" => $id,
						"rank !=" => $rank
					),
					array(
						"rank" => $rank
					)
				);
			}
		}
		public function upload_form($id)
		{

			if(!(isAllowedWriteModule())){
				redirect(base_url($this->router->fetch_class()));
			}

			$viewData = new stdClass();
			
			/* View'e gönderilecek değişkenlerin set edilmesi */			
			$viewData->viewFolder = $this->viewFolder;
			$viewData->subViewFolder = "image";
			$item = $this->gallery_model->get(
				array(
					"id" => $id
				)
			);
			
			$viewData->item = $item;
			
			if($item->gallery_type == "image"){			
				$viewData->items = $this->image_model->get_all(
					array(
						"gallery_id" => $id
					), "rank ASC"
				);	
				$viewData->folder_name = $item->folder_name; 			
			}else if($item->gallery_type == "file"){
				$viewData->items = $this->file_model->get_all(
					array(
						"gallery_id" => $id
					), "rank ASC"
				);					
			}else{
				$viewData->items = $this->video_model->get_all(
					array(
						"gallery_id" => $id
					), "rank ASC"
				);					
			}	
			
			$viewData->gallery_type = $item->gallery_type;
			$this->load->view("{$viewData->viewFolder}/{$viewData->subViewFolder}/index", $viewData);			
		}	
		
		public function file_upload_old($gallery_id, $gallery_type, $folderName)
		{

			if(!(isAllowedWriteModule())){
				redirect(base_url($this->router->fetch_class()));
			}

			// Dosya adı düzenlemesi;
			$ext = pathinfo($_FILES["file"]["name"], PATHINFO_EXTENSION);
			$file_name_stamm = convertToSeo(pathinfo($_FILES["file"]["name"], PATHINFO_FILENAME));
			$file_name = $file_name_stamm. "." .$ext;
			
			// Upload metodunun ayarları set edildi
			$config["file_name"] = $file_name;
			$config["allowed_types"] = "jpg|jpeg|png|pdf|doc|docx|xlsx";
			$config["upload_path"] = ($gallery_type == "image") ? "uploads/$this->viewFolder/images/$folderName" : "uploads/$this->viewFolder/files/$folderName";
			
			$this->load->library("upload", $config);
			
			$upload = $this->upload->do_upload("file");
			
			if($upload)
			{
				$uploaded_file = $this->upload->data("file_name");
				
				$modelName = ($gallery_type == "image") ?  "image_model" : "file_model";
				
				
				$this->$modelName->add(
					array(
						"url"         => "{$config["upload_path"]}/$uploaded_file",
						"rank"        => 0,
						"isActive"    => 1,						
						"createdAt"   => date("Y-m-d H:i:s"),
						"gallery_id"  => $gallery_id	
					)
				);
			}else{
				echo "bir sorunla karşılaşıldı";
			}	
		}

		public function file_upload($gallery_id, $gallery_type, $folderName)
		{

			if(!(isAllowedWriteModule())){
				redirect(base_url($this->router->fetch_class()));
			}

			// Dosya adı düzenlemesi;
			$ext = pathinfo($_FILES["file"]["name"], PATHINFO_EXTENSION);
			$file_name_stamm = convertToSeo(pathinfo($_FILES["file"]["name"], PATHINFO_FILENAME));
			$file_name = $file_name_stamm. "." .$ext;
			
			// Upload metodunun ayarları set edildi
			$config["file_name"] = $file_name;
			$file = $_FILES["file"]["tmp_name"];
			$uploadPath = "uploads/$this->viewFolder/images/$folderName";

			if($gallery_type == "image"){

				$image_252x156 = upload_picture($file, $uploadPath, 252, 156, $file_name);
				$image_350x216 = upload_picture($file, $uploadPath, 350, 216, $file_name);
				$image_851x606 = upload_picture($file, $uploadPath, 851, 606, $file_name);

				if($image_851x606 && $image_350x216 && $image_252x156){
					$this->image_model->add(
						array(
							"url"         => $file_name,
							"rank"        => 0,
							"isActive"    => 1,						
							"createdAt"   => date("Y-m-d H:i:s"),
							"gallery_id"  => $gallery_id	
						)
					);					
				}else{
					echo "bir sorunla karşılaşıldı";
				}	

			}else{

				// Upload metodunun ayarları set edildi
				$config["file_name"] = $file_name;
				$config["allowed_types"] = "jpg|jpeg|png|pdf|doc|docx|xlsx|txt";
				$config["upload_path"] = "uploads/$this->viewFolder/files/$folderName";
				
				$this->load->library("upload", $config);
				
				$upload = $this->upload->do_upload("file");
				if($upload)
				{

					$uploaded_file = $this->upload->data("file_name"); 					
								
					
					$this->file_model->add(
						array(
							"url"         => $uploaded_file,
							"rank"        => 0,
							"isActive"    => 1,						
							"createdAt"   => date("Y-m-d H:i:s"),
							"gallery_id"  => $gallery_id	
						)
					);
				}else{
					echo "bir sorunla karşılaşıldı";
				}				

			}	
	
		}		
		
		public function refresh_file_list($gallery_id, $gallery_type, $folder_name)
		{

			if(!(isAllowedViewModule())){
				redirect(base_url($this->router->fetch_class()));
			}			
			
			$viewData = new stdClass();
			
			/* View'e gönderilecek değişkenlerin set edilmesi */			
			$viewData->viewFolder = $this->viewFolder;
			$viewData->subViewFolder = "image";
			
			$modelName = ($gallery_type == "image") ?  "image_model" : "file_model";
			
			$viewData->items = $this->$modelName->get_all(
				array(
					"gallery_id" => $gallery_id
				)
			);	
			
			$viewData->gallery_type = $gallery_type;	
			$viewData->folder_name  = $folder_name;

			$render_html = $this->load->view("{$viewData->viewFolder}/{$viewData->subViewFolder}/render_elements/file_list_v", $viewData, true);
			echo $render_html;
		}
		public function gallery_video_list($id)
		{

			if(!(isAllowedViewModule())){
				redirect(base_url($this->router->fetch_class()));
			}

			$viewData = new stdClass();
			
			$gallery = $this->gallery_model->get(
				array(
					"id"	=>	$id
				)
			);
			
			/* Tablodan verilerin getirilmesi */
			$items = $this->video_model->get_all(
				array(
					"gallery_id"  => $id
				), "rank ASC"
			);
			
			/* View'e gönderilecek değişkenlerin set edilmesi */
			$viewData->items = $items;
			$viewData->gallery = $gallery;
			$viewData->viewFolder = $this->viewFolder;
			$viewData->subViewFolder = "video/list";			
			
			$this->load->view("{$viewData->viewFolder}/{$viewData->subViewFolder}/index", $viewData);
				
		}		
		
		public function new_gallery_video_form($id)
		{		

			if(!(isAllowedWriteModule())){
				redirect(base_url($this->router->fetch_class()));
			}

			$viewData = new stdClass();		
			
			/* View'e gönderilecek değişkenlerin set edilmesi */
			$viewData->gallery_id = $id;	
			$viewData->viewFolder = $this->viewFolder;
			$viewData->subViewFolder = "video/add";
			
			$this->load->view("{$viewData->viewFolder}/{$viewData->subViewFolder}/index", $viewData);	
		}
		
		public function gallery_video_save($id)
		{

			if(!(isAllowedWriteModule())){
				redirect(base_url($this->router->fetch_class()));
			}

			$this->load->library("form_validation");
			//kurallar yazılır
			$this->form_validation->set_rules("url", "Video URL", "required|trim");
			$this->form_validation->set_message(
				array(
					"required" => "{field} alanı doldurulmalıdır"
				)
			);
			//Form Validation Çalıştırılır
			$validate = $this->form_validation->run();
			//kontrol edilir
			if($validate)
			{

				$insert = $this->video_model->add(
					array(					
						"url"           => $this->input->post("url"),
						"gallery_id"    => $id,
						"rank"          => 0,
						"isActive"      => 1,
						"createdAt"     => date("Y-m-d H:i:s")
					)
				);
				
				if($insert)
				{
					$alert = array(
						"title"  => "İşlem Başarılı",
						"text"   => "Kayıt başarılı bir şekilde eklendi",
						"type"	 => "success"
					);					
				}else{
					$alert = array(
						"title"   => "İşlem Başarısızdır",
						"text"    => "Kayıt Eklenemedi", 
						"type"	 => "error"
					);										
				}
				//İşlemin Sonucunu Sessiona yazma işlemi
				$this->session->set_flashdata("alert", $alert);	
				redirect(base_url("galleries/gallery_video_list/$id"));
			}
			else
			{
				$viewData = new stdClass();
				
				/* View'e gönderilecek değişkenlerin set edilmesi */				
				$viewData->viewFolder = $this->viewFolder;
				$viewData->subViewFolder = "video/add";
				$viewData->form_error = true;
				$viewData->gallery_id = $id;
				$this->load->view("{$viewData->viewFolder}/{$viewData->subViewFolder}/index", $viewData);				
			}			
		}		
		public function update_gallery_video_form($id)
		{

			if(!(isAllowedUpdateModule())){
				redirect(base_url($this->router->fetch_class()));
			}

			$viewData = new stdClass();
			
			/* Tablodan verilerin getirilmesi */			
			$item = $this->video_model->get(
				array(
					"id" => $id
				)
			);			
			
			/* View'e gönderilecek değişkenlerin set edilmesi */			
			$viewData->viewFolder = $this->viewFolder;
			$viewData->subViewFolder = "video/update";
			$viewData->item = $item;		
			
			$this->load->view("{$viewData->viewFolder}/{$viewData->subViewFolder}/index", $viewData);			
		}

		public function gallery_video_update($id, $gallery_id)
		{

			if(!(isAllowedUpdateModule())){
				redirect(base_url($this->router->fetch_class()));
			}

			$this->load->library("form_validation");
			//kurallar yazılır
			$this->form_validation->set_rules("url", "Video URL", "required|trim");
			$this->form_validation->set_message(
				array(
					"required" => "{field} alanı doldurulmalıdır"
				)
			);
			//Form Validation Çalıştırılır
			$validate = $this->form_validation->run();
			//kontrol edilir
			if($validate)
			{	
		
				$update = $this->video_model->update(
					array(
						"id"   => $id
					),
					array(
						"url"  => $this->input->post("url")
					)
				);
				
				if($update)
				{
					$alert = array(
						"title"  => "İşlem Başarılı",
						"text"   => "Güncelleme işlemi başarılı bir şekilde gerçekleştirildi",
						"type"	 => "success"
					);					
				}else{
					$alert = array(
						"title"  => "İşlem Başarısız",
						"text"   => "Güncelleme İşlemi Gerçekleştirilemedi",
						"type"	 => "error"
					);					
				}
				$this->session->set_flashdata("alert", $alert);
				redirect(base_url("galleries/gallery_video_list/$gallery_id"));				
			}
			else
			{
				$viewData = new stdClass();
				
				/* Tablodan verilerin getirilmesi */			
				$item = $this->video_model->get(
					array(
						"id" => $id
					)
				);					
				/* View'e gönderilecek değişkenlerin set edilmesi */				
				$viewData->viewFolder = $this->viewFolder;
				$viewData->subViewFolder = "video/update";
				$viewData->form_error = true;
				$viewData->item = $item;
				$this->load->view("{$viewData->viewFolder}/{$viewData->subViewFolder}/index", $viewData);				
			}			
		}

		public function rankGalleryVideoSetter()
		{
			
			if(!(isAllowedUpdateModule())){
				die();
			}

			$data = $this->input->post("data");
			parse_str($data, $order);
			$items = $order["ord"];
			
			foreach($items as $rank => $id)
			{
				$this->video_model->update(
					array(
						"id" => $id,
						"rank !=" => $rank
					),
					array(
						"rank" => $rank
					)
				);
			}
		}	
		
		public function GalleryVideoisActiveSetter($id)
		{
			
			if(!(isAllowedUpdateModule())){
				die();
			}

			if($id)
			{
				$isActive = ($this->input->post("data") === "true") ? 1 : 0;
				
				$this->video_model->update(
					array(
						"id" => $id
					),
					array(
						"isActive" => $isActive
					)
				);
			}	
		}

		
		
		public function galleryVideodelete($id, $gallery_id)
		{

			if(!(isAllowedDeleteModule())){
				redirect(base_url($this->router->fetch_class()));
			}			
	
			$delete = $this->video_model->delete(
				array(
					"id" => $id
				)
			);
			
			if($delete)
			{
				$alert = array(
					"title"  => "İşlem Başarılı",
					"text"   => "Kayıt başarılı bir şekilde silindi",
					"type"	 => "success"
				);					
			}else{
				$alert = array(
					"title"  => "İşlem Başarısız",
					"text"   => "Silme İşlemi Gerçekleştirilemedi",
					"type"	 => "error"
				);					
			}
			$this->session->set_flashdata("alert", $alert);
			redirect(base_url("galleries/gallery_video_list/$gallery_id"));				
				
		}
}

  ?>