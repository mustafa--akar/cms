<?php

class Product extends MY_CONTROLLER
{
		public $viewFolder = "";
		
		public function __construct()
		{
			parent::__construct();
			$this->viewFolder = "product_v";
			$this->load->model("product_model");
			$this->load->model("product_image_model");
			$this->load->model("product_category_model");

			if(!get_active_user()){
				redirect(base_url("login"));
			}			
			
		}
		public function index()
		{
			if(!(isAllowedViewModule())){
				redirect(base_url());
			}

			$viewData = new stdClass();
			
			/* Tablodan verilerin getirilmesi */
			$items = $this->product_model->get_all(
				array(
					"lang"	=> $this->session->userdata("lang")
				), "rank ASC"
			);
			
			/* View'e gönderilecek değişkenlerin set edilmesi */
			$viewData->items = $items;
			$viewData->viewFolder = $this->viewFolder;
			$viewData->subViewFolder = "list";			
			
			$this->load->view("{$viewData->viewFolder}/{$viewData->subViewFolder}/index", $viewData);
				
		}
		public function new_form()
		{		

			if(!(isAllowedWriteModule())){
				redirect(base_url($this->router->fetch_class()));
			}

			$viewData = new stdClass();
			
			/* View'e gönderilecek değişkenlerin set edilmesi */			
			$viewData->viewFolder = $this->viewFolder;
			$viewData->subViewFolder = "add";			
			
			$viewData->categories = $this->product_category_model->get_all(
				array(
					"isActive"	=> 1,
					"lang"	    => $this->session->userdata("lang")
				)
			);			
			
			$this->load->view("{$viewData->viewFolder}/{$viewData->subViewFolder}/index", $viewData);	
		}
		public function save()
		{

			if(!(isAllowedWriteModule())){
				redirect(base_url($this->router->fetch_class()));
			}

			$this->load->library("form_validation");
			//kurallar yazılır
			$this->form_validation->set_rules("title", "Başlık", "required|trim");
			$this->form_validation->set_rules("category_id", "Kategori", "required|trim");
			$this->form_validation->set_message(
				array(
					"required" => "{field} alanı doldurulmalıdır"
				)
			);
			//Form Validation Çalıştırılır
			$validate = $this->form_validation->run();
			//kontrol edilir
			if($validate)
			{
				$insert = $this->product_model->add(
					array(
						"title"         => $this->input->post("title"),
						"description"   => $this->input->post("description"),	
						"lang"	        => $this->session->userdata("lang"),					
						"category_id"   => $this->input->post("category_id"),
						"url"           => convertToSeo($this->input->post("title")),
						"rank"          => 0,
						"isActive"      => 1,
						"createdAt"     => date("Y-m-d H:i:s")
					)
				);
				
				if($insert)
				{
					$alert = array(
						"title"  => "İşlem Başarılı",
						"text"   => "Kayıt başarılı bir şekilde eklendi",
						"type"	 => "success"
					);					
				}else{
					$alert = array(
						"title"   => "İşlem Başarısızdır",
						"text"    => "Kayıt Eklenemedi", 
						"type"	 => "error"
					);										
				}
				//İşlemin Sonucunu Sessiona yazma işlemi
				$this->session->set_flashdata("alert", $alert);	
				redirect(base_url("product"));
			}
			else
			{
				$viewData = new stdClass();
				
				/* View'e gönderilecek değişkenlerin set edilmesi */				
				$viewData->viewFolder = $this->viewFolder;
				$viewData->subViewFolder = "add";
				$viewData->form_error = true;
				$this->load->view("{$viewData->viewFolder}/{$viewData->subViewFolder}/index", $viewData);				
			}			
		}
		public function update_form($id)
		{
			if(!(isAllowedUpdateModule())){
				redirect(base_url($this->router->fetch_class()));
			}

			$viewData = new stdClass();
			
			/* Tablodan verilerin getirilmesi */			
			$item = $this->product_model->get(
				array(
					"id" => $id
				)
			);

			$viewData->categories = $this->product_category_model->get_all(
				array(
					"isActive"	=> 1,
					"lang"	    => $this->session->userdata("lang")
				)
			);				
			
			/* View'e gönderilecek değişkenlerin set edilmesi */			
			$viewData->viewFolder = $this->viewFolder;
			$viewData->subViewFolder = "update";
			$viewData->item = $item;		
			
			$this->load->view("{$viewData->viewFolder}/{$viewData->subViewFolder}/index", $viewData);			
		}
		public function update($id)
		{
			if(!(isAllowedUpdateModule())){
				redirect(base_url($this->router->fetch_class()));
			}

			$this->load->library("form_validation");
			//kurallar yazılır
			$this->form_validation->set_rules("title", "Başlık", "required|trim");
			$this->form_validation->set_rules("category_id", "Kategori", "required|trim");
			$this->form_validation->set_message(
				array(
					"required" => "{field} alanı doldurulmalıdır"
				)
			);
			//Form Validation Çalıştırılır
			$validate = $this->form_validation->run();
			//kontrol edilir
			if($validate)
			{
				$update = $this->product_model->update(
					array(
						"id" => $id
					),
					array(
						"title"         => $this->input->post("title"),
						"description"   => $this->input->post("description"),
						"lang"	        => $this->session->userdata("lang"),						
						"category_id"   => $this->input->post("category_id"),
						"url"           => convertToSeo($this->input->post("title"))
					)
				);
				
				if($update)
				{
					$alert = array(
						"title"  => "İşlem Başarılı",
						"text"   => "Güncelleme işlemi başarılı bir şekilde gerçekleştirildi",
						"type"	 => "success"
					);					
				}else{
					$alert = array(
						"title"  => "İşlem Başarısız",
						"text"   => "Güncelleme İşlemi Gerçekleştirilemedi",
						"type"	 => "error"
					);					
				}
				$this->session->set_flashdata("alert", $alert);
				redirect(base_url("product"));				
			}
			else
			{
				$viewData = new stdClass();
				
				/* Tablodan verilerin getirilmesi */			
				$item = $this->product_model->get(
					array(
						"id" => $id
					)
				);					
				/* View'e gönderilecek değişkenlerin set edilmesi */				
				$viewData->viewFolder = $this->viewFolder;
				$viewData->subViewFolder = "update";
				$viewData->form_error = true;
				$viewData->item = $item;
				$this->load->view("{$viewData->viewFolder}/{$viewData->subViewFolder}/index", $viewData);				
			}			
		}
		
		public function delete($id)
		{
			if(!(isAllowedDeleteModule())){
				redirect(base_url($this->router->fetch_class()));
			}

			$delete = $this->product_model->delete(
				array(
					"id" => $id
				)
			);

			$images = $this->product_image_model->get_all(
				array(
					"product_id"	=> $id
				)
			);
			
			if($delete)
			{
				foreach($images as $image){
					$fileName = $this->product_image_model->get(
						array(
							"id"  =>  $image->id
						)
					);
					$delete = $this->product_image_model->delete(
						array(
							"id" => $image->id
						)
					);
					if($delete)
					{
						// DB den silme işlemi başarılıysa resim dosyasını da sileriz.
						unlink("uploads/{$this->viewFolder}/333x200/$fileName->img_url");
						unlink("uploads/{$this->viewFolder}/352x171/$fileName->img_url");
						unlink("uploads/{$this->viewFolder}/1080x426/$fileName->img_url");	
					}					

				}
				

				$alert = array(
					"title"  => "İşlem Başarılı",
					"text"   => "Kayıt başarılı bir şekilde silindi",
					"type"	 => "success"
				);					
			}else{
				$alert = array(
					"title"  => "İşlem Başarısız",
					"text"   => "Silme İşlemi Gerçekleştirilemedi",
					"type"	 => "error"
				);					
			}
			$this->session->set_flashdata("alert", $alert);
			redirect(base_url("product"));	
		}
		
		public function imageDelete($id, $parent_id)
		{
			if(!(isAllowedDeleteModule())){
				redirect(base_url($this->router->fetch_class()));
			}

			$fileName = $this->product_image_model->get(
				array(
					"id"  =>  $id
				)
			);
			$delete = $this->product_image_model->delete(
				array(
					"id" => $id
				)
			);			
			
			if($delete)
			{
				// DB den silme işlemi başarılıysa resim dosyasını da sileriz.
				unlink("uploads/{$this->viewFolder}/333x200/$fileName->img_url");
				unlink("uploads/{$this->viewFolder}/352x171/$fileName->img_url");
				unlink("uploads/{$this->viewFolder}/1080x426/$fileName->img_url");
				$alert = array(
					"title"  => "İşlem Başarılı",
					"text"   => "Kayıt başarılı bir şekilde silindi",
					"type"	 => "success"
				);	

			}else{
				$alert = array(
					"title"  => "İşlem Başarısız",
					"text"   => "Silme İşlemi Gerçekleştirilemedi",
					"type"	 => "error"
				);					
			}
			$this->session->set_flashdata("alert", $alert);				
				
			redirect(base_url("product/image_form/$parent_id"));
				
		}		

		public function isActiveSetter($id)
		{
			if(!(isAllowedUpdateModule())){
				die();
			}

			if($id)
			{
				$isActive = ($this->input->post("data") === "true") ? 1 : 0;
				
				$this->product_model->update(
					array(
						"id" => $id
					),
					array(
						"isActive" => $isActive
					)
				);
			}	
		}
		
		public function imageIsActiveSetter($id)
		{
			if(!(isAllowedUpdateModule())){
				die();
			}

			if($id)
			{
				$isActive = ($this->input->post("data") === "true") ? 1 : 0;
				
				$this->product_image_model->update(
					array(
						"id" => $id
					),
					array(
						"isActive" => $isActive
					)
				);
			}	
		}		
		
		public function isCoverSetter($id, $parent_id)
		{
			if(!(isAllowedUpdateModule())){
				die();
			}

			if($id && $parent_id)
			{
				$isCover = ($this->input->post("data") === "true") ? 1 : 0;
				// Kapak yapılmak istenen kayıt
				$this->product_image_model->update(
					array(
						"id"          => $id,
						"product_id"  => $parent_id
					),
					array(
						"isCover" => $isCover
					)
				);
				// Kapak fotoğrafı olmayan fotoğraflarıın checkbox 0 yapılır
				$this->product_image_model->update(
					array(
						"id !="          => $id,
						"product_id"  => $parent_id
					),
					array(
						"isCover" => 0
					)
				);	
				// ekrandaki verilerin güncellenmesi için render_hml methodu uygulanır
				$viewData = new stdClass();
				
				/* View'e gönderilecek değişkenlerin set edilmesi */			
				$viewData->viewFolder = $this->viewFolder;
				$viewData->subViewFolder = "image";
				
				$viewData->item_images = $this->product_image_model->get_all(
					array(
						"product_id" => $parent_id
					), "rank ASC"
				);			
				$render_html = $this->load->view("{$viewData->viewFolder}/{$viewData->subViewFolder}/render_elements/image_list_v", $viewData, true);
				echo $render_html;
				
			}	
		}		

		public function rankSetter()
		{
			if(!(isAllowedUpdateModule())){
				die();
			}

			$data = $this->input->post("data");
			parse_str($data, $order);
			$items = $order["ord"];
			
			foreach($items as $rank => $id)
			{
				$this->product_model->update(
					array(
						"id" => $id,
						"rank !=" => $rank
					),
					array(
						"rank" => $rank
					)
				);
			}
		}

		public function imageRankSetter()
		{
			if(!(isAllowedUpdateModule())){
				die();
			}

			$data = $this->input->post("data");
			parse_str($data, $order);
			$items = $order["ord"];
			
			foreach($items as $rank => $id)
			{
				$this->product_image_model->update(
					array(
						"id" => $id,
						"rank !=" => $rank
					),
					array(
						"rank" => $rank
					)
				);
			}
		}
		public function image_form($id)
		{
			if(!(isAllowedViewModule())){
				redirect(base_url());
			}

			$viewData = new stdClass();
			
			/* View'e gönderilecek değişkenlerin set edilmesi */			
			$viewData->viewFolder = $this->viewFolder;
			$viewData->subViewFolder = "image";
			$viewData->item = $this->product_model->get(
				array(
					"id" => $id
				)
			);
			
			$viewData->item_images = $this->product_image_model->get_all(
				array(
					"product_id" => $id
				), "rank ASC"
			);	
			
			$this->load->view("{$viewData->viewFolder}/{$viewData->subViewFolder}/index", $viewData);			
		}	
		
		public function image_upload($id)
		{

			if(!(isAllowedWriteModule())){
				redirect(base_url($this->router->fetch_class()));
			}

			// Dosya adı düzenlemesi;
			$ext = pathinfo($_FILES["file"]["name"], PATHINFO_EXTENSION);
			$file_name_stamm = convertToSeo(pathinfo($_FILES["file"]["name"], PATHINFO_FILENAME));
			$file_name = $file_name_stamm. "." .$ext;
			$file = $_FILES["file"]["tmp_name"];
			$uploadPath = "uploads/$this->viewFolder/";	

			
			$image_333x200 = upload_picture($file, $uploadPath, 333, 200, $file_name);
			$image_352x171 = upload_picture($file, $uploadPath, 352, 171, $file_name);
			$image_1080x426 = upload_picture($file, $uploadPath, 1080, 426, $file_name);
			
			if($image_333x200 && $image_1080x426 && $image_352x171)
			{
								
				$this->product_image_model->add(
					array(
						"img_url"     => $file_name,
						"rank"        => 0,
						"isActive"    => 1,
						"isCover"     => 0,
						"createdAt"   => date("Y-m-d H:i:s"),
						"product_id"  => $id	
					)
				);
			}else{
				echo "bir sorunla karşılaşıldı";
			}	
		}
		
		public function refresh_image_list($id)
		{
			if(!(isAllowedViewModule())){
				redirect(base_url());
			}
						
			$viewData = new stdClass();
			
			/* View'e gönderilecek değişkenlerin set edilmesi */			
			$viewData->viewFolder = $this->viewFolder;
			$viewData->subViewFolder = "image";
			
			$viewData->item_images = $this->product_image_model->get_all(
				array(
					"product_id" => $id
				)
			);			
			$render_html = $this->load->view("{$viewData->viewFolder}/{$viewData->subViewFolder}/render_elements/image_list_v", $viewData, true);
			echo $render_html;
		}
}

  ?>