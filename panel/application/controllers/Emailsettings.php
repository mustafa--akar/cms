<?php

class Emailsettings extends MY_CONTROLLER
{
		public $viewFolder = "";
		
		public function __construct()
		{
			parent::__construct();
			$this->viewFolder = "email_settings_v";
			$this->load->model("emailsettings_model");

			if(!get_active_user()){
				redirect(base_url("login"));
			}			
			
			
		}
		public function index()
		{

			if(!(isAllowedViewModule())){
				redirect(base_url());
			}

			$viewData = new stdClass();
			
			/* Tablodan verilerin getirilmesi */
			$items = $this->emailsettings_model->get_all(
				array()
			);
			
			/* View'e gönderilecek değişkenlerin set edilmesi */
			$viewData->items = $items;
			$viewData->viewFolder = $this->viewFolder;
			$viewData->subViewFolder = "list";			
			
			$this->load->view("{$viewData->viewFolder}/{$viewData->subViewFolder}/index", $viewData);
				
		}
		public function new_form()
		{		

			if(!(isAllowedWriteModule())){
				redirect(base_url($this->router->fetch_class()));
			}
			
			$viewData = new stdClass();
			
			$this->load->library("form_validation");
			/* View'e gönderilecek değişkenlerin set edilmesi */			
			$viewData->viewFolder = $this->viewFolder;
			$viewData->subViewFolder = "add";
			
			$this->load->view("{$viewData->viewFolder}/{$viewData->subViewFolder}/index", $viewData);	
		}
		public function save()
		{

			if(!(isAllowedWriteModule())){
				redirect(base_url($this->router->fetch_class()));
			}

			$this->load->library("form_validation");
			//kurallar yazılır		
							
			
			$this->form_validation->set_rules("protocol", "Protokol Numarası", "required|trim");
			$this->form_validation->set_rules("host", "E-posta Sunucusu", "required|trim");
			$this->form_validation->set_rules("port", "Port Numarası", "required|trim");
			$this->form_validation->set_rules("user_name", "Kullanıcı Adı", "required|trim");
			$this->form_validation->set_rules("user", "Ad E-posta (User)", "required|trim");
			$this->form_validation->set_rules("from", "Kimden gidecek (from)", "required|trim");
			$this->form_validation->set_rules("to", "Kime gidecek (to)", "required|trim");
			
			$this->form_validation->set_rules("password", "Şifre", "required|trim");
			
			$this->form_validation->set_message(
				array(
					"required"      => "{field} alanı doldurulmalıdır",
					"valid_email" 	=> "Lütfen geçerli bir e-posta adresi giriniz"
				)
			);
			//Form Validation Çalıştırılır
			$validate = $this->form_validation->run();
			//kontrol edilir
			if($validate)
			{
					
				$insert = $this->emailsettings_model->add(
					array(
							"protocol"     => $this->input->post("protocol"),
							"host"     	   => $this->input->post("host"),
							"port"         => $this->input->post("port"),
							"user_name"    => $this->input->post("user_name"),
							"user"         => $this->input->post("user"),
							"from"         => $this->input->post("from"),
							"to"           => $this->input->post("to"),							
							"password"     => $this->input->post("password"),							
							"isActive"     => 1,
							"createdAt"    => date("Y-m-d H:i:s")
						)					
				);				
				if($insert)
				{
					$alert = array(
						"title"  => "İşlem Başarılı",
						"text"   => "Kayıt başarılı bir şekilde eklendi",
						"type"	 => "success"
					);					
				}else{
					$alert = array(
						"title"   => "İşlem Başarısızdır",
						"text"    => "Kayıt Eklenemedi", 
						"type"	 => "error"
					);										
				}
					

				//İşlemin Sonucunu Sessiona yazma işlemi
				$this->session->set_flashdata("alert", $alert);	
				redirect(base_url("emailsettings"));
				die();
			}
			else
			{
				$viewData = new stdClass();
				
				/* View'e gönderilecek değişkenlerin set edilmesi */				
				$viewData->viewFolder = $this->viewFolder;
				$viewData->subViewFolder = "add";
				$viewData->form_error = true;			
				$this->load->view("{$viewData->viewFolder}/{$viewData->subViewFolder}/index", $viewData);				
			}			
		}

		public function update($id)
		{

			if(!(isAllowedUpdateModule())){
				redirect(base_url($this->router->fetch_class()));
			}

			$this->load->library("form_validation");
			//kurallar yazılır		
							
			
			$this->form_validation->set_rules("protocol", "Protokol Numarası", "required|trim");
			$this->form_validation->set_rules("host", "E-posta Sunucusu", "required|trim");
			$this->form_validation->set_rules("port", "Port Numarası", "required|trim");
			$this->form_validation->set_rules("user_name", "Kullanıcı Adı", "required|trim");
			$this->form_validation->set_rules("user", "Ad E-posta (User)", "required|trim");
			$this->form_validation->set_rules("from", "Kimden gidecek (from)", "required|trim");
			$this->form_validation->set_rules("to", "Kime gidecek (to)", "required|trim");
			
			$this->form_validation->set_rules("password", "Şifre", "required|trim");
			
			$this->form_validation->set_message(
				array(
					"required"      => "{field} alanı doldurulmalıdır",
					"valid_email" 	=> "Lütfen geçerli bir e-posta adresi giriniz"
				)
			);
			//Form Validation Çalıştırılır
			$validate = $this->form_validation->run();	
			
			//kontrol edilir
			if($validate)
			{
				
	
				$update = $this->emailsettings_model->update(
					array("id" => $id),
					array(
							"protocol"     => $this->input->post("protocol"),
							"host"     	   => $this->input->post("host"),
							"port"         => $this->input->post("port"),
							"user_name"    => $this->input->post("user_name"),
							"user"         => $this->input->post("user"),
							"from"         => $this->input->post("from"),
							"to"           => $this->input->post("to"),							
							"password"     => $this->input->post("password")
						)				
				);
				
				if($update)
				{
					$alert = array(
						"title"  => "İşlem Başarılı",
						"text"   => "Kayıt başarılı bir şekilde güncellendi",
						"type"	 => "success"
					);					
				}else{
					$alert = array(
						"title"   => "İşlem Başarısızdır",
						"text"    => "Kayıt Güncellenemedi", 
						"type"	 => "error"
					);										
				}
				//İşlemin Sonucunu Sessiona yazma işlemi
				$this->session->set_flashdata("alert", $alert);	
				redirect(base_url("emailsettings"));
			}
			else
			{
				$viewData = new stdClass();
				
				/* View'e gönderilecek değişkenlerin set edilmesi */				
				$viewData->viewFolder = $this->viewFolder;
				$viewData->subViewFolder = "update";
				$viewData->form_error = true;
				
				
				/* Tablodan verilerin getirilmesi */			
				$viewData->item = $this->emailsettings_model->get(
					array(
						"id" => $id
					)
				);				
				$this->load->view("{$viewData->viewFolder}/{$viewData->subViewFolder}/index", $viewData);				
			}			
		}		
		
		public function update_form($id)
		{

			if(!(isAllowedUpdateModule())){
				redirect(base_url($this->router->fetch_class()));
			}

			$viewData = new stdClass();
			
			/* Tablodan verilerin getirilmesi */			
			$item = $this->emailsettings_model->get(
				array(
					"id" => $id
				)
			);			
			
			/* View'e gönderilecek değişkenlerin set edilmesi */			
			$viewData->viewFolder = $this->viewFolder;
			$viewData->subViewFolder = "update";
			$viewData->item = $item;		
			
			$this->load->view("{$viewData->viewFolder}/{$viewData->subViewFolder}/index", $viewData);			
		}

		public function delete($id)
		{

			if(!(isAllowedDeleteModule())){
				redirect(base_url($this->router->fetch_class()));
			}

			$delete = $this->emailsettings_model->delete(
				array(
					"id" => $id
				)
			);
			
			if($delete)
			{
				$alert = array(
					"title"  => "İşlem Başarılı",
					"text"   => "Kayıt başarılı bir şekilde silindi",
					"type"	 => "success"
				);					
			}else{
				$alert = array(
					"title"  => "İşlem Başarısız",
					"text"   => "Silme İşlemi Gerçekleştirilemedi",
					"type"	 => "error"
				);					
			}
			$this->session->set_flashdata("alert", $alert);
			redirect(base_url("emailsettings"));	
		}
				

		public function isActiveSetter($id)
		{
			
			if(!(isAllowedUpdateModule())){
				die();
			}
			
			if($id)
			{
				$isActive = ($this->input->post("data") === "true") ? 1 : 0;
				
				$this->emailsettings_model->update(
					array(
						"id" => $id
					),
					array(
						"isActive" => $isActive
					)
				);
			}	
		}
		



}

  ?>