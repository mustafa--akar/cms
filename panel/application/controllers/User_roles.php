<?php

class User_roles extends MY_CONTROLLER
{
		public $viewFolder = "";
		
		public function __construct()
		{
			parent::__construct();
			$this->viewFolder = "user_roles_v";
			$this->load->model("user_role_model");
			
			if(!get_active_user()){
				redirect(base_url("login"));
			}			
			
		}
		public function index()
		{
			if(!(isAllowedViewModule())){
				redirect(base_url());
			}

			$viewData = new stdClass();
			
			/* Tablodan verilerin getirilmesi */
			$items = $this->user_role_model->get_all(
				array()
			);
			
			/* View'e gönderilecek değişkenlerin set edilmesi */
			$viewData->items = $items;
			$viewData->viewFolder = $this->viewFolder;
			$viewData->subViewFolder = "list";			
			
			$this->load->view("{$viewData->viewFolder}/{$viewData->subViewFolder}/index", $viewData);
				
		}
		public function new_form()
		{		
			if(!(isAllowedWriteModule())){
				redirect(base_url($this->router->fetch_class()));
			}

			$viewData = new stdClass();
			
			/* View'e gönderilecek değişkenlerin set edilmesi */			
			$viewData->viewFolder = $this->viewFolder;
			$viewData->subViewFolder = "add";
			
			$this->load->view("{$viewData->viewFolder}/{$viewData->subViewFolder}/index", $viewData);	
		}
		public function save()
		{
			if(!(isAllowedWriteModule())){
				redirect(base_url($this->router->fetch_class()));
			}

			$this->load->library("form_validation");
			//kurallar yazılır		
					
			
			$this->form_validation->set_rules("title", "Başlık", "required|trim");
			$this->form_validation->set_message(
				array(
					"required" => "{field} alanı doldurulmalıdır"
				)
			);
			//Form Validation Çalıştırılır
			$validate = $this->form_validation->run();
			//kontrol edilir
			if($validate)
			{
					
				$insert = $this->user_role_model->add(
					array(
							"title"         => $this->input->post("title"),										
							"isActive"      => 1,
							"createdAt"     => date("Y-m-d H:i:s")
						)					
				);				
				if($insert)
				{
					$alert = array(
						"title"  => "İşlem Başarılı",
						"text"   => "Kayıt başarılı bir şekilde eklendi",
						"type"	 => "success"
					);					
				}else{
					$alert = array(
						"title"   => "İşlem Başarısızdır",
						"text"    => "Kayıt Eklenemedi", 
						"type"	 => "error"
					);										
				}			

				//İşlemin Sonucunu Sessiona yazma işlemi
				$this->session->set_flashdata("alert", $alert);	
				redirect(base_url("user_roles"));
			}
			else
			{
				$viewData = new stdClass();
				
				/* View'e gönderilecek değişkenlerin set edilmesi */				
				$viewData->viewFolder = $this->viewFolder;
				$viewData->subViewFolder = "add";
				$viewData->form_error = true;			
				$this->load->view("{$viewData->viewFolder}/{$viewData->subViewFolder}/index", $viewData);				
			}			
		}

		public function update($id)
		{
			if(!(isAllowedUpdateModule())){
				redirect(base_url($this->router->fetch_class()));
			}

			$this->load->library("form_validation");
			//kurallar yazılır
			
			$this->form_validation->set_rules("title", "Başlık", "required|trim");
			$this->form_validation->set_message(
				array(
					"required" => "{field} alanı doldurulmalıdır"
				)
			);
			//Form Validation Çalıştırılır
			$validate = $this->form_validation->run();
			//kontrol edilir
			if($validate)
			{											
				$data = array("title" => $this->input->post("title"));			
		
				$update = $this->user_role_model->update(array("id" => $id),$data);
				
				if($update)
				{
					$alert = array(
						"title"  => "İşlem Başarılı",
						"text"   => "Kayıt başarılı bir şekilde güncellendi",
						"type"	 => "success"
					);					
				}else{
					$alert = array(
						"title"   => "İşlem Başarısızdır",
						"text"    => "Kayıt Güncellenemedi", 
						"type"	 => "error"
					);										
				}
				//İşlemin Sonucunu Sessiona yazma işlemi
				$this->session->set_flashdata("alert", $alert);	
				redirect(base_url("user_roles"));
			}
			else
			{
				$viewData = new stdClass();
				
				/* View'e gönderilecek değişkenlerin set edilmesi */				
				$viewData->viewFolder = $this->viewFolder;
				$viewData->subViewFolder = "update";
				$viewData->form_error = true;
				
				
				/* Tablodan verilerin getirilmesi */			
				$viewData->item = $this->user_role_model->get(
					array(
						"id" => $id
					)
				);				
				$this->load->view("{$viewData->viewFolder}/{$viewData->subViewFolder}/index", $viewData);				
			}			
		}		
		
		public function update_form($id)
		{
			if(!(isAllowedUpdateModule())){
				redirect(base_url($this->router->fetch_class()));
			}

			$viewData = new stdClass();
			
			/* Tablodan verilerin getirilmesi */			
			$item = $this->user_role_model->get(
				array(
					"id" => $id
				)
			);			
			
			/* View'e gönderilecek değişkenlerin set edilmesi */			
			$viewData->viewFolder = $this->viewFolder;
			$viewData->subViewFolder = "update";
			$viewData->item = $item;		
			
			$this->load->view("{$viewData->viewFolder}/{$viewData->subViewFolder}/index", $viewData);			
		}

		public function delete($id)
		{
			if(!(isAllowedDeleteModule())){
				redirect(base_url($this->router->fetch_class()));
			}

			$delete = $this->user_role_model->delete(
				array(
					"id" => $id
				)
			);
			
			if($delete)
			{
				$alert = array(
					"title"  => "İşlem Başarılı",
					"text"   => "Kayıt başarılı bir şekilde silindi",
					"type"	 => "success"
				);					
			}else{
				$alert = array(
					"title"  => "İşlem Başarısız",
					"text"   => "Silme İşlemi Gerçekleştirilemedi",
					"type"	 => "error"
				);					
			}
			$this->session->set_flashdata("alert", $alert);
			redirect(base_url("user_roles"));	
		}
				

		public function isActiveSetter($id)
		{
			if(!(isAllowedUpdateModule())){
				die();
			}

			if($id)
			{
				$isActive = ($this->input->post("data") === "true") ? 1 : 0;
				
				$this->user_role_model->update(
					array(
						"id" => $id
					),
					array(
						"isActive" => $isActive
					)
				);
			}	
		}
		
		public function permissions_form($id)
		{
			if(!(isAllowedViewModule())){
				redirect(base_url());
			}

			$viewData = new stdClass();
			
			/* Tablodan verilerin getirilmesi */			
			$item = $this->user_role_model->get(
				array(
					"id" => $id
				)
			);			
			
			/* View'e gönderilecek değişkenlerin set edilmesi */			
			$viewData->viewFolder = $this->viewFolder;
			$viewData->subViewFolder = "permissions";
			$viewData->item = $item;		
			
			$this->load->view("{$viewData->viewFolder}/{$viewData->subViewFolder}/index", $viewData);			
		}			
		
		public function update_permissions($id)
		{
			if(!(isAllowedUpdateModule())){
				redirect(base_url($this->router->fetch_class()));
			}
						
			$permissions = json_encode($this->input->post("permissions"));

	
			$update = $this->user_role_model->update(
				array("id" => $id),
				array(
						"permissions"     => $permissions
					)				
			);
			
			if($update)
			{
				$alert = array(
					"title"  => "İşlem Başarılı",
					"text"   => "Yetki tanımı başarılı bir şekilde güncellendi",
					"type"	 => "success"
				);					
			}else{
				$alert = array(
					"title"   => "İşlem Başarısızdır",
					"text"    => "Yetki tanımı Güncellenemedi", 
					"type"	 => "error"
				);										
			}
			//İşlemin Sonucunu Sessiona yazma işlemi
			$this->session->set_flashdata("alert", $alert);	
			redirect(base_url("user_roles/permissions_form/$id"));
		}

}

  ?>