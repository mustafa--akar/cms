<?php

class News extends MY_CONTROLLER
{
		public $viewFolder = "";
		
		public function __construct()
		{
			parent::__construct();
			$this->viewFolder = "news_v";
			$this->load->model("news_model");
			
			if(!get_active_user()){
				redirect(base_url("login"));
			}		
			
		}
		public function index()
		{

			if(!(isAllowedViewModule())){
				redirect(base_url());
			}

			$viewData = new stdClass();
			
			/* Tablodan verilerin getirilmesi */
			$items = $this->news_model->get_all(
				array(
					"lang"	=> $this->session->userdata("lang")
				), "rank ASC"
			);
			
			/* View'e gönderilecek değişkenlerin set edilmesi */
			$viewData->items = $items;
			$viewData->viewFolder = $this->viewFolder;
			$viewData->subViewFolder = "list";			
			
			$this->load->view("{$viewData->viewFolder}/{$viewData->subViewFolder}/index", $viewData);
				
		}
		public function new_form()
		{		

			if(!(isAllowedWriteModule())){
				redirect(base_url($this->router->fetch_class()));
			}

			$viewData = new stdClass();
			
			/* View'e gönderilecek değişkenlerin set edilmesi */			
			$viewData->viewFolder = $this->viewFolder;
			$viewData->subViewFolder = "add";
			
			$this->load->view("{$viewData->viewFolder}/{$viewData->subViewFolder}/index", $viewData);	
		}
		public function save()
		{

			if(!(isAllowedWriteModule())){
				redirect(base_url($this->router->fetch_class()));
			}

			$this->load->library("form_validation");
			//kurallar yazılır
			$news_type = $this->input->post("news_type");
			if($news_type == "image")
			{
				if($_FILES["img_url"]["name"] == "")
				{
					$alert = array(
						"title"		=> "İşlem Başarısız",
						"text"		=> "Lütfen Bir Görsel Seçiniz",
						"type"		=> "error"	
					);
					// İşlem sonucunu session'a yazma işlemi...
					$this->session->set_flashdata("alert", $alert);
					
					redirect(base_url("news/new_form"));					
				}
				
			}else if($news_type == "video")
			{
				$this->form_validation->set_rules("video_url", "Video URL", "required|trim");
			}		
			
			$this->form_validation->set_rules("title", "Başlık", "required|trim");
			$this->form_validation->set_message(
				array(
					"required" => "{field} alanı doldurulmalıdır"
				)
			);
			//Form Validation Çalıştırılır
			$validate = $this->form_validation->run();
			//kontrol edilir
			if($validate)
			{
				if($news_type == "image")
				{	
					// Upload Süreci
					// Dosya adı düzenlemesi;
					$ext = pathinfo($_FILES["img_url"]["name"], PATHINFO_EXTENSION);
					$file_name_stamm = convertToSeo(pathinfo($_FILES["img_url"]["name"], PATHINFO_FILENAME));
					$file_name = $file_name_stamm. "." .$ext;
					$file = $_FILES["img_url"]["tmp_name"];
					$uploadPath = "uploads/$this->viewFolder/";	

					
					$image_513x289 = upload_picture($file, $uploadPath, 513, 289, $file_name);
					$image_730x411 = upload_picture($file, $uploadPath, 730, 411, $file_name);
					
					if($image_513x289 && $image_730x411)
					{
							
						$data = array(
										"title"         => $this->input->post("title"),
										"description"   => $this->input->post("description"),
										"lang"          => $this->input->post("lang"),
										"url"           => convertToSeo($this->input->post("title")),
										"news_type"     => $news_type,
										"img_url"       => $file_name,
										"video_url"		=> "#",
										"rank"          => 0,
										"isActive"      => 1,
										"createdAt"     => date("Y-m-d H:i:s")
									);

					}else{
						$alert = array(
							"title"   => "İşlem Başarısızdır",
							"text"    => "Görsel yüklenirken bir problem oluştu", 
							"type"	  => "error"
						);	

						//İşlemin Sonucunu Sessiona yazma işlemi
						$this->session->set_flashdata("alert", $alert);	
						redirect(base_url("news/new_form"));
						die();	
					}				
					
				}else if($news_type == "video"){
					$data = array(
									"title"         => $this->input->post("title"),
									"description"   => $this->input->post("description"),
									"lang"          => $this->input->post("lang"),
									"url"           => convertToSeo($this->input->post("title")),
									"news_type"     => $news_type,
									"img_url"       => "#",
									"video_url"		=> $this->input->post("video_url"),
									"rank"          => 0,
									"isActive"      => 1,
									"createdAt"     => date("Y-m-d H:i:s")
								);					
				}	
		
				$insert = $this->news_model->add($data);
				
				if($insert)
				{
					$alert = array(
						"title"  => "İşlem Başarılı",
						"text"   => "Kayıt başarılı bir şekilde eklendi",
						"type"	 => "success"
					);					
				}else{
					$alert = array(
						"title"   => "İşlem Başarısızdır",
						"text"    => "Kayıt Eklenemedi", 
						"type"	 => "error"
					);										
				}
				//İşlemin Sonucunu Sessiona yazma işlemi
				$this->session->set_flashdata("alert", $alert);	
				redirect(base_url("news"));
			}
			else
			{
				$viewData = new stdClass();
				
				/* View'e gönderilecek değişkenlerin set edilmesi */				
				$viewData->viewFolder = $this->viewFolder;
				$viewData->subViewFolder = "add";
				$viewData->form_error = true;
				$viewData->news_type = $news_type;
				$this->load->view("{$viewData->viewFolder}/{$viewData->subViewFolder}/index", $viewData);				
			}			
		}

		public function update($id)
		{

			if(!(isAllowedUpdateModule())){
				redirect(base_url($this->router->fetch_class()));
			}

			$this->load->library("form_validation");
			//kurallar yazılır
			$news_type = $this->input->post("news_type");
			if($news_type == "video")
			{
				$this->form_validation->set_rules("video_url", "Video URL", "required|trim");
			}		
			
			$this->form_validation->set_rules("title", "Başlık", "required|trim");
			$this->form_validation->set_message(
				array(
					"required" => "{field} alanı doldurulmalıdır"
				)
			);
			//Form Validation Çalıştırılır
			$validate = $this->form_validation->run();
			//kontrol edilir
			if($validate)
			{
				if($news_type == "image")
				{	
					if($_FILES["img_url"]["name"] !== "")
					{

						// Upload Süreci
						// Dosya adı düzenlemesi;
						$ext = pathinfo($_FILES["img_url"]["name"], PATHINFO_EXTENSION);
						$file_name_stamm = convertToSeo(pathinfo($_FILES["img_url"]["name"], PATHINFO_FILENAME));
						$file_name = $file_name_stamm. "." .$ext;
						$file = $_FILES["img_url"]["tmp_name"];
						$uploadPath = "uploads/$this->viewFolder/";	

						
						$image_513x289 = upload_picture($file, $uploadPath, 513, 289, $file_name);
						$image_730x411 = upload_picture($file, $uploadPath, 730, 411, $file_name);
						
						if($image_513x289 && $image_730x411)
						{
														
							$data = array(
										"title"         => $this->input->post("title"),
										"description"   => $this->input->post("description"),
										"lang"          => $this->input->post("lang"),
										"url"           => convertToSeo($this->input->post("title")),
										"news_type"     => $news_type,
										"img_url"       => $file_name,
										"video_url"		=> "#"
										);

						}else{
							$alert = array(
								"title"   => "İşlem Başarısızdır",
								"text"    => "Görsel yüklenirken bir problem oluştu", 
								"type"	  => "error"
							);	

							//İşlemin Sonucunu Sessiona yazma işlemi
							$this->session->set_flashdata("alert", $alert);	
							redirect(base_url("news/update_form/$id"));
								
						}				
					
						
					}else{
						$data = array(
									"title"         => $this->input->post("title"),
									"description"   => $this->input->post("description"),
									"lang"          => $this->input->post("lang"),
									"url"           => convertToSeo($this->input->post("title"))
									);						
					}	

				}else if($news_type == "video"){
					$data = array(
								"title"         => $this->input->post("title"),
								"description"   => $this->input->post("description"),
								"lang"          => $this->input->post("lang"),
								"url"           => convertToSeo($this->input->post("title")),
								"news_type"     => $news_type,
								"img_url"       => "#",
								"video_url"		=> $this->input->post("video_url")
								);					
				}	
		
				$update = $this->news_model->update(array("id" => $id),$data);
				
				if($update)
				{
					$alert = array(
						"title"  => "İşlem Başarılı",
						"text"   => "Kayıt başarılı bir şekilde güncellendi",
						"type"	 => "success"
					);					
				}else{
					$alert = array(
						"title"   => "İşlem Başarısızdır",
						"text"    => "Kayıt Güncellenemedi", 
						"type"	 => "error"
					);										
				}
				//İşlemin Sonucunu Sessiona yazma işlemi
				$this->session->set_flashdata("alert", $alert);	
				redirect(base_url("news"));
			}
			else
			{
				$viewData = new stdClass();
				
				/* View'e gönderilecek değişkenlerin set edilmesi */				
				$viewData->viewFolder = $this->viewFolder;
				$viewData->subViewFolder = "update";
				$viewData->form_error = true;
				$viewData->news_type = $news_type;
				
				/* Tablodan verilerin getirilmesi */			
				$viewData->item = $this->news_model->get(
					array(
						"id" => $id
					)
				);				
				$this->load->view("{$viewData->viewFolder}/{$viewData->subViewFolder}/index", $viewData);				
			}			
		}		
		
		public function update_form($id)
		{

			if(!(isAllowedUpdateModule())){
				redirect(base_url($this->router->fetch_class()));
			}

			$viewData = new stdClass();
			
			/* Tablodan verilerin getirilmesi */			
			$item = $this->news_model->get(
				array(
					"id" => $id
				)
			);			
			
			/* View'e gönderilecek değişkenlerin set edilmesi */			
			$viewData->viewFolder = $this->viewFolder;
			$viewData->subViewFolder = "update";
			$viewData->item = $item;		
			
			$this->load->view("{$viewData->viewFolder}/{$viewData->subViewFolder}/index", $viewData);			
		}

		public function delete($id)
		{

			if(!(isAllowedDeleteModule())){
				redirect(base_url($this->router->fetch_class()));
			}
			$news = $this->news_model->get(
				array(
					"id" => $id
				)
			);
			$delete = $this->news_model->delete(
				array(
					"id" => $id
				)
			);
			
			if($delete)
			{
				unlink("uploads/{$this->viewFolder}/513x289/$news->img_url");
				unlink("uploads/{$this->viewFolder}/730x411/$news->img_url");
				$alert = array(
					"title"  => "İşlem Başarılı",
					"text"   => "Kayıt başarılı bir şekilde silindi",
					"type"	 => "success"
				);					
			}else{
				$alert = array(
					"title"  => "İşlem Başarısız",
					"text"   => "Silme İşlemi Gerçekleştirilemedi",
					"type"	 => "error"
				);					
			}
			$this->session->set_flashdata("alert", $alert);
			redirect(base_url("news"));	
		}
				

		public function isActiveSetter($id)
		{

			if(!(isAllowedUpdateModule())){
				die();
			}

			if($id)
			{
				$isActive = ($this->input->post("data") === "true") ? 1 : 0;
				
				$this->news_model->update(
					array(
						"id" => $id
					),
					array(
						"isActive" => $isActive
					)
				);
			}	
		}
		

		public function rankSetter()
		{

			if(!(isAllowedUpdateModule())){
				die();
			}
						
			$data = $this->input->post("data");
			parse_str($data, $order);
			$items = $order["ord"];
			
			foreach($items as $rank => $id)
			{
				$this->news_model->update(
					array(
						"id" => $id,
						"rank !=" => $rank
					),
					array(
						"rank" => $rank
					)
				);
			}
		}

}

  ?>