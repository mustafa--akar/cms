<?php

class Members extends MY_CONTROLLER
{
		public $viewFolder = "";
		
		public function __construct()
		{
			parent::__construct();
			$this->viewFolder = "members_v";
			$this->load->model("member_model");
			
			if(!get_active_user()){
				redirect(base_url("login"));
			}			
			
		}
		public function index()
		{
			if(!(isAllowedViewModule())){
				redirect(base_url());
			}

			$viewData = new stdClass();

			$this->load->library("pagination");
			$config["base_url"]		= base_url("members/index");
			$config["total_rows"]   = $this->member_model->get_count();
			$config["uri_segment"]  = 3;
			$config["per_page"]		= 20;
			$config['full_tag_open'] = '<ul class="pagination">';
			$config['full_tag_close'] = '</ul>';
			$config['attributes'] = ['class' => 'page-link'];
			$config['first_link'] = false;
			$config['last_link'] = false;
			$config['first_tag_open'] = '<li class="page-item">';
			$config['first_tag_close'] = '</li>';
			$config['prev_link'] = '&laquo';
			$config['prev_tag_open'] = '<li class="page-item">';
			$config['prev_tag_close'] = '</li>';
			$config['next_link'] = '&raquo';
			$config['next_tag_open'] = '<li class="page-item">';
			$config['next_tag_close'] = '</li>';
			$config['last_tag_open'] = '<li class="page-item">';
			$config['last_tag_close'] = '</li>';
			$config['cur_tag_open'] = '<li class="page-item active"><a href="#" class="page-link">';
			$config['cur_tag_close'] = '<span class="sr-only">(current)</span></a></li>';
			$config['num_tag_open'] = '<li class="page-item">';
			$config['num_tag_close'] = '</li>';

			$this->pagination->initialize($config);

			$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;

			$viewData->links = $this->pagination->create_links();			
			
			/* Tablodan verilerin getirilmesi */
			$items = $this->member_model->get_all(
				array(), array(), $config["per_page"], $page
			);
			
			/* View'e gönderilecek değişkenlerin set edilmesi */
			$viewData->items = $items;
			$viewData->viewFolder = $this->viewFolder;
			$viewData->subViewFolder = "list";					
			
			$this->load->view("{$viewData->viewFolder}/{$viewData->subViewFolder}/index", $viewData);
				
		}
		public function new_form()
		{		

			if(!(isAllowedWriteModule())){
				redirect(base_url($this->router->fetch_class()));
			}

			$viewData = new stdClass();
			
			/* View'e gönderilecek değişkenlerin set edilmesi */			
			$viewData->viewFolder = $this->viewFolder;
			$viewData->subViewFolder = "add";
			
			$this->load->view("{$viewData->viewFolder}/{$viewData->subViewFolder}/index", $viewData);	
		}
		public function save()
		{

			if(!(isAllowedWriteModule())){
				redirect(base_url($this->router->fetch_class()));
			}

			$this->load->library("form_validation");
			//kurallar yazılır		
				
			
			$this->form_validation->set_rules("email", "E-Posta", "required|trim");
			$this->form_validation->set_message(
				array(
					"required" => "{field} alanı doldurulmalıdır"
				)
			);
			//Form Validation Çalıştırılır
			$validate = $this->form_validation->run();
			//kontrol edilir
			if($validate)
			{				
				$member = $this->member_model->get(
					array(
						"email"	=> $this->input->post("email")
					)
				);
				if($member){
						$alert = array(
							"title"   => "İşlem Başarısızdır",
							"text"    => "Bu E-Posta adresi zaten kayıtlı", 
							"type"	  => "error"
						);	
				}else{
					$insert = $this->member_model->add(
						array(
								"email"         => $this->input->post("email"),	
								"ip_address"	=> "CMS",			
								"isActive"      => 1,
								"createdAt"     => date("Y-m-d H:i:s")
							)					
					);				
					if($insert)
					{
						$alert = array(
							"title"  => "İşlem Başarılı",
							"text"   => "Kayıt başarılı bir şekilde eklendi",
							"type"	 => "success"
						);					
					}else{
						$alert = array(
							"title"   => "İşlem Başarısızdır",
							"text"    => "Kayıt Eklenemedi", 
							"type"	  => "error"
						);										
					}
				}

				//İşlemin Sonucunu Sessiona yazma işlemi
				$this->session->set_flashdata("alert", $alert);	
				redirect(base_url("members"));

			}
			else
			{
				$viewData = new stdClass();
				
				/* View'e gönderilecek değişkenlerin set edilmesi */				
				$viewData->viewFolder = $this->viewFolder;
				$viewData->subViewFolder = "add";
				$viewData->form_error = true;			
				$this->load->view("{$viewData->viewFolder}/{$viewData->subViewFolder}/index", $viewData);				
			}			
		}

		public function update($id)
		{

			if(!(isAllowedUpdateModule())){
				redirect(base_url($this->router->fetch_class()));
			}

			$this->load->library("form_validation");
			//kurallar yazılır
			
			$this->form_validation->set_rules("email", "E-Posta", "required|trim");
			$this->form_validation->set_message(
				array(
					"required" => "{field} alanı doldurulmalıdır"
				)
			);
			//Form Validation Çalıştırılır
			$validate = $this->form_validation->run();
			//kontrol edilir
			if($validate)
			{

				$member = $this->member_model->get(
					array(
						"email"	=> $this->input->post("email"),
						"id !="	=> $id
					)
				);
				if($member){
						$alert = array(
							"title"   => "İşlem Başarısızdır",
							"text"    => "Bu E-Posta adresi zaten kayıtlı", 
							"type"	 => "error"
						);	
				}else{
					$data = array(
								"email"  => $this->input->post("email")
						);		
			
					$update = $this->member_model->update(array("id" => $id),$data);
					
					if($update)
					{
						$alert = array(
							"title"  => "İşlem Başarılı",
							"text"   => "Kayıt başarılı bir şekilde güncellendi",
							"type"	 => "success"
						);					
					}else{
						$alert = array(
							"title"   => "İşlem Başarısızdır",
							"text"    => "Kayıt Güncellenemedi", 
							"type"	 => "error"
						);										
					}

				}

				//İşlemin Sonucunu Sessiona yazma işlemi
				$this->session->set_flashdata("alert", $alert);	
				redirect(base_url("members"));
			}
			else
			{
				$viewData = new stdClass();
				
				/* View'e gönderilecek değişkenlerin set edilmesi */				
				$viewData->viewFolder = $this->viewFolder;
				$viewData->subViewFolder = "update";
				$viewData->form_error = true;
				
				
				/* Tablodan verilerin getirilmesi */			
				$viewData->item = $this->member_model->get(
					array(
						"id" => $id
					)
				);				
				$this->load->view("{$viewData->viewFolder}/{$viewData->subViewFolder}/index", $viewData);				
			}			
		}		
		
		public function update_form($id)
		{

			if(!(isAllowedUpdateModule())){
				redirect(base_url($this->router->fetch_class()));
			}

			$viewData = new stdClass();
			
			/* Tablodan verilerin getirilmesi */			
			$item = $this->member_model->get(
				array(
					"id" => $id
				)
			);			
			
			/* View'e gönderilecek değişkenlerin set edilmesi */			
			$viewData->viewFolder = $this->viewFolder;
			$viewData->subViewFolder = "update";
			$viewData->item = $item;		
			
			$this->load->view("{$viewData->viewFolder}/{$viewData->subViewFolder}/index", $viewData);			
		}

		public function delete($id)
		{

			if(!(isAllowedDeleteModule())){
				redirect(base_url($this->router->fetch_class()));
			}

			$delete = $this->member_model->delete(
				array(
					"id" => $id
				)
			);
			
			if($delete)
			{
				$alert = array(
					"title"  => "İşlem Başarılı",
					"text"   => "Kayıt başarılı bir şekilde silindi",
					"type"	 => "success"
				);					
			}else{
				$alert = array(
					"title"  => "İşlem Başarısız",
					"text"   => "Silme İşlemi Gerçekleştirilemedi",
					"type"	 => "error"
				);					
			}
			$this->session->set_flashdata("alert", $alert);
			redirect(base_url("members"));	
		}
				

		public function isActiveSetter($id)
		{

			if(!(isAllowedUpdateModule())){
				die();
			}
						
			if($id)
			{
				$isActive = ($this->input->post("data") === "true") ? 1 : 0;
				
				$this->member_model->update(
					array(
						"id" => $id
					),
					array(
						"isActive" => $isActive
					)
				);
			}	
		}
		

}

  ?>