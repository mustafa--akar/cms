<?php

class Settings extends MY_CONTROLLER
{
		public $viewFolder = "";
		
		public function __construct()
		{
			parent::__construct();
			$this->viewFolder = "settings_v";
			$this->load->model("settings_model");

			if(!get_active_user()){
				redirect(base_url("login"));
			}			
			
			
		}
		public function index()
		{
			if(!(isAllowedViewModule())){
				redirect(base_url());
			}

			$viewData = new stdClass();
			
			/* Tablodan verilerin getirilmesi */
			$item = $this->settings_model->get(
				array(
					"lang"	=> $this->session->userdata("lang")
				)
			);
			
			if($item){
				$viewData->subViewFolder = "update";
			}else{
				$viewData->subViewFolder = "no_content";
			}
			$this->load->model("gallery_model");
			$viewData->galleries = $this->gallery_model->get_all(
				array(
					"isActive"	    => 1,
					"lang"	=> $this->session->userdata("lang"),
					"gallery_type"	=> "image"
				)
			); 		
						
			/* View'e gönderilecek değişkenlerin set edilmesi */
			$viewData->item = $item;
			$viewData->viewFolder = $this->viewFolder;
						
			
			$this->load->view("{$viewData->viewFolder}/{$viewData->subViewFolder}/index", $viewData);
				
		}
		public function new_form()
		{		
			if(!(isAllowedWriteModule())){
				redirect(base_url($this->router->fetch_class()));
			}

			$viewData = new stdClass();
			$this->load->model("gallery_model");
			$viewData->galleries = $this->gallery_model->get_all(
				array(
					"isActive"	    => 1,
					"lang"	=> $this->session->userdata("lang"),
					"gallery_type"	=> "image"
				)
			); 		
							
			$viewData->viewFolder = $this->viewFolder;
			$viewData->subViewFolder = "add";
			
			$this->load->view("{$viewData->viewFolder}/{$viewData->subViewFolder}/index", $viewData);	
		}
		public function save()
		{
			if(!(isAllowedWriteModule())){
				redirect(base_url($this->router->fetch_class()));
			}

			$this->load->library("form_validation");
			//kurallar yazılır		
			
			if($_FILES["logo"]["name"] == "")
			{
				$alert = array(
					"title"		=> "İşlem Başarısız",
					"text"		=> "Lütfen Bir Görsel Seçiniz",
					"type"		=> "error"	
				);
				// İşlem sonucunu session'a yazma işlemi...
				$this->session->set_flashdata("alert", $alert);
				
				redirect(base_url("settings/new_form"));	
				die();	
			}				
			
			$this->form_validation->set_rules("company_name", "Şirket Adı", "required|trim");
			$this->form_validation->set_rules("phone_1", "Telefon 1", "required|trim");
			$this->form_validation->set_rules("email", "E-posta Adresi", "required|trim|valid_email");			
			$this->form_validation->set_message(
				array(
					"required"    => "{field} alanı doldurulmalıdır",
					"valid_email" => "Lütfen geçerli bir <b> {field} </b> adresi giriniz"
				)
			);
			//Form Validation Çalıştırılır
			$validate = $this->form_validation->run();
			//kontrol edilir
			if($validate)
			{
				
				// Upload Süreci
				// Dosya adı düzenlemesi;
				$ext = pathinfo($_FILES["logo"]["name"], PATHINFO_EXTENSION);
				$file_name_stamm = convertToSeo($this->input->post("company_name"));
				$file_name = $file_name_stamm. "." .$ext;
				$file = $_FILES["logo"]["tmp_name"];
				$uploadPath = "uploads/$this->viewFolder/";	

				
				$image_150x35 = upload_picture($file, $uploadPath, 150, 35, $file_name);
				
				if($image_150x35)
				{
					
					$insert = $this->settings_model->add(
						array(
								"company_name"     => $this->input->post("company_name"),							
								"phone_1"          => $this->input->post("phone_1"),
								"phone_2"          => $this->input->post("phone_2"),								
								"fax_1"            => $this->input->post("fax_1"),
								"fax_2"            => $this->input->post("fax_2"),
								"address"          => $this->input->post("address"),
								"lat"              => $this->input->post("lat"),
								"long"             => $this->input->post("long"),
								"about_us"         => $this->input->post("about_us"),
								"mission"          => $this->input->post("mission"),
								"vision"           => $this->input->post("vision"),
								"motto"            => $this->input->post("motto"),
								"short_motto"      => $this->input->post("short_motto"),
								"homepage_text"    => $this->input->post("homepage_text"),
								"homepage_gallery_id"    => $this->input->post("homepage_gallery_id"),
								"homepage_references_description"    => $this->input->post("homepage_references_description"),
								"email"            => $this->input->post("email"),
								"facebook"         => $this->input->post("facebook"),
								"twitter"          => $this->input->post("twitter"),
								"instagram"        => $this->input->post("instagram"),
								"linkedin"         => $this->input->post("linkedin"),
								"lang"             => $this->session->userdata("lang"),
								"logo"			   => $file_name,								
								"createdAt"        => date("Y-m-d H:i:s")
							)					
					);				
					if($insert)
					{
						$alert = array(
							"title"  => "İşlem Başarılı",
							"text"   => "Kayıt başarılı bir şekilde eklendi",
							"type"	 => "success"
						);					
					}else{
						$alert = array(
							"title"   => "İşlem Başarısızdır",
							"text"    => "Kayıt Eklenemedi", 
							"type"	  => "error"
						);										
					}

				}else{
					$alert = array(
						"title"   => "İşlem Başarısızdır",
						"text"    => "Görsel yüklenirken bir problem oluştu", 
						"type"	  => "error"
					);	

					//İşlemin Sonucunu Sessiona yazma işlemi
					$this->session->set_flashdata("alert", $alert);	
					redirect(base_url("settings/new_form"));
					die();	
				}				

				//settings session değerlerinin atanması
				$settings = $this->settings_model->get();
				$this->session->set_userdata("settings", $settings);
				
				//İşlemin Sonucunu Sessiona yazma işlemi
				$this->session->set_flashdata("alert", $alert);	
				redirect(base_url("settings"));
			}
			else
			{
				$viewData = new stdClass();
				
				/* View'e gönderilecek değişkenlerin set edilmesi */				
				$viewData->viewFolder = $this->viewFolder;
				$viewData->subViewFolder = "add";
				$viewData->form_error = true;			
				$this->load->view("{$viewData->viewFolder}/{$viewData->subViewFolder}/index", $viewData);				
			}

			
		}

		public function update($id)
		{
			if(!(isAllowedUpdateModule())){
				redirect(base_url($this->router->fetch_class()));
			}

			$this->load->library("form_validation");
			//kurallar yazılır						
			
			$this->form_validation->set_rules("company_name", "Şirket Adı", "required|trim");
			$this->form_validation->set_rules("phone_1", "Telefon 1", "required|trim");
			$this->form_validation->set_rules("email", "E-posta Adresi", "required|trim|valid_email");		
			$this->form_validation->set_message(
				array(
					"required"    => "{field} alanı doldurulmalıdır",
					"valid_email" => "Lütfen geçerli bir <b> {field} </b> adresi giriniz"
				)
			);
			//Form Validation Çalıştırılır
			$validate = $this->form_validation->run();
			//kontrol edilir
			if($validate)
			{
				// Eğer resim seçildiyse resmi güncellemem ve yeni resmi eklemem gerek	
				if($_FILES["logo"]["name"] !== "")
				{

					// Upload Süreci
					// Dosya adı düzenlemesi;
					$ext = pathinfo($_FILES["logo"]["name"], PATHINFO_EXTENSION);
					$file_name_stamm = convertToSeo($this->input->post("company_name"));
					$file_name = $file_name_stamm. "." .$ext;
					$file = $_FILES["logo"]["tmp_name"];
					$uploadPath = "uploads/$this->viewFolder/";	

					
					$image_150x35 = upload_picture($file, $uploadPath, 150, 35, $file_name);
					
					if($image_150x35)
					{
												
						$data = array(
									"company_name"     => $this->input->post("company_name"),						
									"phone_1"          => $this->input->post("phone_1"),
									"phone_2"          => $this->input->post("phone_2"),							
									"fax_1"            => $this->input->post("fax_1"),
									"fax_2"            => $this->input->post("fax_2"),
									"address"          => $this->input->post("address"),
									"lat"              => $this->input->post("lat"),
									"long"             => $this->input->post("long"),								
									"about_us"         => $this->input->post("about_us"),
									"mission"          => $this->input->post("mission"),
									"vision"           => $this->input->post("vision"),
									"motto"            => $this->input->post("motto"),
								    "short_motto"      => $this->input->post("short_motto"),
								    "homepage_text"    => $this->input->post("homepage_text"),
							    	"homepage_gallery_id"    => $this->input->post("homepage_gallery_id"),

							     	"homepage_references_description"    => $this->input->post("homepage_references_description"),								    
									"email"            => $this->input->post("email"),
									"facebook"         => $this->input->post("facebook"),
									"twitter"          => $this->input->post("twitter"),
									"instagram"        => $this->input->post("instagram"),
									"linkedin"         => $this->input->post("linkedin"),
									"lang"             => $this->session->userdata("lang"),
									"logo"			   => $file_name,								
									"updatedAt"        => date("Y-m-d H:i:s")
									);

					}else{
						$alert = array(
							"title"   => "İşlem Başarısızdır",
							"text"    => "Görsel yüklenirken bir problem oluştu", 
							"type"	  => "error"
						);	

						//İşlemin Sonucunu Sessiona yazma işlemi
						$this->session->set_flashdata("alert", $alert);	
						redirect(base_url("settings/update_form/$id"));
							
					}				
				
				// Eğer resim seçilmediyse sadece title ve description değerlerini güncelle resmi muhafaza et.
				}else{
					$data = array(
								"company_name"     => $this->input->post("company_name"),							
								"phone_1"          => $this->input->post("phone_1"),
								"phone_2"          => $this->input->post("phone_2"),								
								"fax_1"            => $this->input->post("fax_1"),
								"fax_2"            => $this->input->post("fax_2"),
								"address"          => $this->input->post("address"),
								"lat"              => $this->input->post("lat"),
								"long"             => $this->input->post("long"),								
								"about_us"         => $this->input->post("about_us"),
								"mission"          => $this->input->post("mission"),
								"vision"           => $this->input->post("vision"),
								"motto"            => $this->input->post("motto"),
								"short_motto"      => $this->input->post("short_motto"),
								"homepage_text"    => $this->input->post("homepage_text"),
								"homepage_gallery_id"    => $this->input->post("homepage_gallery_id"),			
								"homepage_references_description"    => $this->input->post("homepage_references_description"),											
								"email"            => $this->input->post("email"),
								"facebook"         => $this->input->post("facebook"),
								"twitter"          => $this->input->post("twitter"),
								"instagram"        => $this->input->post("instagram"),
								"linkedin"         => $this->input->post("linkedin"),	
								"lang"             => $this->session->userdata("lang"),							
								"updatedAt"        => date("Y-m-d H:i:s")
								);						
				}	

		
		
				$update = $this->settings_model->update(array("id" => $id),$data);
				
				if($update)
				{
					$alert = array(
						"title"  => "İşlem Başarılı",
						"text"   => "Kayıt başarılı bir şekilde güncellendi",
						"type"	 => "success"
					);					
				}else{
					$alert = array(
						"title"   => "İşlem Başarısızdır",
						"text"    => "Kayıt Güncellenemedi", 
						"type"	 => "error"
					);										
				}
				
				// Session update işlemi
				
				$settings = $this->settings_model->get();
				$this->session->set_userdata("settings", $settings);				
				
				//İşlemin Sonucunu Sessiona yazma işlemi
				$this->session->set_flashdata("alert", $alert);	
				redirect(base_url("settings"));
			}
			else
			{
				$viewData = new stdClass();
				
				/* View'e gönderilecek değişkenlerin set edilmesi */				
				$viewData->viewFolder = $this->viewFolder;
				$viewData->subViewFolder = "update";
				$viewData->form_error = true;
				
				
				/* Tablodan verilerin getirilmesi */			
				$viewData->item = $this->settings_model->get(
					array(
						"id" => $id
					)
				);				
				$this->load->view("{$viewData->viewFolder}/{$viewData->subViewFolder}/index", $viewData);				
			}			
		}			
		
		public function update_form($id)
		{
			if(!(isAllowedUpdateModule())){
				redirect(base_url($this->router->fetch_class()));
			}
						
			$viewData = new stdClass();
			
			/* Tablodan verilerin getirilmesi */			
			$item = $this->settings_model->get(
				array(
					"id" => $id
				)
			);	

			/* View'e gönderilecek değişkenlerin set edilmesi */			
			$viewData->viewFolder = $this->viewFolder;
			$viewData->subViewFolder = "update";
			$viewData->item = $item;		
			
			$this->load->view("{$viewData->viewFolder}/{$viewData->subViewFolder}/index", $viewData);			
		}

					
}

  ?>