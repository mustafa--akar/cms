<?php

class Product_categories extends MY_CONTROLLER
{
		public $viewFolder = "";
		
		public function __construct()
		{
			parent::__construct();
			$this->viewFolder = "product_categories_v";
			$this->load->model("product_category_model");
			
			if(!get_active_user()){
				redirect(base_url("login"));
			}			
			
		}
		public function index()
		{
			if(!(isAllowedViewModule())){
				redirect(base_url());
			}

			$viewData = new stdClass();
			
			/* Tablodan verilerin getirilmesi */
			$items = $this->product_category_model->get_all(
				array(
					"lang"	=> $this->session->userdata("lang")
				)
			);
			$tree = $this->buildTree($items);			
			
			/* View'e gönderilecek değişkenlerin set edilmesi */
			$viewData->items = $items;	
			$viewData->tree = $tree;			
			$viewData->viewFolder = $this->viewFolder;
			$viewData->subViewFolder = "list";					
			
			$this->load->view("{$viewData->viewFolder}/{$viewData->subViewFolder}/index", $viewData);
				
		}
		public function new_form()
		{		
			if(!(isAllowedWriteModule())){
				redirect(base_url($this->router->fetch_class()));
			}

			$viewData = new stdClass();
			
			$items = $this->product_category_model->get_all(
				array(
					"isActive"	=> 1,
					"lang"	    => $this->session->userdata("lang")
				)
			);
					
			/* View'e gönderilecek değişkenlerin set edilmesi */
			$viewData->categories = $items;	
			$viewData->viewFolder = $this->viewFolder;
			$viewData->subViewFolder = "add";
			
			$this->load->view("{$viewData->viewFolder}/{$viewData->subViewFolder}/index", $viewData);	
		}
		public function save()
		{
			if(!(isAllowedWriteModule())){
				redirect(base_url($this->router->fetch_class()));
			}

			$this->load->library("form_validation");
			//kurallar yazılır		
				
			
			$this->form_validation->set_rules("title", "Başlık", "required|trim");
			$this->form_validation->set_message(
				array(
					"required" => "{field} alanı doldurulmalıdır"
				)
			);
			//Form Validation Çalıştırılır
			$validate = $this->form_validation->run();
			//kontrol edilir
			if($validate)
			{
				
				if($this->input->post("parent_id")){
					$parent_id = $this->input->post("parent_id");
				}else{
					$parent_id = 0;
				}
				$insert = $this->product_category_model->add(
					array(
							"title"         => $this->input->post("title"),	
							"url"           => convertToSeo($this->input->post("title")),
							"lang"          => $this->input->post("lang"),		
							"isActive"      => 1,
							"rank"			=> 0,
							"parent_id"		=> $parent_id,
							"createdAt"     => date("Y-m-d H:i:s")
						)					
				);				
				if($insert)
				{
					$alert = array(
						"title"  => "İşlem Başarılı",
						"text"   => "Kayıt başarılı bir şekilde eklendi",
						"type"	 => "success"
					);					
				}else{
					$alert = array(
						"title"   => "İşlem Başarısızdır",
						"text"    => "Kayıt Eklenemedi", 
						"type"	 => "error"
					);										
				}
		

				//İşlemin Sonucunu Sessiona yazma işlemi
				$this->session->set_flashdata("alert", $alert);	
				redirect(base_url("product_categories"));
			}
			else
			{
				$viewData = new stdClass();
				$items = $this->product_category_model->get_all(
					array()
				);				
				
				/* View'e gönderilecek değişkenlerin set edilmesi */	
				$viewData->categories = $items;					
				$viewData->viewFolder = $this->viewFolder;
				$viewData->subViewFolder = "add";
				$viewData->form_error = true;			
				$this->load->view("{$viewData->viewFolder}/{$viewData->subViewFolder}/index", $viewData);				
			}			
		}

		public function update($id)
		{
			if(!(isAllowedUpdateModule())){
				redirect(base_url($this->router->fetch_class()));
			}

			$this->load->library("form_validation");
			//kurallar yazılır
			
			$this->form_validation->set_rules("title", "Başlık", "required|trim");
			$this->form_validation->set_message(
				array(
					"required" => "{field} alanı doldurulmalıdır"
				)
			);
			//Form Validation Çalıştırılır
			$validate = $this->form_validation->run();
			//kontrol edilir
			if($validate)
			{

				$data = array(
							"title"             => $this->input->post("title"),
							"url"               => convertToSeo($this->input->post("title")),
							"lang"          => $this->input->post("lang"),
							"parent_id"         => $this->input->post("parent_id")
					);		
		
				$update = $this->product_category_model->update(array("id" => $id),$data);
				
				if($update)
				{
					$alert = array(
						"title"  => "İşlem Başarılı",
						"text"   => "Kayıt başarılı bir şekilde güncellendi",
						"type"	 => "success"
					);					
				}else{
					$alert = array(
						"title"   => "İşlem Başarısızdır",
						"text"    => "Kayıt Güncellenemedi", 
						"type"	 => "error"
					);										
				}
				//İşlemin Sonucunu Sessiona yazma işlemi
				$this->session->set_flashdata("alert", $alert);	
				redirect(base_url("product_categories"));
			}
			else
			{
				$viewData = new stdClass();
				
				/* View'e gönderilecek değişkenlerin set edilmesi */				
				$viewData->viewFolder = $this->viewFolder;
				$viewData->subViewFolder = "update";
				$viewData->form_error = true;
				
				
				/* Tablodan verilerin getirilmesi */			
				$viewData->item = $this->product_category_model->get(
					array(
						"id" => $id
					)
				);				
				$this->load->view("{$viewData->viewFolder}/{$viewData->subViewFolder}/index", $viewData);				
			}			
		}		
		
		public function update_form($id)
		{
			if(!(isAllowedUpdateModule())){
				redirect(base_url($this->router->fetch_class()));
			}


			$viewData = new stdClass();
			
			/* Tablodan verilerin getirilmesi */			
			$item = $this->product_category_model->get(
				array(
					"id" => $id
				)
			);	

			$items = $this->product_category_model->get_all(
				array(
					"isActive"	=> 1,
					"lang"	    => $this->session->userdata("lang")
				)
			);			
			
			/* View'e gönderilecek değişkenlerin set edilmesi */			
			$viewData->viewFolder = $this->viewFolder;
			$viewData->subViewFolder = "update";
			$viewData->item = $item;
			$viewData->categories = $items;			
			
			$this->load->view("{$viewData->viewFolder}/{$viewData->subViewFolder}/index", $viewData);			
		}

		public function delete($id)
		{

			if(!(isAllowedDeleteModule())){
				redirect(base_url($this->router->fetch_class()));
			}

			$delete = $this->product_category_model->delete(
				array(
					"id" => $id
				)
			);
			
			if($delete)
			{
				$alert = array(
					"title"  => "İşlem Başarılı",
					"text"   => "Kayıt başarılı bir şekilde silindi",
					"type"	 => "success"
				);					
			}else{
				$alert = array(
					"title"  => "İşlem Başarısız",
					"text"   => "Silme İşlemi Gerçekleştirilemedi",
					"type"	 => "error"
				);					
			}
			$this->session->set_flashdata("alert", $alert);
			redirect(base_url("product_categories"));	
		}
				

		public function isActiveSetter($id)
		{
			if(!(isAllowedUpdateModule())){
				die();
			}
						
			if($id)
			{
				$isActive = ($this->input->post("data") === "true") ? 1 : 0;
				
				$this->product_category_model->update(
					array(
						"id" => $id
					),
					array(
						"isActive" => $isActive
					)
				);
			}	
		}
		
		public function buildTree($elements, $parent_id = 0)
		{
			
			$branch = array();
			
			foreach($elements as $element)
			{
				if($element->parent_id == $parent_id)
				{
					$children = $this->buildTree($elements, $element->id);
					
					if($children){
						$element->children = $children;
					}else{
						$element->children = array();
					}
					
					$branch[] = $element; 	
				}	
			}
			
			return $branch;
		}

}		
		

  ?>