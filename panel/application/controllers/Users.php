<?php

class Users extends MY_CONTROLLER
{
		public $viewFolder = "";
		
		public function __construct()
		{
			parent::__construct();
			$this->viewFolder = "users_v";
			$this->load->model("user_model");

			if(!get_active_user()){
				redirect(base_url("login"));
			}			
			
			
		}
		public function index()
		{
			
			if(!(isAllowedViewModule())){
				redirect(base_url());
			}

			$viewData = new stdClass();

			$user = get_active_user();

			/*
			if(isAdmin()){

				$where = array();
			}else{

				$where = array(
					"id"	=> $user->id
				);
			}	
			*/

			/* Tablodan verilerin getirilmesi */
			$items = $this->user_model->get_all(
				
			);


			
			/* View'e gönderilecek değişkenlerin set edilmesi */
			$viewData->items = $items;
			$viewData->viewFolder = $this->viewFolder;
			$viewData->subViewFolder = "list";			
			
			$this->load->view("{$viewData->viewFolder}/{$viewData->subViewFolder}/index", $viewData);
				
		}
		public function new_form()
		{		
			if(!(isAllowedWriteModule())){
				redirect(base_url($this->router->fetch_class()));
			}			
			
			$viewData = new stdClass();
			
			$this->load->model("user_role_model");

			$viewData->user_roles = $this->user_role_model->get_all(
				array(
					"isActive"	=> 1
				)
			);

			/* View'e gönderilecek değişkenlerin set edilmesi */			
			$viewData->viewFolder = $this->viewFolder;
			$viewData->subViewFolder = "add";
			
			$this->load->view("{$viewData->viewFolder}/{$viewData->subViewFolder}/index", $viewData);	
		}
		public function save()
		{
			if(!(isAllowedWriteModule())){
				redirect(base_url($this->router->fetch_class()));
			}

			$this->load->library("form_validation");
			//kurallar yazılır		
							
			
			$this->form_validation->set_rules("user_name", "Kullanıcı Adı", "required|trim|is_unique[users.user_name]");
			$this->form_validation->set_rules("full_name", "Ad Soyad", "required|trim");
			$this->form_validation->set_rules("email", "E-posta", "required|trim|valid_email|is_unique[users.email]");
			$this->form_validation->set_rules("user_role_id", "Kullanıcı Yetkisi", "required|trim");
			$this->form_validation->set_rules("password", "Şifre", "required|trim|min_length[6]|max_length[16]");
			$this->form_validation->set_rules("re_password", "Şifre Tekrar", "required|trim|min_length[6]|max_length[16]|matches[password]");
			$this->form_validation->set_message(
				array(
					"required"      => "{field} alanı doldurulmalıdır",
					"valid_email" 	=> "Lütfen geçerli bir e-posta adresi giriniz",
					"is_unique"		=> "<b>{field}</b> alanı daha önce kullanılmış",
					"matches"		=> "Şifreler birbiriyle aynı olmalı",
					"min_length"	=> "Şifre minumun 6 karakterden oluşmalıdır",
					"max_length"	=> "Şifre 16 karakterden daha uzun olamaz"	
				)
			);
			//Form Validation Çalıştırılır
			$validate = $this->form_validation->run();
			//kontrol edilir
			if($validate)
			{
					
				$insert = $this->user_model->add(
					array(
							"user_name"     => $this->input->post("user_name"),
							"full_name"   	=> $this->input->post("full_name"),
							"email"         => $this->input->post("email"),	
							"user_role_id"  => $this->input->post("user_role_id"),						
							"password"      => md5($this->input->post("password")),							
							"isActive"      => 1,
							"createdAt"     => date("Y-m-d H:i:s")
						)					
				);				
				if($insert)
				{
					$alert = array(
						"title"  => "İşlem Başarılı",
						"text"   => "Kayıt başarılı bir şekilde eklendi",
						"type"	 => "success"
					);					
				}else{
					$alert = array(
						"title"   => "İşlem Başarısızdır",
						"text"    => "Kayıt Eklenemedi", 
						"type"	 => "error"
					);										
				}
					

				//İşlemin Sonucunu Sessiona yazma işlemi
				$this->session->set_flashdata("alert", $alert);	
				redirect(base_url("users"));
				die();
			}
			else
			{
				$viewData = new stdClass();
				
				/* View'e gönderilecek değişkenlerin set edilmesi */				
				$viewData->viewFolder = $this->viewFolder;
				$viewData->subViewFolder = "add";
				$viewData->form_error = true;			
				$this->load->view("{$viewData->viewFolder}/{$viewData->subViewFolder}/index", $viewData);				
			}			
		}

		public function update($id)
		{
			if(!(isAllowedUpdateModule())){
				redirect(base_url($this->router->fetch_class()));
			}

			$this->load->library("form_validation");
			//kurallar yazılır		
							
			$oldUser = $this->user_model->get(
				array(
					"id"	=> $id
				)
			);
			
			if($oldUser->user_name != $this->input->post("user_name"))
			{
				$this->form_validation->set_rules("user_name", "Kullanıcı Adı", "required|trim|is_unique[users.user_name]");
			}
			if($oldUser->email != $this->input->post("email"))
			{
				$this->form_validation->set_rules("email", "E-mail", "required|trim|is_unique[users.email]");
			}
			
			$this->form_validation->set_rules("full_name", "Ad Soyad", "required|trim");
			$this->form_validation->set_rules("user_role_id", "Kullanıcı Yetkisi", "required|trim");
			
			
			$this->form_validation->set_message(
				array(
					"required"      => "{field} alanı doldurulmalıdır",
					"valid_email" 	=> "Lütfen geçerli bir e-posta adresi giriniz",
					"is_unique"		=> "<b>{field}</b> alanı daha önce kullanılmış"
				)
			);
			//Form Validation Çalıştırılır
			$validate = $this->form_validation->run();
			//kontrol edilir
			if($validate)
			{
				
	
				$update = $this->user_model->update(
					array("id" => $id),
					array(
							"user_name"     => $this->input->post("user_name"),
							"full_name"   	=> $this->input->post("full_name"),
							"email"         => $this->input->post("email"),
							"user_role_id"  => $this->input->post("user_role_id")
					)				
				);
				
				if($update)
				{
					$alert = array(
						"title"  => "İşlem Başarılı",
						"text"   => "Kayıt başarılı bir şekilde güncellendi",
						"type"	 => "success"
					);					
				}else{
					$alert = array(
						"title"   => "İşlem Başarısızdır",
						"text"    => "Kayıt Güncellenemedi", 
						"type"	 => "error"
					);										
				}
				//İşlemin Sonucunu Sessiona yazma işlemi
				$this->session->set_flashdata("alert", $alert);	
				redirect(base_url("users"));
			}
			else
			{
				$viewData = new stdClass();
				
				/* View'e gönderilecek değişkenlerin set edilmesi */				
				$viewData->viewFolder = $this->viewFolder;
				$viewData->subViewFolder = "update";
				$viewData->form_error = true;
				
				
				/* Tablodan verilerin getirilmesi */			
				$viewData->item = $this->user_model->get(
					array(
						"id" => $id
					)
				);	
				$this->load->model("user_role_model");

				$viewData->user_roles = $this->user_role_model->get_all(
					array(
						"isActive"	=> 1
					)
				);							
				$this->load->view("{$viewData->viewFolder}/{$viewData->subViewFolder}/index", $viewData);				
			}			
		}		
		
		public function update_form($id)
		{
			if(!(isAllowedUpdateModule())){
				redirect(base_url($this->router->fetch_class()));
			}

			$viewData = new stdClass();
			
			/* Tablodan verilerin getirilmesi */			
			$item = $this->user_model->get(
				array(
					"id" => $id
				)
			);	

			$this->load->model("user_role_model");

			$viewData->user_roles = $this->user_role_model->get_all(
				array(
					"isActive"	=> 1
				)
			);					
			
			/* View'e gönderilecek değişkenlerin set edilmesi */			
			$viewData->viewFolder = $this->viewFolder;
			$viewData->subViewFolder = "update";
			$viewData->item = $item;		
			
			$this->load->view("{$viewData->viewFolder}/{$viewData->subViewFolder}/index", $viewData);			
		}

		public function delete($id)
		{
			if(!(isAllowedDeleteModule())){
				redirect(base_url($this->router->fetch_class()));
			}

			$delete = $this->user_model->delete(
				array(
					"id" => $id
				)
			);
			
			if($delete)
			{
				$alert = array(
					"title"  => "İşlem Başarılı",
					"text"   => "Kayıt başarılı bir şekilde silindi",
					"type"	 => "success"
				);					
			}else{
				$alert = array(
					"title"  => "İşlem Başarısız",
					"text"   => "Silme İşlemi Gerçekleştirilemedi",
					"type"	 => "error"
				);					
			}
			$this->session->set_flashdata("alert", $alert);
			redirect(base_url("users"));	
		}
				

		public function isActiveSetter($id)
		{
			if(!(isAllowedUpdateModule())){
				die();
			}

			if($id)
			{
				$isActive = ($this->input->post("data") === "true") ? 1 : 0;
				
				$this->user_model->update(
					array(
						"id" => $id
					),
					array(
						"isActive" => $isActive
					)
				);
			}	
		}

		
		
		public function update_password_form($id)
		{
			if(!(isAllowedUpdateModule())){
				redirect(base_url($this->router->fetch_class()));
			}

			$viewData = new stdClass();
			
			/* Tablodan verilerin getirilmesi */			
			$item = $this->user_model->get(
				array(
					"id" => $id
				)
			);			
			
			/* View'e gönderilecek değişkenlerin set edilmesi */			
			$viewData->viewFolder = $this->viewFolder;
			$viewData->subViewFolder = "password";
			$viewData->item = $item;		
			
			$this->load->view("{$viewData->viewFolder}/{$viewData->subViewFolder}/index", $viewData);			
		}



		public function update_password($id)
		{
			if(!(isAllowedUpdateModule())){
				redirect(base_url($this->router->fetch_class()));
			}
						
			$this->load->library("form_validation");
			//kurallar yazılır		
							
			$this->form_validation->set_rules("password", "Şifre", "required|trim|min_length[6]|max_length[16]");
			$this->form_validation->set_rules("re_password", "Şifre Tekrar", "required|trim|min_length[6]|max_length[16]|matches[password]");
			
			
			$this->form_validation->set_message(
				array(					
					"matches"		=> "Şifreler birbiriyle aynı olmalı",
					"min_length"	=> "Şifre minumun 6 karakterden oluşmalıdır",
					"max_length"	=> "Şifre 16 karakterden daha uzun olamaz"	
				)
			);
			//Form Validation Çalıştırılır
			$validate = $this->form_validation->run();
			
			if($validate)
			{
				
	
				$update = $this->user_model->update(
					array("id" => $id),
					array(
							"password"     => md5($this->input->post("password"))
						)				
				);
				
				if($update)
				{
					$alert = array(
						"title"  => "İşlem Başarılı",
						"text"   => "Şifreniz başarılı bir şekilde güncellendi",
						"type"	 => "success"
					);					
				}else{
					$alert = array(
						"title"   => "İşlem Başarısızdır",
						"text"    => "Şifre Güncellenemedi", 
						"type"	 => "error"
					);										
				}
				//İşlemin Sonucunu Sessiona yazma işlemi
				$this->session->set_flashdata("alert", $alert);	
				redirect(base_url("users"));
			}
			else
			{
				$viewData = new stdClass();
				
				/* View'e gönderilecek değişkenlerin set edilmesi */				
				$viewData->viewFolder = $this->viewFolder;
				$viewData->subViewFolder = "password";
				$viewData->form_error = true;
				
				
				/* Tablodan verilerin getirilmesi */			
				$viewData->item = $this->user_model->get(
					array(
						"id" => $id
					)
				);				
				$this->load->view("{$viewData->viewFolder}/{$viewData->subViewFolder}/index", $viewData);				
			}			
		}

		public function login()
		{
			$viewData = new stdClass();
			

			
			/* View'e gönderilecek değişkenlerin set edilmesi */
			
			$viewData->viewFolder = $this->viewFolder;
			$viewData->subViewFolder = "login";			
			
			$this->load->view("{$viewData->viewFolder}/{$viewData->subViewFolder}/index", $viewData);			
		}	
}

  ?>