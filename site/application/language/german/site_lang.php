<?php
	
	$lang["Haberler"]   = "Nachrichten";
	$lang["Anasayfa"]   = "Homepage";
	$lang["Hakkımızda"]   = "Über uns";
	$lang["Referanslar"]   = "Referenzen";
	$lang["Portfolyo"]   = "Portfolio";	
	$lang["Hizmetlerimiz"]   = "Dienstleistungen";
	$lang["Galeriler"]   = "Galerien";
	$lang["Resim Galerisi"]   = "Fotogalerie";
	$lang["Video Galerisi"]   = "Videogalerie";
	$lang["Dosya Galerisi"]   = "Dateigalerie";	
	$lang["Ürünlerimiz"]   = "Produkte";
	$lang["Eğitimlerimiz"]   = "Kurse";
	$lang["Markalar"]   = "Marken";
	$lang["İletişim"]   = "Kontakt";
	$lang["Vizyonumuz"]   = "unsere Vision";
	$lang["Misyonumuz"]   = "Unsere Aufgabe";
	$lang["Neden bizi seçmelisiniz"]   = "Warum sollen Sie uns wählen";
	$lang["Haber Listesi"]   = "Nachrichtenliste";
	$lang["Detay"]   = "Detail";
	$lang["Görüntüle"]   = "Detail";
	$lang["Aşağıda örnek çalışmalarımızdan bazılarını bulabilirsiniz."]   = "Nachfolgend finden Sie einige unserer Fallstudien.";
	$lang["Portfolyo Listesi"]   = "Portfolio-Liste";
	$lang["Diğer Portfolyolar"]   = "Andere Portfolios";
	$lang["URL"]   = "URL";
	$lang["Yer"]   = "Ort";
	$lang["Kategori"]   = "Kategorie";
	$lang["Tarih"]   = "Datum";
	$lang["Müşteri"]   = "Kunde";
	$lang["Portfolyo Detayları"]   = "Portfolio-Details";
	$lang["Görüntülenme"]   = "Aufrufen";
	$lang["Son Haberler"]   = "Letzte Nachrichten";
	$lang["Daha Fazla"]   = "Mehr";












?>