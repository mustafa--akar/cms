     <?php $settings = get_settings(); ?>
      <!-- section start -->
      <!-- ================ -->
      <section class="pv-40">
        <div class="container">
          <div class="row">

            <!-- main start -->
            <!-- ================ -->
            <div class="main col-12">
              <h3 class="title"><strong class="text-default"><?php echo $settings->company_name; ?></strong></h3>
              <div class="separator-2"></div>
              <div class="row">
                <div class="col-lg-7">
                   <?php echo $settings->homepage_text; ?> 
                </div>
                <div class="col-lg-5">
                  <div class="owl-carousel content-slider-with-controls">
                    <?php foreach($homepage_images as $image){ ?>
                      <div class="overlay-container overlay-visible">
                        <img src="<?php echo get_picture("galleries_v/images/$homepage_gallery->folder_name", $image->url, "851x606"); ?>" alt="<?php echo $homepage_gallery->title; ?>">
                        <div class="overlay-bottom hidden-sm-down">
                          <div class="text">
                            <h3 class="title"><?php echo $homepage_gallery->title; ?></h3>
                          </div>
                        </div>
                        <a href="<?php echo get_picture("galleries_v/images/$homepage_gallery->folder_name", $image->url, "851x606"); ?>" class="owl-carousel--popup-img overlay-link" title="<?php echo $homepage_gallery->title; ?>"><i class="icon-plus-1"></i></a>
                      </div>
                    <?php } ?>


                  </div>
                </div>
              </div>
            </div>
            <!-- main end -->

          </div>
        </div>
      </section>
      <!-- section end -->