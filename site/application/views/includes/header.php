<?php
     $settings = get_settings();
     $lang     = $this->session->userdata("lang");
?>

<div class="header-container">
    <!-- header-top start -->
    <!-- classes:  -->
    <!-- "dark": dark version of header top e.g. class="header-top dark" -->
    <!-- "colored": colored version of header top e.g. class="header-top colored" -->
    <!-- ================ -->
    <div class="header-top colored">
        <div class="container">
            <div class="row">
                <div class="col-2 col-md-5">
                    <!-- header-top-first start -->
                    <!-- ================ -->
                    <div class="header-top-first clearfix">
                        <ul class="social-links circle small clearfix hidden-sm-down">
                            <?php if(!empty($settings->twitter)){ ?>
                                 <li class="twitter"><a target="_blank" href="<?php echo $settings->twitter; ?>"><i class="fa fa-twitter"></i></a></li>
                            <?php } ?>
                            <?php if(!empty($settings->linkedin)){ ?>
                                 <li class="linkedin"><a target="_blank" href="<?php echo $settings->linkedin; ?>"><i class="fa  fa-linkedin"></i></a></li>
                            <?php } ?>
                            <?php if(!empty($settings->facebook)){ ?>                           
                            <li class="facebook"><a target="_blank" href="<?php echo $settings->facebook; ?>"><i class="fa fa-facebook"></i></a></li>
                            <?php } ?>
                            <?php if(!empty($settings->instagram)){ ?>
                                <li class="instagram"><a target="_blank" href="<?php echo $settings->instagram; ?>"><i class="fa fa-instagram"></i></a></li> 
                            <?php } ?>                       
                        </ul>
                        <div class="social-links hidden-md-up circle small">
                            <div class="btn-group dropdown">
                                <button id="header-top-drop-1" type="button" class="btn dropdown-toggle dropdown-toggle--no-caret" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-share-alt"></i></button>
                                <ul class="dropdown-menu dropdown-animation" aria-labelledby="header-top-drop-1">
                                    <?php if(!empty($settings->twitter)){ ?>
                                         <li class="twitter"><a target="_blank" href="<?php echo $settings->twitter; ?>"><i class="fa fa-twitter"></i></a></li>
                                    <?php } ?>
                                    <?php if(!empty($settings->linkedin)){ ?>
                                         <li class="linkedin"><a target="_blank" href="<?php echo $settings->linkedin; ?>"><i class="fa  fa-linkedin"></i></a></li>
                                    <?php } ?>
                                    <?php if(!empty($settings->facebook)){ ?>                           
                                    <li class="facebook"><a target="_blank" href="<?php echo $settings->facebook; ?>"><i class="fa fa-facebook"></i></a></li>
                                    <?php } ?>
                                    <?php if(!empty($settings->instagram)){ ?>
                                        <li class="instagram"><a target="_blank" href="<?php echo $settings->instagram; ?>"><i class="fa fa-instagram"></i></a></li> 
                                    <?php } ?> 
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!-- header-top-first end -->
                </div>
                <div class="col-10 col-md-7">

                    <!-- header-top-second start -->
                    <!-- ================ -->
                    <div id="header-top-second"  class="clearfix text-right">
                        <ul class="list-inline">
                            <li class="list-inline-item"><i class="fa fa-phone pr-1 pl-10"></i><?php echo $settings->phone_1; ?></li>
                            <li class="list-inline-item"><i class="fa fa-envelope-o pr-1 pl-10"></i><?php echo $settings->email; ?></li>
                        </ul>
                    </div>
                    <!-- header-top-second end -->

                </div>
            </div>
        </div>
    </div>

    <header class="header fixed fixed-desktop clearfix">
        <div class="container">
            <div class="row">
                <div class="col-md-auto hidden-md-down pl-3">
                    <!-- header-first start -->
                    <!-- ================ -->
                    <div class="header-first clearfix">

                        <!-- logo -->
                        <div id="logo" class="logo">
                            <a href="<?php echo base_url($lang); ?>"><img id="logo_img" src="<?php echo base_url("assets/images");?>/logo_blue.png" alt="The Project"></a>
                        </div>

                        <!-- name-and-slogan -->
                        <div class="site-slogan">

                            <?php  echo $settings->short_motto; ?>
                        </div>

                    </div>
                    <!-- header-first end -->

                </div>
                <div class="col-lg-8 ml-lg-auto">

                 
                    <div class="header-second clearfix">

             
                        <div class="main-navigation main-navigation--mega-menu  animated">
                            <nav class="navbar navbar-expand-lg navbar-light p-0">
                                <div class="navbar-brand clearfix hidden-lg-up">

                                    <!-- logo -->
                                    <div id="logo-mobile" class="logo">
                                        <a href="<?php echo base_url($lang); ?>"><img id="logo-img-mobile" src="<?php echo base_url("assets/images");?>/logo_blue.png" alt="<?php echo $settings->company_name; ?>"></a>
                                    </div>

                                    <!-- name-and-slogan -->
                                    <div class="site-slogan">
                                        <?php echo $settings->short_motto; ?>
                                    </div>

                                </div>

                                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-collapse-1" aria-controls="navbar-collapse-1" aria-expanded="false" aria-label="Toggle navigation">
                                    <span class="navbar-toggler-icon"></span>
                                </button>

                                <div class="collapse navbar-collapse" id="navbar-collapse-1">
                                    <!-- main-menu -->

                                    <ul class="navbar-nav ml-xl-auto">

                                        <li class="nav-item ">
                                            <a href="<?php echo base_url($lang); ?>" class="nav-link"><?php echo text("Anasayfa"); ?></a>
                                        </li>

                                        <li class="nav-item dropdown ">
                                            <a href="#" class="nav-link dropdown-toggle" id="third-dropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><?php echo text("Hakkımızda"); ?></a>
                                            <ul class="dropdown-menu" aria-labelledby="third-dropdown">
                                                <li ><a href="<?php echo base_url(get_url('hakkimizda')); ?>"><?php echo text("Hakkımızda"); ?></a></li>
                                                <li ><a href="<?php echo base_url(get_url('haberler')); ?>"><?php echo text("Haberler"); ?></a></li>
                                                <li ><a href="<?php echo base_url(get_url('portfolyo-listesi')); ?>"><?php echo text("Portfolyo"); ?></a></li>
                                                <li ><a href="<?php echo base_url(get_url("referanslar")); ?>"><?php echo text("Referanslar"); ?></a></li>
                                                <li ><a href="<?php echo base_url(get_url("hizmetlerimiz")); ?>"><?php echo text("Hizmetlerimiz"); ?></a></li>
                                            </ul>
                                        </li>

                                        <li class="nav-item dropdown ">
                                            <a href="#" class="nav-link dropdown-toggle" id="third-dropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><?php echo text("Galeriler"); ?></a>
                                            <ul class="dropdown-menu" aria-labelledby="third-dropdown">
                                                <li ><a href="<?php echo base_url(get_url("fotograf-galerisi")); ?>"><?php echo text("Resim Galerisi"); ?> </a></li>
                                                <li ><a href="<?php echo base_url(get_url("video-galerisi")); ?>"><?php echo text("Video Galerisi"); ?></a></li>
                                                <li ><a href="<?php echo base_url(get_url("dosya-galerisi")); ?>"><?php echo text("Dosya Galerisi"); ?></a></li>
                                            </ul>
                                        </li>


                                        <li class="nav-item ">
                                            <a href="<?php echo base_url(get_url("urun-listesi")); ?>" class="nav-link"><?php echo text("Ürünlerimiz"); ?></a>
                                        </li>

                                        <li class="nav-item ">
                                            <a href="<?php echo base_url(get_url("egitim-listesi")); ?>" class="nav-link"><?php echo text("Eğitimlerimiz"); ?></a>
                                        </li>

                                        <li class="nav-item ">
                                            <a href="<?php echo base_url(get_url("markalar")); ?>" class="nav-link"><?php echo text("Markalar"); ?></a>
                                        </li>

                                    </ul>
                                    <!-- main-menu end -->
                                </div>
                            </nav>
                        </div>
                        <!-- main-navigation end -->
                    </div>
                    <!-- header-second end -->

                </div>
                <div class="col-auto hidden-md-down pl-0 pl-md-1">
                    <!-- header dropdown buttons -->
                    <div class="header-dropdown-buttons">
                        <a href="<?php echo base_url(get_url("iletisim")); ?>" class="btn btn-sm btn-default"><?php echo text("İletişim"); ?> <i class="fa fa-envelope-o pl-1"></i></a>
                    </div>
                    <!-- header dropdown buttons end-->
                </div>
            </div>
        </div>
    </header>
    <!-- header end -->
</div>

