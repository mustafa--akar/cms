    <?php $settings = get_settings(); ?>
	
	<meta charset="utf-8">
    <title><?php echo $settings->company_name." | ".$settings->short_motto; ?></title>
    <meta name="description" content="The Project a Bootstrap-based, Responsive HTML5 Template">
    <meta name="author" content="Mustafa Akar">

    <!-- Mobile Meta -->
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	
    <?php if(isset($opengraph)){ ?>
        <meta property="og:title" content="<?php echo $news->title; ?>"/>
        <meta property="og:description" content="<?php echo character_limiter(strip_tags($news->description),200); ?>"/>
        <?php if($news->news_type == "image"){ ?>
            <meta property="og:image" content="<?php echo base_url("panel/uploads/news_v/$news->img_url"); ?>"/>
        <?php }elseif ($news->news_type= "video") {  ?>
             <meta property="og:video" content="<?php echo $news->video_url; ?>"/>
        <?php } ?>    


    <?php } ?>    

	<?php $this->load->view("includes/include_style"); ?>