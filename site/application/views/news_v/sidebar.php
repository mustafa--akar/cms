            <!-- sidebar start -->
            <!-- ================ -->
            <aside class="col-lg-4 col-xl-3 ml-xl-auto">
              <div class="sidebar">
                <div class="block clearfix">
         
                  <nav>
                    <ul class="nav flex-column">
                      <li class="nav-item"><a class="nav-link" href="<?php echo base_url($this->session->userdata("lang")); ?>"><?php echo text("Anasayfa"); ?></a></li>
                      <li class="nav-item"><a class="nav-link" href="<?php echo base_url(get_url("urun-listesi")); ?>"><?php echo text("Ürünlerimiz"); ?></a></li>
                      <li class="nav-item"><a class="nav-link" href="<?php echo base_url(get_url("portfolyo-listesi")); ?>"><?php echo text("Portfolyo"); ?></a></li>
                      <li class="nav-item"><a class="nav-link" href="<?php echo base_url(get_url("hakkimizda")); ?>"><?php echo text("Hakkımızda"); ?></a></li>
                      <li class="nav-item"><a class="nav-link" href="<?php echo base_url(get_url("iletisim")); ?>"><?php echo text("İletişim"); ?></a></li>
                    </ul>
                  </nav>
                </div>

                <div class="block clearfix">
                  <h3 class="title"><?php echo text("Son Haberler"); ?> </h3>
                  <div class="separator-2"></div>

                  <?php foreach ($recent_news_list as $recent_news) {  ?>
                                  
                    <div class="media margin-clear">
                      <div class="d-flex pr-2">

                        <?php if($recent_news->news_type == "image"){ ?>  
                          <div class="overlay-container">
                            <img class="media-object" src="<?php echo get_picture("news_v", $recent_news->img_url, "513x289"); ?>" alt="<?php echo $recent_news->url; ?>">
                            <a href="<?php echo base_url(get_url('haber').'/'.$recent_news->url); ?>" class="overlay-link small"><i class="fa fa-link"></i></a>
                          </div>
                         <?php }elseif ($recent_news->news_type == "video") { ?>
                          <div>
                            <iframe style="width: 60px; height: 35px;" src="<?php echo $recent_news->video_url; ?>"></iframe>
                          </div>                            
                         <?php } ?> 

                      </div>
                      <div class="media-body">
                        <h6 class="media-heading"><a href="<?php echo base_url(get_url('haber').'/'.$recent_news->url); ?>"><?php echo $recent_news->title; ?></a></h6>
                        <p class="small margin-clear"><i class="fa fa-calendar pr-10"></i><?php echo get_readable_date($recent_news->createdAt); ?></p>
                      </div>
                    </div>
                    <hr>
                <?php } ?>
                  <div class="text-right space-top">
                    <a href="<?php echo base_url(get_url("haberler")); ?>" class="link-dark"><i class="fa fa-plus-circle pl-1 pr-1"></i><?php echo text("Daha Fazla"); ?> </a> 
                  </div>
                </div>

                </div>                
              </div>
            </aside>
            <!-- sidebar end -->