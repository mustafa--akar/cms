
  <section class="main-container">
    <div class="container">
      <div class="row">    
        <div class="main col-md-12">
          <h1 class="page-title"><?php echo $gallery->title; ?></h1>
          <div class="separator-2"></div>    
          <br> 			  
             
          <div class="row grid-space-20">
            <?php if(!empty($videos)){ ?>
              <?php foreach($videos as $video){ ?>
                <div class="col-md-4 mb-20">
                  <div class="embed-responsive embed-responsive-16by9">
                    <iframe class="embed-responsive-item" src="<?php echo $video->url; ?>"></iframe>
                  </div>                 
                </div>
              <?php } ?>
            <?php }else{ ?>
              <div class="alert alert-warning col-md-12">
                Burada malesef bir veri bulunamadı !
              </div>              
            <?php } ?>
            <div class="col-md-12">
              <a class="btn btn-default" href="<?php echo base_url(get_url('video-galerisi')); ?>">
                <i class="fa fa-arrow-left"></i> Geri Dön  
              </a>              
            </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>        
            
			