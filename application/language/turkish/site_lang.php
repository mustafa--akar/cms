<?php
	
	$lang["Anasayfa"]   = "Anasayfa";
	$lang["Hakkımızda"]   = "Hakkımızda";
	$lang["Haberler"]   = "Haberler";
	$lang["Referanslar"]   = "Referanslar";
	$lang["Portfolyo"]   = "Portfolyo";
	$lang["Portfolyolar"]   = "Portfolyolar";
	$lang["Hizmetlerimiz"]   = "Hizmetlerimiz";
	$lang["Galeriler"]   = "Galeriler";
	$lang["Resim Galerisi"]   = "Fotoğraf Galerisi";
	$lang["Video Galerisi"]   = "Video Galerisi";
	$lang["Dosya Galerisi"]   = "Dosya Galerisi";
	$lang["Ürünlerimiz"]   = "Ürünlerimiz";
	$lang["Eğitimlerimiz"]   = "Eğitimlerimiz";
	$lang["Markalar"]   = "Markalar";
	$lang["İletişim"]   = "İletişim";
	$lang["Vizyonumuz"]   = "Vizyonumuz";
	$lang["Misyonumuz"]   = "Misyonumuz";
	$lang["Neden bizi seçmelisiniz"]   = "Neden bizi seçmelisiniz";
	$lang["Haber Listesi"]   = "Haber Listesi";
	$lang["Neden bizi seçmelisiniz"]   = "Neden bizi seçmelisiniz";
	$lang["Detay"]   = "Detay";
	$lang["Görüntüle"]   = "Görüntüle";
	$lang["Aşağıda örnek çalışmalarımızdan bazılarını bulabilirsiniz."]   = "Aşağıda örnek çalışmalarımızdan bazılarını bulabilirsiniz.";
	$lang["Portfolyo Listesi"]   = "Portfolyo Listesi";
	$lang["URL"]   = "URL";
	$lang["Yer"]   = "Yer";
	$lang["Kategori"]   = "Kategori";
	$lang["Tarih"]   = "Tarih";
	$lang["Müşteri"]   = "Müşteri";
	$lang["Görüntülenme"]   = "Görüntülenme";
	$lang["Portfolyo Detayları"]   = "Portfolyo Detayları";
	$lang["Son Haberler"]   = "Son Haberler";
	$lang["Daha Fazla"]   = "Daha Fazla";
	$lang["Lütfen formu eksiksiz doldurun !"]   = "Lütfen formu eksiksiz doldurun !";



?>