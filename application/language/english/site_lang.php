<?php
	
	$lang["Hakkımızda"]   = "About us";
	$lang["Anasayfa"]   = "Home Page";
	$lang["Haberler"]   = "News";
	$lang["Referanslar"]   = "References";
	$lang["Portfolyo"]   = "Portfolio";	
	$lang["Portfolyolar"]   = "Portfolios";
	$lang["Hizmetlerimiz"]   = "Services";
	$lang["Galeriler"]   = "Galleries";
	$lang["Resim Galerisi"]   = "Photo gallery";
	$lang["Video Galerisi"]   = "Video gallery";
	$lang["Dosya Galerisi"]   = "File gallery";	
	$lang["Ürünlerimiz"]   = "Products";
	$lang["Eğitimlerimiz"]   = "Courses";
	$lang["Markalar"]   = "Brands";
	$lang["İletişim"]   = "Contact";
	$lang["Vizyonumuz"]   = "Our Vision";
	$lang["Misyonumuz"]   = "Our Mission";
	$lang["Neden bizi seçmelisiniz"]   = "Why choose us";
	$lang["Haber Listesi"]   = "News List";
	$lang["Detay"]   = "Detail";
	$lang["Görüntüle"]   = "Detail";
	$lang["Aşağıda örnek çalışmalarımızdan bazılarını bulabilirsiniz."]   = "You can find some of our case studies below.";
	$lang["Portfolyo Listesi"]   = "Portfolio List";
	$lang["Diğer Portfolyolar"]   = "Other Portfolios";
	$lang["URL"]   = "URL";
	$lang["Yer"]   = "Place";
	$lang["Kategori"]   = "Category";
	$lang["Tarih"]   = "Date";
	$lang["Müşteri"]   = "Customer";
	$lang["Portfolyo Detayları"]   = "Portfolio Details";
	$lang["Görüntülenme"]   = "Shows";
	$lang["Son Haberler"]   = "Last News";
	$lang["Daha Fazla"]   = "More";
	$lang["Lütfen formu eksiksiz doldurun !"]   = "Please fill out the form completely !";












?>