<?php


function get_category_title($category_id = 0, $category_type = "product")
{
	$t = &get_instance();
	
	if($category_type == "product"){
			
		$category = $t->load->product_category_model->get(
			array(
				"id"	=> $category_id
			)
		);	
	
	}else if($category_type == "portfolio"){
		
		$category = $t->load->portfolio_category_model->get(
			array(
				"id"	=> $category_id
			)
		);	
		
	}

	
	if($category){
		
		return $category->title;
		
	}else{
		
		return "<b style='color:red'> Tanımlı değil</b>";
		
	}
	
}

function get_readable_date($date)
{

    $dateFormat = strftime('%d %B %Y', strtotime($date));
	
	$aylarIng = array(
	   "January", "February", "March", "April", "May", "June", 
	   "July", "August", "September", "October", "November", "December");
	   
	$aylar = array(
	   "Ocak", "Şubat", "Mart", "Nisan", "Mayıs", "Haziran", 
	   "Temmuz", "Ağustos", "Eylül", "Ekim", "Kasım", "Aralık"
	   );	   
	   
	return str_replace($aylarIng, $aylar, $dateFormat);
	
}

function get_product_cover_image($product_id)
{
	$t = &get_instance();
	
	$t->load->model("product_image_model");
	
	$cover_image = $t->product_image_model->get(
		array(
			"isCover"	  => 1,
			"product_id"  => $product_id
		)
	);
	
	if(empty($cover_image))
	{
		$cover_image = $t->product_image_model->get(
			array(				
				"product_id"  => $product_id
			)
		);		
	}

	return !empty($cover_image) ? $cover_image->img_url : "" ;	
	
}

function get_portfolio_cover_image($portfolio_id)
{
	$t = &get_instance();
	
	$t->load->model("portfolio_image_model");
	
	$cover_image = $t->portfolio_image_model->get(
		array(
			"isCover"	  => 1,
			"portfolio_id"  => $portfolio_id
		)
	);
	
	if(empty($cover_image))
	{
		$cover_image = $t->portfolio_image_model->get(
			array(				
				"portfolio_id"  => $portfolio_id
			)
		);		
	}

	return !empty($cover_image) ? $cover_image->img_url : "" ;	
	
}


function buildTree($elements, $parent_id = 0)
{
	
	$branch = array();
	
	foreach($elements as $element)
	{
		if($element->parent_id == $parent_id)
		{
			$children = buildTree($elements, $element->id);
			
			if($children){
				$element->children = $children;
			}else{
				$element->children = array();
			}
			
			$branch[] = $element; 	
		}	
	}
	
	return $branch;
}


function drawElements($items)
{
	$t = &get_instance();
	
	echo "<ul class='flex-column'>";

	foreach($items as $item)
	{
		
		$url = base_url(get_url("urun-listesi")."/".$item->url);
		
		echo "<li class='nav-item'><a class='nav-link' href='$url'>$item->title</a></li>";
		
		if(sizeof($item->children) > 0)
		{
			drawElements($item->children);
		}	
	}

	echo "</ul>";	
	
}


function get_portfolio_category_title($id)
{
	
	$t = &get_instance();
	
	$t->load->model("portfolio_category_model");
	
	$portfolio = $t->portfolio_category_model->get(
		array(
			"id"    => $id
		)	
	);
	
	return (empty($portfolio)) ? false : $portfolio->title;
	
}

function get_settings()
{
	
	$t = &get_instance();
	
	//$settings = $t->session->userdata("settings");
	
	//if(empty($settings))
	//{
		$t->load->model("settings_model");
		$settings = $t->settings_model->get(
			array(
				"lang"	=> $t->session->userdata("lang")
			)
		);
		
		$t->session->set_userdata("settings", $settings);		
	//}	
	
	return $settings;
	
}

function send_email($toEmail = "", $subject = "", $message = "")
{
	$t = &get_instance();
	
	$t->load->model("emailsettings_model");
	
	 
	$email_settings = $t->emailsettings_model->get(
		array(
			"isActive"	=> 1
		)
	);
	
	if(empty($toEmail))
		$toEmail = $email_settings->to;
	
	//print_r($email_settings);
	
	$config = array(
		"protocol"	=> $email_settings->protocol,
		"smtp_host"	=> $email_settings->host,
		"smtp_port"	=> $email_settings->port,
		"smtp_user"	=> $email_settings->user,
		"smtp_pass"	=> $email_settings->password,
		"starttls"	=> true,
		"charset"	=> "utf-8",
		"mailtype"	=> "html",
		"newline"	=> "\r\n",
		"wordwrap"	=> true
	);
	$t->load->library("email", $config);
	
	$t->email->from($email_settings->from, $email_settings->user_name);			
	$t->email->to($toEmail);			
	$t->email->subject($subject);			
	$t->email->message($message);
	
	return $t->email->send();	
	
}

function get_picture($path = "", $picture = "", $resolation = "50x50")
{

	if($picture != ""){
		//echo FCPATH."panel/uploads/$path/$resolation/$picture"; die();	
		if(file_exists(FCPATH."panel/uploads/$path/$resolation/$picture")){

			$picture = base_url("panel/uploads/$path/$resolation/$picture");
		}else{

			$picture = base_url("assets/images/no_image.png");
		}

	}else{
		
		$picture = base_url("assets/images/no_image.png");
	}
	return $picture;

}

function get_gallery_cover_image($folderName)
{

	$path = "panel/uploads/galleries_v/images/$folderName/350x216";

	if($handle = opendir($path))
	{
		while(($file = readdir($handle)) !== false){

			if($file != "." && $file != ".."){
				
				return $file;
			}
		}
	}	

}

function get_popup_service($page = "")
{

	$t = &get_instance();
	$t->load->model("popup_model");
	$popup = $t->popup_model->get(
		array(
			"isActive"	=> 1,
			"page"		=>$page
		)
	);

	return (!empty($popup)) ? $popup : false ;
}

function get_gallery_by_url($url = "")
{

	$t = &get_instance();
	$t->load->model("gallery_model");

	$gallery = $t->gallery_model->get(
		array(
			"isActive"     => 1,
			"url"  => $url
		)
	);

	return ($gallery) ? $gallery : false;

}

function get_gallery_file_name($file_url = "")
{
	if($file_url){

		$file_name_with_punkt = str_replace("-", " ", $file_url);
		$file_explode = explode(".", $file_name_with_punkt, -1);
		return $file_explode[0];	
		
	}else{
		return false;
	}
	
}


function text($text)
{
	$t = &get_instance();

	return ($t->lang->line($text)) ? $t->lang->line($text) : $text;
}

function get_url($url){

	$t = &get_instance();
	$lang = $t->session->userdata("lang");

	$multiLanguageUrl = array(
		"hakkimizda" => array(
			"tr"	=> "hakkimizda",
			"de"	=> "ueber-uns",
			"en"	=> "about-us"
		),
		"haberler"	=> array(
			"tr"	=> "haberler",
			"de"	=> "nachrichten",
			"en"	=> "news-list"
		),
		"urun-listesi"	=> array(
			"tr"	=> "urun-listesi",
			"de"	=> "produktliste",
			"en"	=> "product-list"
		),
		"urun-detay"	=> array(
			"tr"	=> "urun-detay",
			"de"	=> "produktdetails",
			"en"	=> "product-details"
		),
		"portfolyo-listesi"	=> array(
			"tr"	=> "portfolyo-listesi",
			"de"	=> "portfolioliste",
			"en"	=> "portfolio-liste"
		),
		"portfolyo-detay"	=> array(
			"tr"	=> "portfolyo-detay",
			"de"	=> "portfoliodetails",
			"en"	=> "portfolio-details"
		),
		"egitim-listesi"	=> array(
			"tr"	=> "egitim-listesi",
			"de"	=> "kursliste",
			"en"	=> "course-list"
		),
		"egitim-detay"	=> array(
			"tr"	=> "egitim-detay",
			"de"	=> "kursdetails",
			"en"	=> "course-details"
		),
		"referanslar"	=> array(
			"tr"	=> "referanslar",
			"de"	=> "referenzen",
			"en"	=> "references"
		),	
		"markalar"	=> array(
			"tr"	=> "markalar",
			"de"	=> "marken",
			"en"	=> "brands"
		),
		"hizmetlerimiz"	=> array(
			"tr"	=> "hizmetlerimiz",
			"de"	=> "dienstleistungen",
			"en"	=> "services"
		),
		"iletisim"	=> array(
			"tr"	=> "iletisim",
			"de"	=> "kontakt",
			"en"	=> "contact"
		),
		"mesaj-gonder"	=> array(
			"tr"	=> "mesaj-gonder",
			"de"	=> "nachricht-senden",
			"en"	=> "message-send"
		),
		"abone-ol"	=> array(
			"tr"	=> "abone-ol",
			"de"	=> "abonieren",
			"en"	=> "subscribe"
		),
		"haber"	=> array(
			"tr"	=> "haber",
			"de"	=> "nachricht",
			"en"	=> "news"
		),
		"bir-daha-gosterme"	=> array(
			"tr"	=> "bir-daha-gosterme",
			"de"	=> "nie-wieder-zeigen",
			"en"	=> "never-show-again"
		),
		"fotograf-galerisi"	=> array(
			"tr"	=> "fotograf-galerisi",
			"de"	=> "fotogalerie",
			"en"	=> "photo-gallery"
		),
		"video-galerisi"	=> array(
			"tr"	=> "video-galerisi",
			"de"	=> "videogalerie",
			"en"	=> "video-gallery"
		),	
		"dosya-galerisi"	=> array(
			"tr"	=> "dosya-galerisi",
			"de"	=> "dateigalerie",
			"en"	=> "file-gallery"
		),																																		
	);

	// 

	if(isset($multiLanguageUrl[$url])){
		return $t->session->userdata("lang")."/".$multiLanguageUrl[$url][$lang];
	}else{
		return $t->session->userdata("lang")."/".$url;
	}
}

?>