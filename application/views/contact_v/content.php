   <?php 
     $settings = get_settings();    
     $alert = $this->session->userdata("alert");       
     $member_alert = $this->session->userdata("member_alert");       

    if($this->session->userdata("form_error")){
      $form_error = $this->session->userdata("form_error");
    }  
   

   ?>

      <div class="banner dark-translucent-bg" style="background-image:url('<?php echo base_url("assets/images"); ?>/about_us_banner.jpg'); background-position: 50% 30%;">
 
        <div class="container">
          <div class="row justify-content-lg-center">
            <div class="col-lg-8 text-center pv-20">
              <h1 class="page-title text-center">Bize Ulaşın</h1>
              <div class="separator"></div>
              <p class="lead text-center">Bize ulaşmak için aşağıdaki kanallardan herhangi birini kullanabilirsiniz.</p>
              <ul class="list-inline mb-20 text-center">
                <li class="list-inline-item"><i class="text-default fa fa-map-marker pr-2"></i><?php echo strip_tags($settings->address); ?></li>
                <li class="list-inline-item"><a href="tel:<?php echo $settings->phone_1; ?>" class="link-dark"><i class="text-default fa fa-phone pl-10 pr-2"></i><?php echo $settings->phone_1; ?></a></li>
                <li class="list-inline-item"><a href="mailto:<?php echo $settings->email; ?>" class="link-dark"><i class="text-default fa fa-envelope-o pl-10 pr-2"></i><?php echo $settings->email; ?></a></li>
              </ul>
              <div class="separator"></div>
              <ul class="social-links circle animated-effect-1 margin-clear text-center space-bottom">
				<?php if($settings->facebook){ ?>
					<li class="facebook"><a target="_blank" href="<?php echo $settings->facebook; ?>"><i class="fa fa-facebook"></i></a></li>
				<?php } ?>
				<?php if($settings->twitter){ ?>
					<li class="twitter"><a target="_blank" href="<?php echo $settings->twitter; ?>"><i class="fa fa-twitter"></i></a></li>
				<?php } ?>
				<?php if($settings->instagram){ ?>
					<li class="instagram"><a target="_blank" href="<?php echo $settings->instagram; ?>"><i class="fa fa-instagram"></i></a></li>
				<?php } ?>	
				<?php if($settings->linkedin){ ?>
					<li class="linkedin"><a target="_blank" href="<?php echo $settings->linkedin; ?>"><i class="fa fa-linkedin"></i></a></li>
				<?php } ?>
              </ul>
            </div>
          </div>
        </div>
      </div>

      <section class="main-container">

        <div class="container">
          <div class="row">   
            <div class="main col-12 space-bottom">
              <h2 class="title">Bize Yazın</h2>
              <div class="row">
                <div class="col-lg-6">
                  <p>Bize ulaşmak için aşağıdaki formu doldurabilirsiniz.</p>
                  <div class="alert alert-success <?php echo ($alert['type'] == 'success') ? "" : "hidden-xs-up" ; ?>" id="MessageSent">
                    Mesajınız başarılı bir şekilde gönderildi.
                  </div>
                  <div class="alert alert-danger <?php echo (isset($form_error)) ? "" : "hidden-xs-up" ; ?>" id="MessageNotSent">
                    <?php echo $error_message; ?>
                  </div>
                  <div class="contact-form">
                    <form class="margin-clear" action="<?php echo base_url(get_url("mesaj-gonder")); ?>" method="post">
                      <div class="form-group has-feedback">
                        <label for="name">Ad*</label>
                        <input type="text" class="form-control" id="name" name="name" value="<?php echo isset($form_error)  ? set_value("name") : "" ;?>" placeholder="">
                        <i class="fa fa-user form-control-feedback"></i>
                        <?php if(isset($form_error)){ ?>
                          <small class="input-form-error"><?php echo form_error("name"); ?></small>
                        <?php } ?>                        
                      </div>
                      <div class="form-group has-feedback">
                        <label for="email">E-posta*</label>
                        <input type="email" class="form-control" id="email" name="email" value="<?php echo isset($form_error)  ? set_value("email") : "" ;?>" placeholder="">
                        <i class="fa fa-envelope form-control-feedback"></i>
                        <?php if(isset($form_error)){ ?>
                          <small class="input-form-error"><?php echo form_error("email"); ?></small>
                        <?php } ?>                        
                      </div>
                      <div class="form-group has-feedback">
                        <label for="subject">Konu*</label>
                        <input type="text" class="form-control" id="subject" name="subject" value="<?php echo isset($form_error)  ? set_value("subject") : "" ;?>" placeholder="">
                        <i class="fa fa-navicon form-control-feedback"></i>
                        <?php if(isset($form_error)){ ?>
                          <small class="input-form-error"><?php echo form_error("subject"); ?></small>
                        <?php } ?>                        
                      </div>
                      <div class="form-group has-feedback">
                        <label for="message">Mesajınız*</label>
                        <textarea class="form-control" rows="6" id="message" name="message">
                          <?php echo isset($form_error)  ? set_value("message") : "" ;?>                          
                        </textarea>
                        <i class="fa fa-pencil form-control-feedback"></i>
                        <?php if(isset($form_error)){ ?>
                          <small class="input-form-error"><?php echo  strip_tags(form_error("message")); ?></small>
                        <?php } ?>                        
                      </div>
          					  <div class="row">
            						<div class="col-md-6">                      
            							<input type="text" class="form-control" name="captcha" placeholder="Doğrulama Kodu">		
                          <?php if(isset($form_error)){ ?>
                            <small class="input-form-error"><?php echo form_error("captcha"); ?></small>
                          <?php } ?>               				
            						</div>
            						<div class="col-md-6">
            							<?php echo $captcha['image']; ?>
            						</div>						
          					  </div>				  

          					  <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">	
                             
          					  <button class="submit-button btn btn-lg btn-default" type="submit">Gönder</button>
                    </form>
                  </div>
                </div>
                <div class="col-lg-6">
                  <div id="map-canvas"></div>
                </div>
              </div>
            </div>
            
          </div>
        </div>
      </section>

    
      <section class="section pv-40 dark-bg text-center">
        <div class="container">
          <div class="row">
            <div class="col-md-12">
              <div class="call-to-action text-center">
                <div class="row justify-content-lg-center">
                  <div class="col-lg-8">
                    <h2 class="title">Güncel Haberler İçin Abone Ol</h2>
                    <p>Kampanyalarımızdan, fırsatlarımızdan, ilk önce siz haberdar olmak istiyorsanız; bize abone olmayı unutmayın...</p>
                  <div class="alert alert-success <?php echo ($member_alert['type'] == 'success') ? "" : "hidden-xs-up" ; ?>">
                      <?php echo $member_alert["message"]; ?>
                  </div>  
                  <div class="alert alert-danger <?php echo ($member_alert['type'] == 'error') ? "" : "hidden-xs-up" ; ?>">
                      <?php echo $member_alert["message"]; ?>
                  </div>                                     
                    <div class="separator"></div>
                    <form class="form-inline margin-clear justify-content-center" action="<?php echo base_url(get_url("abone-ol")); ?>" method="post">
                      <div class="form-group has-feedback">
                        <label class="sr-only" for="subscribe2">E-posta adresiniz</label>
                        <input type="email" class="form-control form-control-lg" id="subscribe2" placeholder="E-posta adresiniz" name="subscribe_email" required="">
						
					             	<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">	
						
                        <i class="fa fa-envelope form-control-feedback"></i>
                      </div>
                      <button type="submit" class="btn btn-lg btn-gray-transparent btn-animated margin-clear ml-3">Abone Ol <i class="fa fa-send"></i></button>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>


   