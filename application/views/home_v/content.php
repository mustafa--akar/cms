

      <!-- banner start -->
      <!-- ================ -->
      <div class="banner clearfix">

      <?php $this->load->view("{$viewFolder}/slider"); ?>

      </div>
      <!-- banner end -->

      <div id="page-start"></div>

      <?php $this->load->view("{$viewFolder}/about_us"); ?>

      <?php $this->load->view("{$viewFolder}/services"); ?>

      <?php $this->load->view("{$viewFolder}/testimonials"); ?>

      <?php $this->load->view("{$viewFolder}/portfolio"); ?>       

      <?php $this->load->view("{$viewFolder}/references"); ?>

