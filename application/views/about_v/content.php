      <!-- ================ -->
      <div class="banner dark-translucent-bg" style="background-image:url('<?php echo base_url("assets/images"); ?>/about_us_banner.jpg'); background-position: 50% 27%;">

        <div class="container">
          <div class="row justify-content-lg-center">
            <div class="col-lg-8 text-center pv-20">
              <h3 class="title logo-font object-non-visible" data-animation-effect="fadeIn" data-effect-delay="100"><?php echo $settings->company_name; ?></h3>
              <div class="separator object-non-visible mt-10" data-animation-effect="fadeIn" data-effect-delay="100"></div>
              <p class="text-center object-non-visible" data-animation-effect="fadeIn" data-effect-delay="100">
				<?php echo character_limiter(strip_tags($settings->motto), 300); ?>
			  </p>
            </div>
          </div>
        </div>
      </div>
      <!-- banner end -->

      <!-- main-container start -->
      <!-- ================ -->
      <section class="main-container padding-bottom-clear">

        <div class="container">
          <div class="row">

            <!-- main start -->
            <!-- ================ -->
            <div class="main col-12">
              <h3 class="title"> <strong><?php echo text("Hakkımızda"); ?></strong></h3>
              <div class="separator-2"></div>
              <div class="row">
                <div class="col-lg-12">
                  <p>
					<?php echo $settings->about_us; ?>
				  </p> 
                </div>
              </div>
            </div>
            <!-- main end -->

          </div>
        </div>

   

        <!-- section start -->
        <!-- ================ -->
        <div class="section">
          <div class="container">
            <h3 class="mt-4"><strong><?php echo text("Neden bizi seçmelisiniz"); ?> ?</strong></h3>
            <div class="separator-2"></div>
            <div class="row">
              <!-- accordion start -->
              <!-- ================ -->
              <div class="col-lg-12">
                <div id="accordion" class="collapse-style-1" role="tablist" aria-multiselectable="true">
                  <div class="card">
                    <div class="card-header" role="tab" id="headingOne">
                      <h4 class="panel-title">
                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                          <i class="fa fa-rocket pr-10"></i><?php echo text("Misyonumuz"); ?>
                        </a>
                      </h4>
                    </div>
                    <div id="collapseOne" class="collapse show" role="tabpanel" aria-labelledby="headingOne">
                      <div class="card-block">
                        <?php echo strip_tags($settings->mission); ?>
                      </div>
                    </div>
                  </div>
                  <div class="card">
                    <div class="card-header" role="tab" id="headingTwo">
                      <h4 class="panel-title">
                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" class="collapsed" aria-expanded="true" aria-controls="collapseTwo">
                          <i class="fa fa-leaf pr-10"></i><?php echo text("Vizyonumuz"); ?>
                        </a>
                      </h4>
                    </div>
                    <div id="collapseTwo" class="collapse" role="tabpanel" aria-labelledby="headingTwo">
                      <div class="card-block">
                         <?php echo strip_tags($settings->vision); ?> 
                      </div>
                    </div>
                  </div>

                </div>
              </div>
              <!-- accordion end -->

            </div>
            <!-- clients start -->
            <!-- ================ -->
            <div class="separator"></div>
            <div class="clients-container">
              <div class="clients">
                <?php foreach($references as $reference){ ?>
                   <div class="client-image object-non-visible" data-animation-effect="fadeIn" data-effect-delay="100">
                    <a href="#"><img src="<?php echo get_picture("references_v", $reference->img_url, "80x80"); ?>" alt="<?php echo $reference->title; ?>"></a>
                  </div>
                <?php } ?>

              </div>
            </div>
            <!-- clients end -->
          </div>
        </div>
        <!-- section end -->

      </section>
      <!-- main-container end -->

