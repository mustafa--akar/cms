	<div class="main-container">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					  <!-- banner start -->
					  <!-- ================ -->
					  <div class="pv-40 banner light-gray-bg">
						<div class="container clearfix">

						  <!-- slideshow start -->
						  <!-- ================ -->
						  <div class="slideshow">

							<!-- slider revolution start -->
							<!-- ================ -->
							<div class="slider-revolution-5-container">
							  <div id="slider-banner-boxedwidth" class="slider-banner-boxedwidth rev_slider" data-version="5.0">
								<ul class="slides">
								  
								  <?php foreach($portfolio_images as $image){ ?>
									<li class="text-center" data-transition="slidehorizontal" data-slotamount="default" data-masterspeed="default" data-title="<?php echo $portfolio->title; ?>">

								  
										<img src="<?php echo get_picture("portfolio_v", $image->img_url, "1080x426");  ?>" alt="<?php echo $portfolio->title; ?>" data-bgposition="center top"  data-bgrepeat="no-repeat" data-bgfit="cover" class="rev-slidebg">

								  </li>
								
								  <?php } ?>	
		
								</ul>
								<div class="tp-bannertimer"></div>
							  </div>
							</div>
							<!-- slider revolution end -->

						  </div>
						  <!-- slideshow end -->

						</div>
					  </div>
					  <!-- banner end -->

					  <!-- main-container start -->
					  <!-- ================ -->
					  <section class="main-container padding-ver-clear">
						<div class="container pv-40">
						  <div class="row">

							<!-- main start -->
							<!-- ================ -->
							<div class="main col-lg-8">
							  <h1 class="title"><?php echo $portfolio->title; ?></h1>
							  <div class="separator-2"></div>
							  <p><?php echo $portfolio->description; ?></p>
		
							</div>
							<!-- main end -->
							<aside class="col-lg-4">
							  <div class="sidebar">
								<div class="block clearfix">
								  <h3 class="title"><?php echo text("Portfolyo Detayları"); ?></h3>
								  <div class="separator-2"></div>
								  <ul class="list margin-clear">
									<li><strong><?php echo text("Müşteri"); ?>: </strong><span class="text-right"><?php echo $portfolio->client; ?></span></li>
								    <li><strong><?php echo text("Tarih"); ?>: </strong><span class="text-right"><?php echo get_readable_date($portfolio->finishedAt); ?></span></li>
								     <li><strong><?php echo text("Kategori"); ?>: </strong><span class="text-right"><?php echo get_category_title($portfolio->category_id, "portfolio"); ?></span></li>
									 <li><strong><?php echo text("Yer"); ?>: </strong><span class="text-right"><?php echo $portfolio->place; ?></span></li>
									 <li><strong><?php echo text("URL"); ?>: </strong><span class="text-right"><a href="<?php echo $portfolio->portfolio_url; ?>"><?php echo $portfolio->portfolio_url; ?></a></span></li>
								  </ul>	
			
								</div>
							  </div>
							</aside>

						  </div>
						</div>
					  </section>
					  <!-- main-container end -->

					  <!-- section start -->
					  <!-- ================ -->
					  <section class="section light-gray-bg pv-40 clearfix">
						<div class="container">
						  <h3 class="mt-3"> <strong><?php echo text("Diğer Portfolyolar"); ?></strong></h3>
						  <div class="row grid-space-10">
								<?php foreach($other_portfolios as $portfolio){ ?>
									<div class="col-sm-4">
									  <div class="image-box style-2 mb-20 bordered light-gray-bg">
										<div class="overlay-container overlay-visible">
										
											 <img
                       							 src="<?php echo get_picture("portfolio_v", get_portfolio_cover_image($portfolio->id), "352x171"); ?>"
                      							  alt="<?php echo $portfolio->title; ?>">  
										
										  <div class="overlay-bottom text-left">
											<p class="lead margin-clear"><?php echo $portfolio->title; ?></p>
										  </div>
										</div>
										<div class="body">
										  <p class="small mb-10 text-muted"><i class="icon-calendar"></i> <?php echo get_readable_date($portfolio->createdAt); ?> <i class="pl-10 icon-tag-1"></i> <?php echo get_category_title($portfolio->category_id, "portfolio"); ?></p>
										  <p><?php echo character_limiter(strip_tags($portfolio->description), 120); ?></p>
										  <a href="<?php echo base_url(get_url("portfolyo-detay")."/".$portfolio->url); ?>" class="btn btn-default btn-sm btn-hvr hvr-sweep-to-right margin-clear"><?php echo text("Görüntüle"); ?><i class="fa fa-arrow-right pl-10"></i></a>
										</div>
									  </div>
									</div>
								<?php } ?>				
						  </div>
						</div>
					  </section>
					  <!-- section end -->
				
				</div>
			</div>
		</div>
	</div>