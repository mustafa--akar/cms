	<section class="main-container">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
				  <h1 class="page-title">Ürün Listesi</h1>
				  <p>Ürünlerimizin Listesi Aşağıdaki Gibidir...</p>
				  <div class="separator-2"></div>				
				</div>			
			</div>
			
			<div class="row">
			
				<!-- sidebar start -->
				<!-- ================ -->
				<aside class="col-md-4">
				  <div class="sidebar">
					<div class="block clearfix">
					  <h3 class="title">Ürün Kategorileri</h3>
					  <div class="separator-2"></div>
					  <nav>
					  
						<?php echo drawElements($tree); ?>	

					  </nav>
					</div>
					<div class="block clearfix">
					  <h3 class="title">Son Eklenen Eğitimlerimiz</h3>
					  <div class="separator-2"></div>
					  <?php foreach($courses as $course){ ?>
					  <div class="media margin-clear">
						<div class="d-flex pr-2">
						  <div class="overlay-container">
							<img class="media-object" src="<?php echo get_picture("courses_v", $course->img_url, "255x157"); ?>" alt="blog-thumb">
							<a href="<?php echo base_url(get_url("egitim-detay")."/".$course->url); ?>" class="overlay-link small"><i class="fa fa-link"></i></a>
						  </div>
						</div>
						<div class="media-body">
						  	
						  <h6 class="media-heading">
						  	<a href="<?php echo base_url(get_url("egitim-detay")."/".$course->url); ?>"> 
						  		<?php echo $course->title; ?>						  			
						  	</a>
						  </h6>
						  <p class="small margin-clear"><i class="fa fa-calendar pr-10"></i><?php echo get_readable_date($course->event_date); ?></p>
						</div>
					  </div>
					  <hr>
					  <?php } ?>

					  <div class="text-right space-top">
						<a href="<?php echo base_url(get_url("egitim-listesi")); ?>" class="link-dark"><i class="fa fa-plus-circle pl-1 pr-1"></i>Daha Fazlası</a>
					  </div>
					</div>

				  </div>
				</aside>
				<!-- sidebar end -->
			
				<div class="col-md-8">
					<section class="main-container">
						<div class="container">
							 <div class="row">
							 
								<?php foreach($products as $product){ ?>
									<div class="col-sm-6">
									  <div class="image-box style-2 mb-20 bordered light-gray-bg">
										<div class="overlay-container overlay-visible">
										
							
										  <img src="<?php echo get_picture("product_v", get_product_cover_image($product->id), "333x200"); ?>" alt="<?php echo $product->title; ?>">
										  <div class="overlay-bottom text-left">
											<p class="lead margin-clear"><?php echo $product->title; ?></p>
										  </div>
										</div>
										<div class="body">
										  <p class="small mb-10 text-muted"><i class="icon-calendar"></i> <?php echo get_readable_date($product->createdAt); ?> <i class="pl-10 icon-tag-1"></i> <?php echo get_category_title($product->category_id); ?></p>
										  <p><?php echo character_limiter(strip_tags($product->description), 120); ?></p>
										  <a href="<?php echo base_url(get_url("urun-detay")."/".$product->url); ?>" class="btn btn-default btn-sm btn-hvr hvr-sweep-to-right margin-clear">Görüntüle<i class="fa fa-arrow-right pl-10"></i></a>
										</div>
									  </div>
									</div>
								<?php } ?>

							  </div>						
						</div>
					</section>						
				</div>		
			</div>
		
		</div>
	</section>	

