      <!-- main-container start -->
      <!-- ================ -->
      <section class="main-container">

        <div class="container">
          <div class="row">

            <!-- main start -->
            <!-- ================ -->
            <div class="main col-12">

              <!-- page-title start -->
              <!-- ================ -->
              <h1 class="page-title">Referans Listesi</h1>
              <div class="separator-2"></div>
              <!-- page-title end -->
              <p class="lead">Sizin için referanslarımızdan bazıları</p>
			  <?php $index = 0; ?>
			  <?php foreach($references as $reference){ ?>	
			  
				  <div class="image-box style-4 light-gray-bg">
					<div class="row grid-space-0">
					
					  <?php if(($index % 2) == 0){ ?>	
						  <div class="col-lg-3">
							<div class="overlay-container">
							  <img src="<?php echo get_picture("references_v", $reference->img_url, "300x300"); ?>" alt="<?php echo $reference->title; ?>">
							  <div class="overlay-to-top">
								<p class="small margin-clear"><em> <?php echo $reference->title; ?></em></p>
							  </div>
							</div>
						  </div>
						  <div class="col-lg-9">
							<div class="body">
							  <div class="pv-30 hidden-lg-down"></div>
							  <h3><?php echo $reference->title ; ?></h3>
						
							  <div class="separator-2"></div>
							  <p class="margin-clear"><?php echo character_limiter(strip_tags($reference->description), 400); ?></p>
					
							</div>
						  </div>
					  <?php }else{ ?>

						  <div class="col-lg-9">
							<div class="body">
							  <div class="pv-30 hidden-lg-down"></div>
							  <h3><?php echo $reference->title ; ?></h3>
						
							  <div class="separator-2"></div>
							  <p class="margin-clear"><?php echo character_limiter(strip_tags($reference->description), 400); ?></p>
					
							</div>
						  </div>
						  <div class="col-lg-3">
							<div class="overlay-container">
							  <img src="<?php echo get_picture("references_v", $reference->img_url, "300x300"); ?>" alt="<?php echo $reference->title; ?>">
							  <div class="overlay-to-top">
								<p class="small margin-clear"><em> <?php echo $reference->title; ?></em></p>
							  </div>
							</div>
						  </div>						  
					  <?php } ?>	
					</div>
				  </div>
				  <?php $index++; ?>	
			  <?php } ?>	
			  <div class="text-center"><?php echo $links; ?></div>
            </div>
            <!-- main end -->

          </div>
        </div>
      </section>
      <!-- main-container end -->
	

