  <?php $settings = get_settings(); ?>

  <section class="main-container">
    <div class="container">
      <div class="row">    
        <div class="main col-md-12">
          <h1 class="page-title"><?php echo $gallery->title; ?></h1>
          <div class="separator-2"></div>    
          <br> 			  
             
          <div class="row grid-space-20">
            <?php if(!empty($files)){ ?>
              <table class="table table-hover table-striped table-bordered table-colored">
                <thead>
                  <th> Dosya Adı </th>          
                 
                  <th> İndir  </th>
                  <tbody>
                     <?php foreach($files as $file){ ?>                     
    
                      <tr>
                        <td><?php echo ucwords(get_gallery_file_name($file->url)); ?></td>
                        <td>
                         <a 
                          target="_blank"
                           href="<?php echo base_url("panel/uploads/galleries_v/files/$gallery->folder_name/$file->url"); ?>"
                           class="btn btn-animated btn-default"
                           download="<?php echo $settings->company_name."-".$file->url ?>">İndir<i class="pl-10 fa fa-download"></i></a>  
                        </td>
                      </tr>
                    <?php } ?>
                  </tbody>                    
                </thead>
              </table>  
            <?php }else{ ?>
              <div class="alert alert-warning col-md-12">
                Burada malesef bir veri bulunamadı !
              </div>              
            <?php } ?>
            <div class="col-md-12">
              <a class="btn btn-default" href="<?php echo base_url(get_url("dosya-galerisi")); ?>">
                <i class="fa fa-arrow-left"></i> Geri Dön  
              </a>              
            </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>        
            
			