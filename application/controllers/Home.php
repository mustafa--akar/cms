<?php 

class Home extends MY_Controller {
	
	public $viewFolder = "";
	
	public function __construct()
	{			
		parent::__construct();
			
	}

	public function index()
	{
		if($this->uri->segment(1) == "en" || $this->uri->segment(1) == "tr" || $this->uri->segment(1) == "de"){
			$viewData = new stdClass();		
			$viewData->viewFolder = "home_v";

			$this->load->model("slide_model");
			$this->load->model("reference_model");
			$this->load->model("service_model");
			$this->load->model("portfolio_model");
			$this->load->model("testimonial_model");
			$this->load->model("settings_model");
			$this->load->model("gallery_model");
			$this->load->model("image_model");
			
			$settings = $this->settings_model->get(
				array(
					"lang"	=> $this->session->userdata("lang")
				)
			);	

			if(!empty($settings->homepage_gallery_id)){
				$viewData->homepage_gallery = $this->gallery_model->get(
					array(
						"id"	=> $settings->homepage_gallery_id
					)
				);	
			}
			$viewData->homepage_images = $this->image_model->get_all(
				array(
					"isActive"	    => 1,
					"gallery_id"    => $settings->homepage_gallery_id
				), "rank DESC"
			
			);			

			$viewData->slides = $this->slide_model->get_all(
				array(
					"isActive"	=> 1,
					"lang"		=> $this->session->userdata("lang")
				), "rank ASC"
			);

			$viewData->references = $this->reference_model->get_all(
				array(
					"isActive"	=> 1,
					"lang"		=> $this->session->userdata("lang")
				), "rank ASC", 8, 0
			);

			$viewData->services = $this->service_model->get_all(
				array(
					"isActive"	=> 1,
					"lang"		=> $this->session->userdata("lang")
				), "rank ASC", 3, 0
			);		

			$viewData->portfolios = $this->portfolio_model->get_all(
				array(
					"isActive"	=> 1,
					"lang"		=> $this->session->userdata("lang")
				), "rank ASC", 4, 0
			);	

			$viewData->testimonials = $this->testimonial_model->get_all(
				array(
					"isActive"	=> 1,
					"lang"		=> $this->session->userdata("lang")
				), "rank ASC"
			);				

			$this->load->view($viewData->viewFolder, $viewData);

		}else{
			redirect(base_url($this->session->userdata("lang")));
		}

	}
	
	public function product_list()
	{
		if($this->uri->segment(3)){			
			$category_url = $this->uri->segment(3);			
		}else{
			$category_url = FALSE;
		}
		$viewData = new stdClass();		
		$viewData->viewFolder = "product_list_v";		
		$this->load->model("product_model");
		$this->load->model("product_category_model");
		
		if($category_url){
			$category = $this->product_category_model->get(
				array(
					"url"    => $category_url,
					"lang"	 => $this->session->userdata("lang")
				)
			);
			$viewData->products = $this->product_model->get_all(
				array(
					"isActive"	   => 1,
					"category_id"  => $category->id
				), "rank ASC", array()
			
			);		
		}else{
			$viewData->products = $this->product_model->get_all(
				array(
					"isActive"	=> 1,
					"lang"		=> $this->session->userdata("lang")
				), "rank ASC"
			
			);			
		}	

		$items = $this->product_category_model->get_all(
			array(
				"isActive"	=> 1,
				"lang"		=> $this->session->userdata("lang")
			)
		);
		$viewData->tree = buildTree($items);	
		
		$this->load->model("course_model");	
		$viewData->courses = $this->course_model->get_all(
			array(
				"isActive"	=> 1,
				"lang"		=> $this->session->userdata("lang")
			), "rank ASC", 3, 0
		);
		
		$this->load->view($viewData->viewFolder, $viewData);
		
	}
	
	
	public function portfolio_list()
	{

		//echo $this->uri->total_segments(); die();
		$viewData = new stdClass();		
		$viewData->viewFolder = "portfolio_list_v";		
		$this->load->model("portfolio_model");
		$this->load->model("portfolio_category_model");

		$this->load->library("pagination");
		$config["base_url"]			= base_url(get_url("portfolyo-listesi"));
		$config["total_rows"]   	= $this->portfolio_model->get_count(array("lang" => $this->session->userdata("lang")));
		$config["uri_segment"]  	= 3;
		$config["per_page"]			= 8;
		$config['full_tag_open']    = '<ul class="pagination justify-content-center">';
		$config['full_tag_close']   = '</ul>';
		$config['attributes']	    = ['class' => 'page-link'];
		$config['first_link']	    = false;
		$config['last_link'] 		= false;
		$config['first_tag_open'] 	= '<li class="page-item">';
		$config['first_tag_close']  = '</li>';
		$config['prev_link'] 		= '&laquo';
		$config['prev_tag_open'] 	= '<li class="page-item">';
		$config['prev_tag_close']   = '</li>';
		$config['next_link'] 		= '&raquo';
		$config['next_tag_open'] 	= '<li class="page-item">';
		$config['next_tag_close']   = '</li>';
		$config['last_tag_open']    = '<li class="page-item">';
		$config['last_tag_close']   = '</li>';
		$config['cur_tag_open']     = '<li class="page-item active"><a href="#" class="page-link">';
		$config['cur_tag_close']    = '<span class="sr-only">(current)</span></a></li>';
		$config['num_tag_open']     = '<li class="page-item">';
		$config['num_tag_close']    = '</li>';

		$this->pagination->initialize($config);

		$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;

		$viewData->links = $this->pagination->create_links();		

		$viewData->portfolios = $this->portfolio_model->get_all(
			array(
				"isActive"	   => 1,
				"lang"		   => $this->session->userdata("lang")
			), "rank ASC", $config["per_page"], $page
		
		);		
		

		$viewData->categories = $this->portfolio_category_model->get_all(
			array(
				"isActive"	=> 1,
				"lang"		=> $this->session->userdata("lang")
			)
		);
			
		
		$this->load->view($viewData->viewFolder, $viewData);
		
	}
	
	public function product_detail($url)
	{
		$viewData = new stdClass();		
		$viewData->viewFolder = "product_v";
		$this->load->model("product_model");
		$this->load->model("product_image_model");
		$this->load->model("product_category_model");
		
		
		$viewData->product = $this->product_model->get(
			array(
				"isActive"	  => 1,
				"url"		  => $url
			)
		
		);
		
		$viewData->product_images = $this->product_image_model->get_all(
			array(
				"isActive"	   => 1,
				"product_id"   => $viewData->product->id					
			), "rank ASC"
		
		);
		
		$viewData->other_products = $this->product_model->get_all(
			array(
				"isActive"	=> 1,
				"id != "	=> $viewData->product->id,
				"lang"		=> $this->session->userdata("lang")
			), "rand()", 3, 0
		
		);			
		
		$this->load->view($viewData->viewFolder, $viewData);
	}
	
	public function portfolio_detail($url)
	{
		$viewData = new stdClass();		
		$viewData->viewFolder = "portfolio_v";
		$this->load->model("portfolio_model");
		$this->load->model("portfolio_image_model");
		$this->load->model("portfolio_category_model");		
		
		$viewData->portfolio = $this->portfolio_model->get(
			array(
				"isActive"	  => 1,
				"url"		  => $url
			)
		
		);
		
		$viewData->portfolio_images = $this->portfolio_image_model->get_all(
			array(
				"isActive"	   => 1,
				"portfolio_id"   => $viewData->portfolio->id					
			), "rank ASC"
		
		);
		
		$viewData->other_portfolios = $this->portfolio_model->get_all(
			array(
				"isActive"	=> 1,
				"id != "	=> $viewData->portfolio->id,
				"lang"		=> $this->session->userdata("lang")
			), "rand()", array("start" => 0, "count" => 3)
		
		);			
		
		$this->load->view($viewData->viewFolder, $viewData);
	}

	public function course_list()
	{		
		$viewData = new stdClass();
		$viewData->viewFolder = "course_list_v";
		
		$this->load->model("course_model");

		$this->load->library("pagination");
		$config["base_url"]			= base_url(get_url("egitim-listesi"));
		$config["total_rows"]   	= $this->course_model->get_count(array("lang" => $this->session->userdata("lang")));
		$config["uri_segment"]  	= 3;
		$config["per_page"]			= 8;
		$config['full_tag_open']    = '<ul class="pagination justify-content-center">';
		$config['full_tag_close']   = '</ul>';
		$config['attributes']	    = ['class' => 'page-link'];
		$config['first_link']	    = false;
		$config['last_link'] 		= false;
		$config['first_tag_open'] 	= '<li class="page-item">';
		$config['first_tag_close']  = '</li>';
		$config['prev_link'] 		= '&laquo';
		$config['prev_tag_open'] 	= '<li class="page-item">';
		$config['prev_tag_close']   = '</li>';
		$config['next_link'] 		= '&raquo';
		$config['next_tag_open'] 	= '<li class="page-item">';
		$config['next_tag_close']   = '</li>';
		$config['last_tag_open']    = '<li class="page-item">';
		$config['last_tag_close']   = '</li>';
		$config['cur_tag_open']     = '<li class="page-item active"><a href="#" class="page-link">';
		$config['cur_tag_close']    = '<span class="sr-only">(current)</span></a></li>';
		$config['num_tag_open']     = '<li class="page-item">';
		$config['num_tag_close']    = '</li>';

		$this->pagination->initialize($config);

		$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;

		$viewData->links = $this->pagination->create_links();		
		$viewData->courses = $this->course_model->get_all(
			array(
				"isActive"	=> 1,
				"lang"		=> $this->session->userdata("lang")				
			), "rank ASC, event_date ASC", $config["per_page"], $page
		);
		
		$this->load->view($viewData->viewFolder, $viewData);		
		
	}

	public function course_detail($url)
	{
		$viewData = new stdClass();		
		$viewData->viewFolder = "course_v";
		$this->load->model("course_model");
		
		
		$viewData->course = $this->course_model->get(
			array(
				"isActive"	  => 1,
				"url"		  => $url
			)		
		);	
		
		$viewData->other_courses = $this->course_model->get_all(
			array(
				"isActive"	=> 1,
				"id != "	=> $viewData->course->id,
				"lang"		=> $this->session->userdata("lang")	
			), "rand()", array("start" => 0, "count" => 3)
		
		);			
		
		$this->load->view($viewData->viewFolder, $viewData);
	}

	public function reference_list()
	{		
		$viewData = new stdClass();
		$viewData->viewFolder = "reference_list_v";
		
		$this->load->model("reference_model");

		$this->load->library("pagination");
		$config["base_url"]			= base_url(get_url("referanslar"));
		$config["total_rows"]   	= $this->reference_model->get_count(array("lang" => $this->session->userdata("lang")));
		$config["uri_segment"]  	= 3;
		$config["per_page"]			= 8;
		$config['full_tag_open']    = '<ul class="pagination justify-content-center">';
		$config['full_tag_close']   = '</ul>';
		$config['attributes']	    = ['class' => 'page-link'];
		$config['first_link']	    = false;
		$config['last_link'] 		= false;
		$config['first_tag_open'] 	= '<li class="page-item">';
		$config['first_tag_close']  = '</li>';
		$config['prev_link'] 		= '&laquo';
		$config['prev_tag_open'] 	= '<li class="page-item">';
		$config['prev_tag_close']   = '</li>';
		$config['next_link'] 		= '&raquo';
		$config['next_tag_open'] 	= '<li class="page-item">';
		$config['next_tag_close']   = '</li>';
		$config['last_tag_open']    = '<li class="page-item">';
		$config['last_tag_close']   = '</li>';
		$config['cur_tag_open']     = '<li class="page-item active"><a href="#" class="page-link">';
		$config['cur_tag_close']    = '<span class="sr-only">(current)</span></a></li>';
		$config['num_tag_open']     = '<li class="page-item">';
		$config['num_tag_close']    = '</li>';

		$this->pagination->initialize($config);

		$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;

		$viewData->links = $this->pagination->create_links();		
		$viewData->references = $this->reference_model->get_all(
			array(
				"isActive"	=> 1,
				"lang"		=> $this->session->userdata("lang")				
			), "rank ASC", $config["per_page"], $page
		);
		
		$this->load->view($viewData->viewFolder, $viewData);		
		
	}	

	public function brand_list()
	{		
		$viewData = new stdClass();
		$viewData->viewFolder = "brand_list_v";
		
		$this->load->model("brand_model");
		
		$viewData->brands = $this->brand_model->get_all(
			array(
				"isActive"	=> 1				
			), "rank ASC"
		);
		
		$this->load->view($viewData->viewFolder, $viewData);		
		
	}
	
	public function service_list()
	{		
		$viewData = new stdClass();
		$viewData->viewFolder = "service_list_v";
		
		$this->load->model("service_model");

		$this->load->library("pagination");
		$config["base_url"]			= base_url(get_url("hizmetlerimiz"));
		$config["total_rows"]   	= $this->service_model->get_count(array("lang" => $this->session->userdata("lang")));
		$config["uri_segment"]  	= 3;
		$config["per_page"]			= 6;
		$config['full_tag_open']    = '<ul class="pagination justify-content-center">';
		$config['full_tag_close']   = '</ul>';
		$config['attributes']	    = ['class' => 'page-link'];
		$config['first_link']	    = false;
		$config['last_link'] 		= false;
		$config['first_tag_open'] 	= '<li class="page-item">';
		$config['first_tag_close']  = '</li>';
		$config['prev_link'] 		= '&laquo';
		$config['prev_tag_open'] 	= '<li class="page-item">';
		$config['prev_tag_close']   = '</li>';
		$config['next_link'] 		= '&raquo';
		$config['next_tag_open'] 	= '<li class="page-item">';
		$config['next_tag_close']   = '</li>';
		$config['last_tag_open']    = '<li class="page-item">';
		$config['last_tag_close']   = '</li>';
		$config['cur_tag_open']     = '<li class="page-item active"><a href="#" class="page-link">';
		$config['cur_tag_close']    = '<span class="sr-only">(current)</span></a></li>';
		$config['num_tag_open']     = '<li class="page-item">';
		$config['num_tag_close']    = '</li>';

		$this->pagination->initialize($config);

		$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;

		$viewData->links = $this->pagination->create_links();			
		
		$viewData->services = $this->service_model->get_all(
			array(
				"isActive"	=> 1,
				"lang"		=> $this->session->userdata("lang")				
			), "rank ASC", $config["per_page"], $page
		);
		
		$this->load->view($viewData->viewFolder, $viewData);		
		
	}	

	public function about_us()
	{
		if($this->uri->segment(1) == "en" || $this->uri->segment(1) == "tr" || $this->uri->segment(1) == "de"){
			$viewData = new stdClass();
			$viewData->viewFolder = "about_v";
			
			$this->load->model("settings_model");		
			$viewData->settings = $this->settings_model->get(
				array(
					"lang"	=> $this->session->userdata("lang")
				)
			);

			$this->load->model("reference_model");
			$viewData->references = $this->reference_model->get_all(
				array(
					"isActive"	=> 1,
					"lang"		=> $this->session->userdata("lang")
				), "rank ASC", 8, 0
			);
			
			$this->load->view($viewData->viewFolder, $viewData);
		}else{
			redirect(base_url($this->session->userdata("lang")."/hakkimizda"));
		}				
	}

	public function contact($data = "")
	{
		$viewData = new stdClass();
		if($data){
			
			$viewData->error_message = $data->error_message;
			$viewData->form_error    = $data->form_error;
		}
		
		$viewData->viewFolder = "contact_v";
		
		$this->load->helper("captcha");
		
		$config = array(
			"word"			=> "",
			"img_path"		=> "captcha/",
			"img_url"		=> base_url("captcha"),
			"font_path" 	=> "fonts/corbel.ttf",
			"img_width"		=> 150,
			"img_height"	=> 50,
			"expiration"	=> 7200,
			"word_length"	=> 5,
			"font_size"		=> 25,
			"img_id"		=> "captcha_img",
			"pool"			=> "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ",
			"colors"		=> array(
				"background"   =>  array(56,255,45),
				"text"  	   =>  array(0,0,0,),
				"grid"		   =>  array(0,0,0),
				"border"	   =>  array(255,40,40)			
			
			)	
		);
		
		
		$viewData->captcha = create_captcha($config);
		
		$this->session->set_userdata("captcha", $viewData->captcha["word"]);
		
		$this->load->view($viewData->viewFolder, $viewData);		
		
	}

	public function send_contact_message()
	{

		$this->load->library("form_validation");
		
		$this->form_validation->set_rules("name","Ad","trim|required");
		$this->form_validation->set_rules("email","E-posta","trim|required|valid_email");
		$this->form_validation->set_rules("subject","Konu","trim|required");
		$this->form_validation->set_rules("message","Mesaj","trim|required");
		$this->form_validation->set_rules("captcha","Doğrulama Kodu","trim|required");
		$this->form_validation->set_rules("captcha","Doğrulama Kodu","trim|required");
		$this->form_validation->set_message(
			array(
				"required"      => "{field} alanı doldurulmalıdır"	
			)
		);		
		
		if($this->form_validation->run() === FALSE){
			$viewData = new stdClass();
			$viewData->error_message = text("Lütfen formu eksiksiz doldurun !");	
			$viewData->form_error    = true;
			$this->contact($viewData);			

		}else{
			
			if($this->session->userdata("captcha") == $this->input->post("captcha")){
				
				$name = $this->input->post("name");
				$email = $this->input->post("email");
				$subject = $this->input->post("subject");
				$message = $this->input->post("message");
				
				$email_message = "{$name} isimli ziyaretçiden bir mesajınız var <br> <b> E-Posta :</b> {$email} <br> <b> Mesaj : </b> {$message}";
				
				if(send_email("", "Site İletişim Mesajı | $subject", $email_message)){

					$alert = array(					
						"type"	  => "success"
					);	
				}else{
					$alert = array(			
						"type"	  => "error"
					);	
				}
				$this->session->set_flashdata("alert", $alert);
				redirect(base_url(get_url("iletisim")));
			}else{


				//İşlemin Sonucunu Sessiona yazma işlemi
				$this->session->set_flashdata("alert", $alert);	
				redirect(base_url(get_url("iletisim")));
				
			}
		}
			
		
	}

	public function make_me_member()
	{
		$this->load->library("form_validation");
		
		$this->form_validation->set_rules("subscribe_email","E-Posta Adresi","trim|required|valid_email");
		
		if($this->form_validation->run() == FALSE){
			$alert = array(					
					"type"	  => "valid_error",
					"message" => "Lütfen E-Posta adresinizi yazınız !"
				);
			$this->session->set_flashdata("member_alert", $alert);	
			redirect(base_url(get_url("iletisim")));		
		}else{
			$this->load->model("member_model");
			
			$insert = $this->member_model->add(
						array(
							"email"		  => $this->input->post("subscribe_email"),
							"ip_address"  => $this->input->ip_address(),
							"isActive"	  => 1,
							"createdAt"	  => date("Y-m-d H:i:s") 		
						)
			);
			if($insert){
				$alert = array(					
				    "type"	  => "success",
					"message" => "E-Posta adresiniz başarıyla kaydedildi"
					);
			}else{
				$alert = array(			
					"type"	  => "error",
					"message" => "Kayıt esnasında bir hata oluştu"
				);
			}
					
		}
		$this->session->set_flashdata("member_alert", $alert);	
		redirect(base_url(get_url("iletisim")));
		
	}	

	public function news_list()
	{
		
		$viewData = new stdClass();		
		$viewData->viewFolder = "news_list_v";		
		$this->load->model("news_model");

		$this->load->library("pagination");
		$config["base_url"]			= base_url(get_url("haberler"));
		$config["total_rows"]   	= $this->news_model->get_count(array("lang" => $this->session->userdata("lang")));
		$config["uri_segment"]  	= 3;
		$config["per_page"]			= 8;
		$config['full_tag_open']    = '<ul class="pagination justify-content-center">';
		$config['full_tag_close']   = '</ul>';
		$config['attributes']       = ['class' => 'page-link'];
		$config['first_link']       = false;
		$config['last_link']        = false;
		$config['first_tag_open']   = '<li class="page-item">';
		$config['first_tag_close']  = '</li>';
		$config['prev_link']        = '&laquo';
		$config['prev_tag_open']    = '<li class="page-item">';
		$config['prev_tag_close']   = '</li>';
		$config['next_link']        = '&raquo';
		$config['next_tag_open']    = '<li class="page-item">';
		$config['next_tag_close']   = '</li>';
		$config['last_tag_open']    = '<li class="page-item">';
		$config['last_tag_close']   = '</li>';
		$config['cur_tag_open']	    = '<li class="page-item active"><a href="#" class="page-link">';
		$config['cur_tag_close']    = '<span class="sr-only">(current)</span></a></li>';
		$config['num_tag_open']     = '<li class="page-item">';
		$config['num_tag_close']    = '</li>';

		$this->pagination->initialize($config);

		$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;

		$viewData->links = $this->pagination->create_links();					

		$viewData->news_list = $this->news_model->get_all(
			array(
				"isActive"	   => 1,
				"lang"		   => $this->session->userdata("lang")
			), "rank DESC", $config["per_page"], $page
		
		);		
		
		$this->load->view($viewData->viewFolder, $viewData);
		
	}

	public function news($url)
	{

		if($url != ""){

			$viewData = new stdClass();		
			$viewData->viewFolder = "news_v";		
			$this->load->model("news_model");
					

			$news = $this->news_model->get(
				array(
					"isActive"	   => 1,
					"url"		   => $url	
				)
			
			);		
			
			if($news){

				$viewData->news = $news;


				$viewData->recent_news_list = $this->news_model->get_all(
					array(
						"isActive"	=> 1,
						"id !="		=> $news->id,
						"lang"		=> $this->session->userdata("lang")
					), "rank DESC" , array("count" => 4, "start" => 0)
				);
				
				/********** Views Count değerini arttırma *****/
				$view_count = $news->view_count + 1;

				$this->news_model->update(
					array(
						"id"   => $news->id
					),
					array(
						"view_count"  => $view_count
					)
				);

				$viewData->news->view_count = $view_count;
				$viewData->opengraph = true;

				$this->load->view($viewData->viewFolder, $viewData);

			}else{
				// TODO Alert

			}
			

		}else{
			// TODO Alert	
		}
		
	}	

	public function popup_never_show_again()
	{
		$popup_id = $this->input->post("popup_id");

		set_cookie($popup_id, "true", 60 * 60 * 24 * 365);
	}

	public function image_gallery_list()
	{
		$viewData = new stdClass();		
		$viewData->viewFolder = "galleries_v";	
		$viewData->subViewFolder = "image_galleries";
		$viewData->viewName = "list_content";

		$this->load->model("gallery_model");
				

		$viewData->galleries = $this->gallery_model->get_all(
			array(
				"isActive"	    => 1,
				"gallery_type"  => "image",
				"lang"			=> $this->session->userdata("lang")
			), "rank DESC"
		
		);		
	
		$this->load->view($viewData->viewFolder, $viewData);
	}
	
	public function image_gallery($gallery_url = "")
	{

		if($gallery_url){
			
			$viewData = new stdClass();	
			$viewData->gallery = get_gallery_by_url($gallery_url);	
			$viewData->viewFolder = "galleries_v";	
			$viewData->subViewFolder = "image_galleries";
			$viewData->viewName = "item_content";
			
			$this->load->model("image_model");
					

			$viewData->images = $this->image_model->get_all(
				array(
					"isActive"	    => 1,
					"gallery_id"    => $viewData->gallery->id
				), "rank DESC"
			
			);		
		
			$this->load->view($viewData->viewFolder, $viewData);			
		}
		
	}

	public function video_gallery_list()
	{
		$viewData = new stdClass();		
		$viewData->viewFolder = "galleries_v";	
		$viewData->subViewFolder = "video_galleries";
		$viewData->viewName = "list_content";

		$this->load->model("gallery_model");
				

		$viewData->galleries = $this->gallery_model->get_all(
			array(
				"isActive"	    => 1,
				"gallery_type"  => "video",
				"lang"			=> $this->session->userdata("lang")
			), "rank DESC"
		
		);		
	
		$this->load->view($viewData->viewFolder, $viewData);
	}
	
	public function video_gallery($gallery_url = "")
	{
		if($gallery_url){
			
			$viewData = new stdClass();	
			$viewData->gallery = get_gallery_by_url($gallery_url);	
			$viewData->viewFolder = "galleries_v";	
			$viewData->subViewFolder = "video_galleries";
			$viewData->viewName = "item_content";

			$this->load->model("video_model");
					

			$viewData->videos = $this->video_model->get_all(
				array(
					"isActive"	    => 1,
					"gallery_id"    => $viewData->gallery->id
				), "rank DESC"
			
			);		
		
			$this->load->view($viewData->viewFolder, $viewData);			
		}		
	}

	public function file_gallery_list()
	{
		$viewData = new stdClass();		
		$viewData->viewFolder = "galleries_v";	
		$viewData->subViewFolder = "file_galleries";
		$viewData->viewName = "list_content";

		$this->load->model("gallery_model");
				

		$viewData->galleries = $this->gallery_model->get_all(
			array(
				"isActive"	    => 1,
				"gallery_type"  => "file",
				"lang"			=> $this->session->userdata("lang")
			), "rank DESC"
		
		);		
	
		$this->load->view($viewData->viewFolder, $viewData);
	}
	
	public function file_gallery($gallery_url = "")
	{
		if($gallery_url){
			
			$viewData = new stdClass();	
			$viewData->gallery = get_gallery_by_url($gallery_url);	
			$viewData->viewFolder = "galleries_v";	
			$viewData->subViewFolder = "file_galleries";
			$viewData->viewName = "item_content";

			$this->load->model("file_model");
					

			$viewData->files = $this->file_model->get_all(
				array(
					"isActive"	    => 1,
					"gallery_id"    => $viewData->gallery->id
				), "rank DESC"
			
			);		
		
			$this->load->view($viewData->viewFolder, $viewData);			
		}		
	}	


}


?>