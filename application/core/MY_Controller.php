<?php

class MY_Controller extends CI_Controller{

	public function __construct(){

		parent::__construct();
		$this->load_lang();
		$this->lang->load("site");	
		
		$this->viewFolder = "homepage";
		$this->load->helper("text");					
	}

	protected function load_lang()
	{
		if($this->uri->segment(1) == "en" || $this->uri->segment(1) == "tr" || $this->uri->segment(1) == "de"){

			$this->session->set_userdata("lang", $this->uri->segment(1));
		}

		if($this->session->userdata("lang")	== "de"){

			$lang = "german";
			$this->config->set_item("language", $lang);
			$this->session->set_userdata("lang", "de");

		}elseif($this->session->userdata("lang")	== "en"){

			$lang = "english";
			$this->config->set_item("language", $lang);
			$this->session->set_userdata("lang", "en");				
		}else{

			$lang = "turkish";
			$this->config->set_item("language", $lang);
			$this->session->set_userdata("lang", "tr");				
		}

	}



}


?>