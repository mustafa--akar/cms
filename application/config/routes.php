<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'home';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;


/*

$route['^(en|de|tr)/(.+)$'] = "$2";

$route['^en/(.+)$']			   = "$1";	
$route['^tr/(.+)$']			   = "$1";	
$route['^de/(.+)$']		       = "$1";	

$languages = "(en|de|tr)/";
$route[$languages.'urun-listesi'] = "home/product_list";

*/

$route['^(en|de|tr)$'] = $route['default_controller'];


$route['tr/urun-listesi'] = "home/product_list";
$route['de/produktliste'] = "home/product_list";
$route['en/product-list'] = "home/product_list";

$route["tr/urun-listesi/(:any)"] = "home/product_list/$1";
$route["de/produktliste/(:any)"] = "home/product_list/$1";
$route["en/product-list/(:any)"] = "home/product_list/$1";


$route['tr/urun-detay'] = "home/product_detail";
$route['de/produktdetails'] = "home/product_detail";
$route['en/product-details'] = "home/product_detail";


$route["tr/urun-detay/(:any)"] = "home/product_detail/$1";
$route["de/produktdetails/(:any)"] = "home/product_detail/$1";
$route["en/product-details/(:any)"] = "home/product_detail/$1";

$route["tr/portfolyo-listesi"] = "home/portfolio_list/$1";
$route["de/portfolioliste"] = "home/portfolio_list/$1";
$route["en/portfolio-liste"] = "home/portfolio_list/$1";

$route["tr/portfolyo-listesi/(:any)"] = "home/portfolio_list";
$route["de/portfolioliste/(:any)"] = "home/portfolio_list";
$route["en/portfolio-liste/(:any)"] = "home/portfolio_list";

$route["tr/portfolyo-detay"] = "home/portfolio_detail";
$route["de/portfoliodetails"] = "home/portfolio_detail";
$route["en/portfolio-details"] = "home/portfolio_detail";

$route["tr/portfolyo-detay/(:any)"] = "home/portfolio_detail/$1";
$route["de/portfoliodetails/(:any)"] = "home/portfolio_detail/$1";
$route["en/portfolio-details/(:any)"] = "home/portfolio_detail/$1";

$route["tr/egitim-listesi"] = "home/course_list";
$route["de/kursliste"] = "home/course_list";
$route["en/course-list"] = "home/course_list";

$route["tr/egitim-listesi/(:any)"] = "home/course_list/$1";
$route["de/kursliste/(:any)"] = "home/course_list/$1";
$route["en/course-list/(:any)"] = "home/course_list/$1";

$route["tr/egitim-detay/(:any)"] = "home/course_detail/$1";
$route["de/kursdetails/(:any)"] = "home/course_detail/$1";
$route["en/course-details/(:any)"] = "home/course_detail/$1";

$route["tr/referanslar"] = "home/reference_list";
$route["de/referenzen"] = "home/reference_list";
$route["en/references"] = "home/reference_list";

$route["tr/referanslar/(:any)"] = "home/reference_list/$1";
$route["de/referenzen/(:any)"] = "home/reference_list/$1";
$route["en/references/(:any)"] = "home/reference_list/$1";


$route["tr/markalar"] = "home/brand_list";
$route["de/marken"] = "home/brand_list";
$route["en/brands"] = "home/brand_list";

$route["tr/hizmetlerimiz"] = "home/service_list";
$route["de/dienstleistungen"] = "home/service_list";
$route["en/services"] = "home/service_list";

$route["tr/hizmetlerimiz/(:any)"] = "home/service_list/$1";
$route["de/dienstleistungen/(:any)"] = "home/service_list/$1";
$route["en/services/(:any)"] = "home/service_list/$1";

$route["tr/hakkimizda"] = "home/about_us";
$route["de/ueber-uns"] = "home/about_us";
$route["en/about-us"] = "home/about_us";


$route["tr/iletisim"] = "home/contact";
$route["de/kontakt"] = "home/contact";
$route["en/contact"] = "home/contact";

$route["mesaj-gonder"] = "home/send_contact_message"; 
$route["tr/mesaj-gonder"] = "home/send_contact_message"; 
$route["de/nachricht-senden"] = "home/send_contact_message"; 
$route["en/send-message"] = "home/send_contact_message"; 

$route["abone-ol"] = "home/make_me_member";
$route["tr/abone-ol"] = "home/make_me_member";
$route["de/abonieren"] = "home/make_me_member";
$route["en/subscribe"] = "home/make_me_member";

$route["tr/haberler"] = "home/news_list";
$route["de/nachrichten"] = "home/news_list";
$route["en/news-list"] = "home/news_list";

$route["tr/haberler/(:any)"] = "home/news_list/$1";
$route["de/nachrichten/(:any)"] = "home/news_list/$1";
$route["en/news-list/(:any)"] = "home/news_list/$1";

$route["tr/haber/(:any)"] = "home/news/$1";
$route["de/nachricht/(:any)"] = "home/news/$1";
$route["en/news/(:any)"] = "home/news/$1";

$route["bir-daha-gosterme"] = "home/popup_never_show_again";
$route["tr/bir-daha-gosterme"] = "home/popup_never_show_again";
$route["de/nie-wieder-zeigen"] = "home/popup_never_show_again";
$route["en/never-show-again"] = "home/popup_never_show_again";

$route["tr/fotograf-galerisi"] = "home/image_gallery_list";
$route["de/fotogalerie"] = "home/image_gallery_list";
$route["en/photo-gallery"] = "home/image_gallery_list";

$route["tr/fotograf-galerisi/(:any)"] = "home/image_gallery/$1";
$route["de/fotogalerie/(:any)"] = "home/image_gallery/$1";
$route["en/photo-gallery/(:any)"] = "home/image_gallery/$1";

$route["tr/video-galerisi"] = "home/video_gallery_list";
$route["de/videogalerie"] = "home/video_gallery_list";
$route["en/video-gallery"] = "home/video_gallery_list";

$route["tr/video-galerisi/(:any)"] = "home/video_gallery/$1";
$route["de/videogalerie/(:any)"] = "home/video_gallery/$1";
$route["en/video-gallery/(:any)"] = "home/video_gallery/$1";

$route["tr/dosya-galerisi"] = "home/file_gallery_list";
$route["de/dateigalerie"] = "home/file_gallery_list";
$route["en/file-gallery"] = "home/file_gallery_list";

$route["tr/dosya-galerisi/(:any)"] = "home/file_gallery/$1";
$route["de/dateigalerie/(:any)"] = "home/file_gallery/$1";
$route["en/file-gallery/(:any)"] = "home/file_gallery/$1";


